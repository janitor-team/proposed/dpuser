extern "C" void testProc(dpuserType &a) {
    a = 222;
    return;
}

extern "C" dpuserType testFunc1(dpuserType &x, dpuserType &y) {
x=3;
    return x * y;
}

extern "C" dpuserType testFunc2() {
	dpuserType a = 2;
	dpuserType b=3;
	return testFunc1(a,b);
}

extern "C" dpuserType testFunc3() {
	return 2;
}
