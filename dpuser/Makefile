################################################################################
# dpuser Makefile
################################################################################
# Type one of the following lines:
#   make                    same as 'make release'
#   make release            (static, 32/64-bit autodetect)
#   make release_shared     WIN only (shared, 32/64-bit autodetect)
#   make debug              (static, 32/64-bit autodetect)
#   make debug_shared       WIN only (shared, 32/64-bit autodetect)
#   make clean              to clean up the project
#   make revision           creates the revision-string
#   make dist               LINUX/MACOSX only to create a distribution as
#                           tar.gz-file, calls automatically the revision-target
#                           (do a checkout to a new location before, since all
#                           .svn-folders will be deleted!)
#
#
#   make parsers PLATFORM=LINUX (or MACOSX or WIN)
#                          to create just the parsers and lexxers for dpuser,
#                          dpuser2c & idl2c
#   make dpuser2c_header PLATFORM=LINUX (or MACOSX or WIN)
#                          to create just the dpuser2c.h-file
#
# In dpuser a static target means that all libraries and functionality is linked
# statically into the created binary.
# A shared target means that some dpuser-functionality is moved to a dll
# (external libraries are still linked statically).
#
################################################################################
RM          = rm -f
SED         = sed
CC          = gcc
CXX         = g++
LINK        = g++
QF          =
GDL         = -DHAS_GDL
PY          = -DHAS_PYTHON

PGPLOT      = -DHAS_PGPLOT
DPPGPLOT    = -DHAS_DPPGPLOT

DEFINES     = $(PGPLOT) $(DPPGPLOT) $(GDL) $(PY) -D$(OS)
ifeq ($(QF), -qt)
   DEFINES  += -DDPQT -DNO_READLINE
endif

CFLAGS      = -pipe -O2 -D_REENTRANT -w $(DEFINES)
CXXFLAGS    = -pipe -O2 -std=c++11 -D_REENTRANT -w $(DEFINES)
INCPATH     = -I. -I../utils -I../libfits -I../include -Iparser -I.

ifeq ($(GDL), -DHAS_GDL)
   INCPATH  += -I../include/gdl
endif

OBJECTS =     dpuser_utils.o \
              dpuser.procs.o \
              helpmap.o \
              y.mpfit.o \
              mpfitAST.o \
              lex.mpfit.o \
              dpuserType.o \
              dpstring.o \
              dpstringlist.o \
              regex_sr.o \
              mpfit.o \
	            kabsch2d.o \
              3d_stuff.o \
              cube.o \
              dpheader.o \
              fits.o \
              fits_cube.o \
              fits_dpl.o \
              fits_exc.o \
              fits_file.o \
              fits_filters.o \
              fits_funcs.o \
              fits_mem.o \
              fits_ops.o \
              fits_procs.o \
              fits_range.o \
              fits_red.o \
              JulianDay.o \
              math_utils.o \
              dpComplex.o \
              fitting.o \
              voronoi.o \
              fits_logic.o \
              astrolib.o \
							arithmetics.o \
							boolean.o \
							dpuserAST.o \
              dpuser.yacchelper.o \
              functions.o \
              y.tab.o \
              dpuser.input.o \
              procedures.o \
              lex.yy.o \
              svn_revision.o \
							gdl_dpuser.o \
							python_dpuser.o

ifeq ($(PGPLOT), -DHAS_PGPLOT)
   ifeq ($(DPPGPLOT), -DHAS_DPPGPLOT)
      OBJECTS += cpg3d.o
   endif
endif

################################################################################
# Platform-specific defines

ifeq ($(HOMEDRIVE), C:)
   OS             = WIN
   RM             = del
   SED_INPLACE    = ../bin/WIN/sed.exe -i
   
   # didn't manage to do this as onliner like in the unix-case, pipe doesn't work...
   REV_NR := $(shell svnversion -c ..)
   $(shell @echo $(REV_NR) > _bbb.txt)
   REV_NR := $(shell sed -e 's/[MS]//g' -e 's/^[[:digit:]]*://' _bbb.txt)
   $(shell del _bbb.txt)

   ifndef $(ARCH)
   # didn't manage to do this as onliner like in the unix-case, pipe doesn't work...
      ARCH := $(shell wmic os get osarchitecture)
      $(shell @echo $(ARCH) > _bbb.txt)
      ARCH := $(shell sed  -e "s/OSArchitecture//g" -e "s/ //g" -e "s/-bit//g" -e "s/-Bit//g" _bbb.txt)
      $(shell del _bbb.txt)
   endif

   DEFINES  += -DHAVE_MINGW32

   LIBS =     ../lib/$(OS)$(ARCH)/libfftw3.a \
              ../lib/$(OS)$(ARCH)/libcpgplot.a \
              ../lib/$(OS)$(ARCH)/libpgplot.a \
              ../lib/$(OS)$(ARCH)/libpng.a \
              ../lib/$(OS)$(ARCH)/libxpa.a \
              ../lib/$(OS)$(ARCH)/libf2c.a \
              ../lib/$(OS)$(ARCH)/libreadline.a \
              ../lib/$(OS)$(ARCH)/libtermcap.a

   ifeq ($(GDL), -DHAS_GDL)
          LIBS  += ../lib/$(OS)$(ARCH)/libgdl.a \
                   ../lib/$(OS)$(ARCH)/libfftw3f.a \
                   ../lib/$(OS)$(ARCH)/libplplotcxx.a \
                   ../lib/$(OS)$(ARCH)/libplplot.a \
                   ../lib/$(OS)$(ARCH)/libqsastime.a \
                   ../lib/$(OS)$(ARCH)/libcsirocsa.a \
                   ../lib/$(OS)$(ARCH)/libgnurx.a \
                   ../lib/$(OS)$(ARCH)/libxdr.a \
                   -lole32 -lspoolss -lwinspool -loleaut32 -luuid
   endif
   ifeq ($(PY), -DHAS_PYTHON)
#      LIBS +=	/Programme/Python3/libs/libpython36.a
      INCPATH  += -I/Programme/Python3/include
   endif
       LIBS  += ../lib/$(OS)$(ARCH)/libgsl.a \
                ../lib/$(OS)$(ARCH)/libgslcblas.a \
								../lib/$(OS)$(ARCH)/libz.a \
                -lgdi32 -lws2_32 -lshlwapi -lcomdlg32 -lcomctl32
				 
   LDFLAG      = -static-libgcc -static-libstdc++ -Wl,--enable-runtime-pseudo-reloc
else
   REV_NR := $(shell svnversion -c .. | sed -e 's/[MS]//g' -e 's/^[[:digit:]]*://')

   ifeq ($(shell uname -m), x86_64)
      ARCH = 64
   else
      ARCH = 32
   endif

   ifeq ($(TERM_PROGRAM), Apple_Terminal)
      OS          = MACOSX
      ifeq ($(ARCH), 32)
         LDFLAG      = -arch i386
         CXXFLAGS    := -arch i386 $(CXXFLAGS)
      else
         # -arch x86_64
         LDFLAG      = 
      endif
      SED_INPLACE = sed -i ''
      LIBS        = ../lib/$(OS)$(ARCH)/libxpa.a \
                    ../lib/$(OS)$(ARCH)/libfftw3.a \
                    ../lib/$(OS)$(ARCH)/libcpgplot.a \
                    ../lib/$(OS)$(ARCH)/libpgplot.a \
                    ../lib/$(OS)$(ARCH)/libpng.a \
                    ../lib/$(OS)$(ARCH)/libgsl.a \
                    ../lib/$(OS)$(ARCH)/libgslcblas.a \
                    ../lib/$(OS)$(ARCH)/libreadline.a \
                    ../lib/$(OS)$(ARCH)/libz.a \
                    ../lib/$(OS)$(ARCH)/libncurses.a \
                    ../lib/$(OS)$(ARCH)/libf2c.a
   ifeq ($(GDL), -DHAS_GDL)
            LIBS += ../lib/$(OS)$(ARCH)/libgdl.a \
                    ../lib/$(OS)$(ARCH)/libfftw3f.a \
                    ../lib/$(OS)$(ARCH)/libplplot.a \
                    ../lib/$(OS)$(ARCH)/libplplotcxx.a \
                    ../lib/$(OS)$(ARCH)/libqsastime.a \
                    ../lib/$(OS)$(ARCH)/libcsirocsa.a
   endif
            LIBS += -L/opt/X11/lib \
                    -lX11
   ifeq ($(PY), -DHAS_PYTHON)
#            LIBS += -L/Library/Frameworks/Python.framework/Versions/3.6/lib -lpython3.6
      INCPATH  += -I/Library/Frameworks/Python.framework/Versions/3.6/include/python3.6m -I/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/numpy/core/include
   endif

   else
      OS          = LINUX
      LDFLAG      = -static-libstdc++ -static-libgcc
      SED_INPLACE = sed -i''
      LIBS        = -rdynamic \
                    ../lib/$(OS)$(ARCH)/libxpa.a \
                    ../lib/$(OS)$(ARCH)/libfftw3.a \
                    ../lib/$(OS)$(ARCH)/libcpgplot.a \
                    ../lib/$(OS)$(ARCH)/libpgplot.a \
                    ../lib/$(OS)$(ARCH)/libf2c.a \
                    ../lib/$(OS)$(ARCH)/libz.a \
                    ../lib/$(OS)$(ARCH)/libpng.a \
                    ../lib/$(OS)$(ARCH)/libreadline.a \
                    ../lib/$(OS)$(ARCH)/libncurses.a
   ifeq ($(GDL), -DHAS_GDL)
	LIBS +=	          ../lib/$(OS)$(ARCH)/libgdl.a \
                    ../lib/$(OS)$(ARCH)/libplplotcxx.a \
                    ../lib/$(OS)$(ARCH)/libfftw3f.a \
                    ../lib/$(OS)$(ARCH)/libplplot.a \
                    ../lib/$(OS)$(ARCH)/libqsastime.a
   endif
          LIBS +=          ../lib/$(OS)$(ARCH)/libgsl.a \
                    ../lib/$(OS)$(ARCH)/libgslcblas.a \
                    ../lib/$(OS)$(ARCH)/libz.a

   ifeq ($(PY), -DHAS_PYTHON)
#      LIBS += /usr/lib/x86_64-linux-gnu/libpython3.5m.a -lexpat -lutil
      INCPATH  += -I/usr/include/python3.5m
   endif
       LIBS +=      -lX11 -ldl -lxcb -lpthread -lXdmcp -lXau

   endif
   TARDIR     := $(shell echo $(PWD) | sed 's|/dpuser$$||g' | sed 's|\/.*\/||g')
endif

################################################################################
# main targets
all: release 

##### RELEASE TARGETS #####
ifeq ($(OS), WIN) # start WIN
release:
	$(MAKE) static "TARGET_WIN = dpuser.exe"
else # start LINUX & MACOSX
release:
	$(MAKE) static "TARGET_LINUX = dpuser"
endif # end LINUX & MACOSX

##### DEBUG TARGETS #####
ifeq ($(OS), WIN) # start WIN
debug:
	$(MAKE) static "TARGET_WIN = dpuserD.exe" "CFLAGS = $(CFLAGS) -g -DDBG" "CXXFLAGS = $(CXXFLAGS) -g -DDBG"
else # start LINUX & MACOSX
debug:
	$(MAKE) static "TARGET_LINUX = dpuserD" "CFLAGS = $(CFLAGS) -g -DDBG" "CXXFLAGS = $(CXXFLAGS) -g -DDBG"
endif # end LINUX & MACOSX

################################################################################
# sub targets

ifeq ($(OS), WIN) # start WIN
static: revision parsers
	$(MAKE) $(MULTIPROZGEIL) $(TARGET_WIN)
else # end WIN, start LINUX & MACOSX
static: doc/helpmap.cpp revision parsers
	$(MAKE) $(MULTIPROZGEIL) $(TARGET_LINUX)
endif # end LINUX & MACOSX

$(TARGET_DLL): $(DLL_OBJECTS)
	g++ -shared -o $(TARGET_DLL) $(DLL_OBJECTS) $(DLL_LIBS)

$(TARGET_WIN): $(OBJECTS)
	$(LINK) $(LDFLAG) -o $(TARGET_WIN) $(OBJECTS) $(LIBS)

$(TARGET_LINUX): $(OBJECTS)
	$(LINK) $(LDFLAG) -o $(TARGET_LINUX) $(OBJECTS) $(LIBS)

# Builds parser-, dpuser2c & idl2c-source-files
parsers: parsers_qf
	$(MAKE) -C parser PLATFORM=$(OS)

parsers_qf:
	$(MAKE) -C mpfit PLATFORM=$(OS)

clean: clean_qf
	-$(RM) $(OBJECTS)
	$(if $(OS:WIN=), \
		-$(RM) dpuser dpuserD \
	, \
		-$(RM) dpuser.exe dpuserD.exe \
	)

# this target is called by QFitsView 'make clean'
clean_qf: FORCE
	$(MAKE) -C parser clean  PLATFORM=$(OS)
	$(MAKE) -C mpfit clean PLATFORM=$(OS)

FORCE:

################################################################################
# Updates the revision in parser/svn_revision.h
revision:
ifeq ($(OS), WIN)
	echo #include "svn_revision.h" > parser/svn_revision.cpp
	echo. >> parser/svn_revision.cpp
	echo char* GetRevString() { return "Rev. $(REV_NR)"; } >> parser/svn_revision.cpp
else
	@echo '#include "svn_revision.h"' > parser/svn_revision_new.cpp; \
	echo '' >> parser/svn_revision_new.cpp; \
	echo 'char* GetRevString() { return "Rev. $(REV_NR)"; }' >> parser/svn_revision_new.cpp; \
	cmp -s parser/svn_revision.cpp parser/svn_revision_new.cpp; \
	RETVAL=$$?; \
	if [ $$RETVAL -ne 0 ]; then \
		mv parser/svn_revision_new.cpp parser/svn_revision.cpp; \
	fi
endif
	@echo "Revision number: "$(REV_NR)


################################################################################
# Creates a distribution
ifeq ($(OS), WIN) # start WIN
dist:
	@echo "*************************************************************"
	@echo "ERROR: Distribution can only be generated under LINUX or MAC!"
	@echo "*************************************************************"
else # end WIN, start LINUX & MACOSX
dist: clean
	cd ../QFitsView && make -i clean
	make revision
	find .. -type d -name ".svn" -exec rm -rf {} +
	find .. -type d -name "obsolete" -exec rm -rf {} +
	find .. -regextype posix-extended -regex '.*\.(a|lib|dll|exe)' -exec rm {} +
	rm -rf ../include/*
	rm -rf ../bin/*
	rm -rf ../external/*z
	tar cfvz ../../dpuser-r$(REV_NR).tar.gz ../../$(TARDIR)
endif # end LINUX & MACOSX

################################################################################
# Compile rules
dpuser.o:              dpuser.cpp \
                       dpuser.h \
                       dpuser.yacchelper.h \
                       dpuser.procs.h \
                       procedures.h \
                       functions.h \
                       dpuser_utils.h \
                       dpuserType.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o dpuser.o dpuser.cpp

dpuser.input.o:        dpuser.input.cpp \
                       dpuser.h \
                       dpuserType.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o dpuser.input.o dpuser.input.cpp

procedures.o:          procedures.cpp \
                       dpuser.h \
                       procedures.h \
                       dpuser.procs.h \
                       dpuser_utils.h \
                       dpuser.yacchelper.h \
                       functions.h \
                       dpuserType.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o procedures.o procedures.cpp

dpuser.procs.o:        dpuser.procs.cpp \
                       dpuser.h \
                       functions.h \
                       dpuser_utils.h \
                       procedures.h \
                       dpuserType.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o dpuser.procs.o dpuser.procs.cpp

dpuser.yacchelper.o:    dpuser.yacchelper.cpp \
                        dpuser.h \
                        functions.h \
                        procedures.h \
                        dpuser_utils.h \
                        dpuserType.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o dpuser.yacchelper.o dpuser.yacchelper.cpp

functions.o:            functions.cpp \
                        dpuser.h \
                        functions.h \
                        procedures.h \
                        dpuser_utils.h \
                        dpuser.yacchelper.h \
                        dpuserType.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o functions.o functions.cpp

dpuser_utils.o:         dpuser_utils.cpp \
                        dpuser_utils.h \
                        dpuser.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o dpuser_utils.o dpuser_utils.cpp

lex.yy.o:               parser/lex.yy.cpp \
                        dpuser.h \
                        dpuser.yacchelper.h \
                        procedures.h \
                        functions.h \
                        dpuser_utils.h \
                        dpuserType.h \
                        parser/y.tab.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o lex.yy.o parser/lex.yy.cpp

y.tab.o:                parser/y.tab.cpp \
                        dpuser.h \
                        dpuser.yacchelper.h \
                        dpuser.procs.h \
                        procedures.h \
                        functions.h \
                        dpuser_utils.h \
                        dpuserType.h \
                        parser/svn_revision.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o y.tab.o parser/y.tab.cpp

svn_revision.o:         parser/svn_revision.cpp \
                        parser/svn_revision.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o svn_revision.o parser/svn_revision.cpp

dpuserType.o:           dpuserType.cpp \
                        dpuser.h \
                        dpuser_utils.h \
                        dpuserType.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o dpuserType.o dpuserType.cpp

y.mpfit.o:              mpfit/y.mpfit.cpp
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o y.mpfit.o mpfit/y.mpfit.cpp

lex.mpfit.o:            mpfit/lex.mpfit.cpp \
                        mpfit/y.tab.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o lex.mpfit.o mpfit/lex.mpfit.cpp

mpfitAST.o:             mpfit/mpfitAST.cpp \
                        mpfit/mpfitAST.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o mpfitAST.o mpfit/mpfitAST.cpp

cpg3d.o:                ../utils/cpg3d.c 
	$(CC) -c $(CFLAGS) $(INCPATH) -o cpg3d.o ../utils/cpg3d.c

dpstring.o:             ../utils/dpstring.cpp \
                        ../utils/dpstring.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o dpstring.o ../utils/dpstring.cpp

dpstringlist.o:         ../utils/dpstringlist.cpp \
                        ../utils/dpstringlist.h \
                        ../utils/dpstring.h \
                        ../utils/osdir/osdir.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o dpstringlist.o ../utils/dpstringlist.cpp

regex_sr.o:            ../utils/regex/regex_sr.cpp \
                       ../utils/regex/regex_sr.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o regex_sr.o ../utils/regex/regex_sr.cpp

mpfit.o:               ../utils/cmpfit/mpfit.cpp \
                       ../utils/cmpfit/mpfit.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o mpfit.o ../utils/cmpfit/mpfit.cpp

kabsch2d.o:            ../utils/kabsch/kabsch2d.cpp \
                       ../utils/kabsch/kabsch2d.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o kabsch2d.o ../utils/kabsch/kabsch2d.cpp

arithmetics.o:         arithmetics.cpp \
                       dpuserAST.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o arithmetics.o arithmetics.cpp

boolean.o:         boolean.cpp \
                       dpuserAST.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o boolean.o boolean.cpp

dpuserAST.o:           dpuserAST.cpp \
                       dpuserAST.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o dpuserAST.o dpuserAST.cpp

gdl_dpuser.o:           gdl_dpuser.cpp
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o gdl_dpuser.o gdl_dpuser.cpp

python_dpuser.o:        python_dpuser.cpp
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o python_dpuser.o python_dpuser.cpp

3d_stuff.o:             ../libfits/3d_stuff.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h \
                        ../libfits/platform.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o 3d_stuff.o ../libfits/3d_stuff.cpp

cube.o:                 ../libfits/cube.c \
                        ../libfits/cube.h
	$(CC) -c $(CFLAGS) $(INCPATH) -o cube.o ../libfits/cube.c

dpheader.o:             ../libfits/dpheader.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o dpheader.o ../libfits/dpheader.cpp

fits.o:                 ../libfits/fits.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h \
                        ../libfits/platform.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits.o ../libfits/fits.cpp

fits_cube.o:            ../libfits/fits_cube.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/platform.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits_cube.o ../libfits/fits_cube.cpp

fits_dpl.o:             ../libfits/fits_dpl.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h \
                        ../libfits/cube.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits_dpl.o ../libfits/fits_dpl.cpp

fits_exc.o:             ../libfits/fits_exc.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits_exc.o ../libfits/fits_exc.cpp

fits_file.o:            ../libfits/fits_file.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/platform.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits_file.o ../libfits/fits_file.cpp

fits_filters.o:         ../libfits/fits_filters.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits_filters.o ../libfits/fits_filters.cpp

fits_funcs.o:           ../libfits/fits_funcs.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits_funcs.o ../libfits/fits_funcs.cpp

fits_mem.o:             ../libfits/fits_mem.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits_mem.o ../libfits/fits_mem.cpp

fits_ops.o:             ../libfits/fits_ops.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h \
                        ../libfits/platform.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits_ops.o ../libfits/fits_ops.cpp

fits_procs.o:           ../libfits/fits_procs.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h \
                        ../libfits/platform.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits_procs.o ../libfits/fits_procs.cpp

fits_range.o:           ../libfits/fits_range.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits_range.o ../libfits/fits_range.cpp

fits_red.o:             ../libfits/fits_red.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h \
                        ../libfits/fitting.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits_red.o ../libfits/fits_red.cpp

JulianDay.o:            ../libfits/JulianDay.cpp \
                        ../libfits/JulianDay.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o JulianDay.o ../libfits/JulianDay.cpp

math_utils.o:           ../libfits/math_utils.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o math_utils.o ../libfits/math_utils.cpp

dpComplex.o:            ../libfits/dpComplex.cpp \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o dpComplex.o ../libfits/dpComplex.cpp

fitting.o:              ../libfits/fitting.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/fitting.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fitting.o ../libfits/fitting.cpp

voronoi.o:              ../libfits/voronoi.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o voronoi.o ../libfits/voronoi.cpp

fits_logic.o:           ../libfits/fits_logic.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fits_logic.o ../libfits/fits_logic.cpp

astrolib.o:             ../libfits/astrolib.cpp \
                        dpuser_utils.h \
                        dpuser.h \
                        ../libfits/astrolib.h \
                        ../libfits/fits.h \
                        ../libfits/dpComplex.h \
                        ../libfits/defines.h \
                        ../libfits/math_utils.h \
                        ../libfits/fitting.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o astrolib.o ../libfits/astrolib.cpp

helpmap.o:               doc/helpmap.cpp
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o helpmap.o doc/helpmap.cpp

ifeq ($(OS), LINUX)
doc/helpmap.cpp:	doc/functions_doc.h 	doc/procedures_doc.h

	cd doc && ./makec++.sh
else
doc/helpmap.cpp:
	@echo "*************************************************************"
	@echo "WARNING: Documentation can only be generated under LINUX!"
	@echo "*************************************************************"
endif
