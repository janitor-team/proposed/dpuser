#ifndef DPUSER_PROCS_H
#define DPUSER_PROCS_H

#include "dpuser.h"
#include <vector>
#include "dpuserType.h"

void print(dpuserType &v, int options);
void mem(void);

void plot(Fits *vv, Fits *v, float min, float max, long method, int closeorcontinue);
#ifdef HAS_PGPLOT
void surface(Fits *input, float angle, long skip);
void contour(Fits *v, Fits *levels);
void radialplot(Fits *v, long x, long y, long radius, long center);
void graymap(Fits *v, float min, float max, long itf);
void shade(Fits *v);
#endif /* HAS_PGPLOT */

void help(char *what);
void CreateHelpMaps();

extern std::map<std::string, std::vector<std::string> > functionHelp;
extern std::map<std::string, std::vector<std::string> > procedureHelp;

#endif /* DPUSER_PROCS_H */
