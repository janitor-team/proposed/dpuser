#!/bin/bash

function writemap {
IFS=''
start=-1
first=0
previous=""
cat $1 |
while read line
do
    previous1="${line//\"/\\\"}"
		previous2="${previous1/<code>/}"
		previous3="${previous2/function_/function }"
		previous4="${previous3/procedure_/procedure }"
		previous="${previous4/category_/category }"
    if [ $start == 1 ]
		then
		    echo "{" >> helpmap.cpp
		    echo "{ \"$line\" }, {" >> helpmap.cpp
				start=0
				first=1
    elif [ "$line" == "/*!" ]
		then
		    start=1
	  elif [ "$line" == "*/" ]
		then
		    echo "" >> helpmap.cpp
		    echo "}" >> helpmap.cpp
		    echo "}," >> helpmap.cpp
		    start=-1
		elif [ $start == 0 ]
		then
		    if [ $first == 0 ]
				then
				    echo "," >> helpmap.cpp
				fi
				first=0
		    echo -ne "{ \"$previous\" }" >> helpmap.cpp
	  fi
done
truncate -s-2 helpmap.cpp
echo "" >> helpmap.cpp
echo "};" >> helpmap.cpp
}

echo "#include <vector>" > helpmap.cpp
echo "#include <string>" >> helpmap.cpp
echo "#include <map>" >> helpmap.cpp
echo "std::map<std::string, std::vector<std::string> > functionHelp = {" >> helpmap.cpp
echo "" >> helpmap.cpp
cat "pgplot_functions.cpp" >> helpmap.cpp

writemap functions_doc.h
echo "" >> helpmap.cpp
echo "std::map<std::string, std::vector<std::string> > procedureHelp = {" >> helpmap.cpp
cat "pgplot_procedures.cpp" >> helpmap.cpp

writemap procedures_doc.h
