echo "char *help_funcs_map[] = {" > helpmap.h
sed -n 's/\"/\\"/g;/\/\*\!/,/\*\//p' ../dpuser2c/functions.cpp | awk {'if ($1!="/*!") if ($1=="*/") print "\"\","; else {if ($1 == "") print "\" \","; else print "\""$0"\","}'} >> helpmap.h
echo "\"\"};" >> helpmap.h
echo "char *help_pg_map[] = {" >> helpmap.h
sed -n 's/\"/\\"/g;/\/\*\!/,/\*\//p' ../dpuser2c/procedures.cpp | awk {'if ($1!="/*!") if ($1=="*/") print "\"\","; else {if ($1 == "") print "\" \","; else print "\""$0"\","}'} >> helpmap.h
echo "\"\"};" >> helpmap.h
