/****************************************************************************
** dpuserOutput meta object code from reading C++ file 'qtdpuser.h'
**
** Created: Thu Aug 24 13:17:39 2006
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.5   edited Sep 2 14:41 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "qtdpuser.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *dpuserOutput::className() const
{
    return "dpuserOutput";
}

QMetaObject *dpuserOutput::metaObj = 0;
static QMetaObjectCleanUp cleanUp_dpuserOutput( "dpuserOutput", &dpuserOutput::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString dpuserOutput::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "dpuserOutput", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString dpuserOutput::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "dpuserOutput", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* dpuserOutput::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QTextEdit::staticMetaObject();
    metaObj = QMetaObject::new_metaobject(
	"dpuserOutput", parentObject,
	0, 0,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_dpuserOutput.setMetaObject( metaObj );
    return metaObj;
}

void* dpuserOutput::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "dpuserOutput" ) )
	return this;
    return QTextEdit::qt_cast( clname );
}

bool dpuserOutput::qt_invoke( int _id, QUObject* _o )
{
    return QTextEdit::qt_invoke(_id,_o);
}

bool dpuserOutput::qt_emit( int _id, QUObject* _o )
{
    return QTextEdit::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool dpuserOutput::qt_property( int id, int f, QVariant* v)
{
    return QTextEdit::qt_property( id, f, v);
}

bool dpuserOutput::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES


const char *qtdpuserInput::className() const
{
    return "qtdpuserInput";
}

QMetaObject *qtdpuserInput::metaObj = 0;
static QMetaObjectCleanUp cleanUp_qtdpuserInput( "qtdpuserInput", &qtdpuserInput::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString qtdpuserInput::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "qtdpuserInput", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString qtdpuserInput::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "qtdpuserInput", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* qtdpuserInput::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QLineEdit::staticMetaObject();
    static const QUMethod signal_0 = {"tabPressed", 0, 0 };
    static const QUMethod signal_1 = {"upPressed", 0, 0 };
    static const QUMethod signal_2 = {"downPressed", 0, 0 };
    static const QMetaData signal_tbl[] = {
	{ "tabPressed()", &signal_0, QMetaData::Protected },
	{ "upPressed()", &signal_1, QMetaData::Protected },
	{ "downPressed()", &signal_2, QMetaData::Protected }
    };
    metaObj = QMetaObject::new_metaobject(
	"qtdpuserInput", parentObject,
	0, 0,
	signal_tbl, 3,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_qtdpuserInput.setMetaObject( metaObj );
    return metaObj;
}

void* qtdpuserInput::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "qtdpuserInput" ) )
	return this;
    return QLineEdit::qt_cast( clname );
}

// SIGNAL tabPressed
void qtdpuserInput::tabPressed()
{
    activate_signal( staticMetaObject()->signalOffset() + 0 );
}

// SIGNAL upPressed
void qtdpuserInput::upPressed()
{
    activate_signal( staticMetaObject()->signalOffset() + 1 );
}

// SIGNAL downPressed
void qtdpuserInput::downPressed()
{
    activate_signal( staticMetaObject()->signalOffset() + 2 );
}

bool qtdpuserInput::qt_invoke( int _id, QUObject* _o )
{
    return QLineEdit::qt_invoke(_id,_o);
}

bool qtdpuserInput::qt_emit( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->signalOffset() ) {
    case 0: tabPressed(); break;
    case 1: upPressed(); break;
    case 2: downPressed(); break;
    default:
	return QLineEdit::qt_emit(_id,_o);
    }
    return TRUE;
}
#ifndef QT_NO_PROPERTIES

bool qtdpuserInput::qt_property( int id, int f, QVariant* v)
{
    return QLineEdit::qt_property( id, f, v);
}

bool qtdpuserInput::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES


const char *qtdpuser::className() const
{
    return "qtdpuser";
}

QMetaObject *qtdpuser::metaObj = 0;
static QMetaObjectCleanUp cleanUp_qtdpuser( "qtdpuser", &qtdpuser::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString qtdpuser::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "qtdpuser", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString qtdpuser::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "qtdpuser", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* qtdpuser::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QWidget::staticMetaObject();
    static const QUMethod slot_0 = {"appendHistory", 0, 0 };
    static const QUMethod slot_1 = {"fileNameCompletion", 0, 0 };
    static const QUMethod slot_2 = {"historyUp", 0, 0 };
    static const QUMethod slot_3 = {"historyDown", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "appendHistory()", &slot_0, QMetaData::Public },
	{ "fileNameCompletion()", &slot_1, QMetaData::Public },
	{ "historyUp()", &slot_2, QMetaData::Public },
	{ "historyDown()", &slot_3, QMetaData::Public }
    };
    static const QUParameter param_signal_0[] = {
	{ 0, &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod signal_0 = {"statusbartext", 1, param_signal_0 };
    static const QUMethod signal_1 = {"dpuserExited", 0, 0 };
    static const QMetaData signal_tbl[] = {
	{ "statusbartext(const QString&)", &signal_0, QMetaData::Public },
	{ "dpuserExited()", &signal_1, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"qtdpuser", parentObject,
	slot_tbl, 4,
	signal_tbl, 2,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_qtdpuser.setMetaObject( metaObj );
    return metaObj;
}

void* qtdpuser::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "qtdpuser" ) )
	return this;
    return QWidget::qt_cast( clname );
}

// SIGNAL statusbartext
void qtdpuser::statusbartext( const QString& t0 )
{
    activate_signal( staticMetaObject()->signalOffset() + 0, t0 );
}

// SIGNAL dpuserExited
void qtdpuser::dpuserExited()
{
    activate_signal( staticMetaObject()->signalOffset() + 1 );
}

bool qtdpuser::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: appendHistory(); break;
    case 1: fileNameCompletion(); break;
    case 2: historyUp(); break;
    case 3: historyDown(); break;
    default:
	return QWidget::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool qtdpuser::qt_emit( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->signalOffset() ) {
    case 0: statusbartext((const QString&)static_QUType_QString.get(_o+1)); break;
    case 1: dpuserExited(); break;
    default:
	return QWidget::qt_emit(_id,_o);
    }
    return TRUE;
}
#ifndef QT_NO_PROPERTIES

bool qtdpuser::qt_property( int id, int f, QVariant* v)
{
    return QWidget::qt_property( id, f, v);
}

bool qtdpuser::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
