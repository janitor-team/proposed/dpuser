%option noyywrap
%{
#include <cstdlib>
#include <string>
#include "mpfitAST.h"
#include "dpuser_utils.h"

int mpfitparse();
void mpfiterror(const char *s);
void initialize_mpfit_functions(void);

#include "y.tab.h"
%}
dig [0-9]

asc [a-zA-Z0-9]
spa [ \t]
ascspa {asc}|{spa}

num1 {dig}+\.?([eEdD][-+]?{dig}+)?
num2 {dig}*\.{dig}+([eEdD][-+]?{dig}+)?
number {num1}|{num2}


%%

[0-9]+      {
                yylval.i = atof(yytext);
                return REAL;
            }

([0-9]+\.?([eEdD][-+]?[0-9]+)?)|([0-9]*\.[0-9]+([eEdD][-+]?[0-9]+)?)    {
								for (int _c_ = 0; _c_ < strlen(yytext); _c_++) {
									if (yytext[_c_] == 'd') yytext[_c_] = 'e';
									else if (yytext[_c_] == 'D') yytext[_c_] = 'E';
								}
								yylval.i = atof(yytext);
								return REAL;
						}

[_[:alpha:]][_[:alnum:]]*" "*"("       {
                              yylval.s = new std::string(yytext);
															return FUNC;
								 }


[_[:alpha:]][_[:alnum:]]*     yylval.s = new std::string(yytext);  return VAR;
[-+*/=^,;()]                      return *yytext;
\n                         return ENTER ; 
.                          yyerror("Unknown character"); /* ignore all the rest */
%%

void mpfiterror(const char *s) {
/*	dp_output("%s\n", s); */
	throw dpuserTypeException(const_cast<char*>(s));
}

int main_mpfit(std::string line) {
	initialize_mpfit_functions();
  mpfit_sorted_variables.clear();
	mpfitAST_dummy.clear();
		yy_scan_string(line.c_str());
		mpfitparse();
	return 0;
}
