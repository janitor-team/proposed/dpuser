function coordstring, ra, dec {
/*
; create a nice string of celestial coordinates.
; ra and dec should be given in radians.
*/
  sign = dec < 0 ? "-" : "+"
  adec = abs(dec)
  rad = rad2deg(ra) / 15
  rah = int(rad)
  rad = (rad-rah) * 60
  ram = int(rad)
  rad = (rad-ram) * 60
  de = rad2deg(adec)
  deh = int(de)
  de = (de-deh) * 60
  dem = int(de)
  de = (de-dem) * 60

  coordstring = "RA = " + sprintf("%2.0fh ",rah) + sprintf("%2.0fm ", ram) + sprintf("%5.2fs", rad) + ", DEC = " + sign + sprintf("%2.0fd ", deh) + sprintf("%2.0f' ", dem) + sprintf("%4.1f\"", de)
}
