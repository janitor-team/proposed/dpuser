procedure writepng, filename, array {
	n1 = naxis1(array)
	n2 = naxis2(array)
	if (n1 > 1024 || n2 > 1024) {
		print "ERROR - the current implementation of writepng can only create PNG's smaller than 1024x1024 pixels"
	} else {	
		width = getenv("PGPLOT_PNG_WIDTH")
		height = getenv("PGPLOT_PNG_HEIGHT")
		setenv "PGPLOT_PNG_WIDTH", string(n1)
		setenv "PGPLOT_PNG_HEIGHT", string(n2)
	
		pgbeg 0, filename+"/png", 1, 1
		pgswin 1, n1, 1, n2
		pgsvp 0, 1, 0, 1
		pggray array, n1, n2, 1, n1, 1, n2, 255, 0, [0, 1, 0, 0, 0, 1]
		pgend
	
		setenv "PGPLOT_PNG_WIDTH", width
		setenv "PGPLOT_PNG_HEIGHT", height
	}
}
