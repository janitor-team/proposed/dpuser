procedure precess, rah, ram, ras, decd, decm, decs, equinox1, equinox2 {
/*
;+
; NAME:
;      PRECESS
; PURPOSE:
;      Precess coordinates from EQUINOX1 to EQUINOX2.  
; EXPLANATION:
;      For interactive display, one can use the procedure ASTRO which calls 
;      PRECESS or use the /PRINT keyword.   The default (RA,DEC) system is 
;      FK5 based on epoch J2000.0 but FK4 based on B1950.0 is available via 
;      the /FK4 keyword.
;
;      Use BPRECESS and JPRECESS to convert between FK4 and FK5 systems
; CALLING SEQUENCE:
;      PRECESS, ra, dec, equinox1, equinox2
;      PRECESS, rah, ram, ras, decd, decm, decs, equinox1, equinox2
;
; INPUT - OUTPUT:
;      RA - Input right ascension (scalar or vector) in DEGREES, unless the 
;              /RADIAN keyword is set
;      DEC - Input declination in DEGREES (scalar or vector), unless the 
;              /RADIAN keyword is set
;              
;      The input RA and DEC are modified by PRECESS to give the 
;      values after precession.
;
; INPUTS:
;      EQUINOX1 - Original equinox of coordinates, numeric scalar.  If 
;               omitted, then PRECESS will query for EQUINOX1 and EQUINOX2.
;      EQUINOX2 - Equinox of precessed coordinates.
;
;
; RESTRICTIONS:
;       Accuracy of precession decreases for declination values near 90 
;       degrees.  PRECESS should not be used more than 2.5 centuries from
;       2000 on the FK5 system (1950.0 on the FK4 system).
;
; EXAMPLES:
;       (1) The Pole Star has J2000.0 coordinates (2h, 31m, 46.3s, 
;               89d 15' 50.6"); compute its coordinates at J1985.0
;
;    DPUSER> precess 2,31,46.3,89,15,50.6, 2000, 1985
;
;               ====> 2h 16m 22.73s, 89d 11' 47.3"
;
;       (2) Precess the B1950 coordinates of Eps Ind (RA = 21h 59m,33.053s,
;       DEC = (-56d, 59', 33.053") to equinox B1975.
;
;     DPUSER> ra = deg2rad(ten(21, 59, 33.053)*15)
;     DPUSER> dec = deg2rad(ten(-56, 59, 33.053))
;     DPUSER> precess ra, dec ,1950, 1975
;
; PROCEDURE:
;       Algorithm from Computational Spherical Astronomy by Taff (1983), 
;       p. 24. (FK4). FK5 constants from "Astronomical Almanac Explanatory
;       Supplement 1992, page 104 Table 3.211.1.
;
; PROCEDURE CALLED:
;       Function PREMAT - computes precession matrix 
;
; REVISION HISTORY
;       Written, Wayne Landsman, STI Corporation  August 1986
;       Correct negative output RA values   February 1989
;       Added /PRINT keyword      W. Landsman   November, 1991
;       Provided FK5 (J2000.0)  I. Freedman   January 1994
;       Precession Matrix computation now in PREMAT   W. Landsman June 1994
;       Added /RADIAN keyword                         W. Landsman June 1997
;       Converted to IDL V5.0   W. Landsman   September 1997
;       Correct negative output RA values when /RADIAN used    March 1999  
;       Converted to DPUSER     T. Ott    Mai 2002
;-    
*/
  if (nparams == 8) {
    ra = deg2rad(ten(rah, ram, ras) * 15)
    dec = deg2rad(ten(decd, decm, decs))
  } else {
    if (nparams == 4) {
      ra = rah
      dec = ram
      equinox1 = ras
      equinox2 = decd
    }
  }
  if (nparams == 4 || nparams == 8) {
  deg_to_rad = pi()/180

  ra_rad = ra
  dec_rad = dec

  a = cos( dec_rad )  
  
  x = doublearray(1,3)
  
  x[1,1] = a*cos(ra_rad)
  x[1,2] = a*sin(ra_rad)
  x[1,3] = sin(dec_rad)
  x = transpose(x)

  sec_to_rad = deg_to_rad/3600

// Use PREMAT function to get precession matrix from Equinox1 to Equinox2
  r = premat(equinox1, equinox2)
  x2 = r#x      // rotate to get output direction cosines
  ra_rad = atan(x2[2],x2[1])
  dec_rad = asin(x2[3])
  ra = ra_rad ; dec = dec_rad
  if (ra < 0) ra += 2.0*pi()

  print "Precessed coords are: " + coordstring(ra, dec)
  } else {
    print "precess: must be either 4 or 8 arguments"
  }
}
