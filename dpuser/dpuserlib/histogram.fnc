function histogram, array, binsize {
  min = min(array)
  max = max(array)
  n = (max - min) / binsize
  result = longarray(n)
  narray = nelements(array)
  for i = 1, n {
    binmin = min + (i-1) * binsize
    binmax = min + i * binsize
    for j=1, narray {
      if ((array[j] >= binmin) && (array[j] < binmax)) result[j]++
    }
  }
  histogram = result
}

  
