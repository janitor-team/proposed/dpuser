@luciedgedetect.fnc
// image:    image to calibrate in spatial direction
// ycalpars: [1]:  lower range for bottom edge
//           [2]:  upper range for bottom edge
//           [3]:   lower range for upper edge
//           [4]:  upper range for upper edge
//           [5]:  height of calibrated longslit
// edgepars: [1]:  number of pixels to omit at left end
//           [2]:  number of pixels to omit at right end
//           [3]:  size of moving window to median
//           [4]:  order of polynomial to fit locally to the edge (in the moving window)
//           [5]:  order of the polynomial to fit to the whole edge
//           [6]:  sigma-factor for rejection
function lucicaly, image, ycalpars, edgepars {
    bottomlo = int(ycalpars[1])
    bottomhi = int(ycalpars[2])
    upperlo = int(ycalpars[3])
    upperhi = int(ycalpars[4])
    newheight = int(ycalpars[5])

    imagewidth = naxis1(image)
    imageheight = naxis2(image)

    // detect edges
    parlo=luciedgedetect(image, bottomlo, bottomhi, edgepars)
    parhi=luciedgedetect(image, upperlo, upperhi, edgepars)

    // evaluate polynomials at edges
    xline = [1:imagewidth]
    edgelo = poly(xline,parlo)
    edgehi = poly(xline,parhi)

    // calculate distance between edges at beginning and end

    // fit a line in y-direction through the two points defined
    // by the two edges
    yline = [1:imageheight]
    lucicaly = fits(imagewidth, imageheight)
    id = [1, newheight]
    for x=1,imagewidth {
       p = polyfitxy([edgelo[x], edgehi[x]], id, 1)
       lucicaly[x,*] = poly(yline, p[*,1])
    }

    print "FINISHED CALIBRATING IN Y"
}
