// midline: signal across longslit to extract line candidates in
// sigma:   sigma factor for thresholding
// omitpixels: number of pixels to omit at left and right end of midline
// maxlines: maximum number of lines to expect (200 should be enough)
function lucilinedetect, midline, sigma , omitpixels, maxlines{
    thresh = median(midline) + sigma*meddev(midline)

    // get local maxima on midline (omit first and last pixels)
    lucilinedetect = [1:maxlines]
    nrfound = 0
    for i = omitpixels, nelements(midline)-omitpixels {
        if (midline[i] > thresh && midline[i] > midline[i-1] && midline[i] > midline[i+1]) {
            nrfound++
            lucilinedetect[nrfound] = i
        }
    }
    lucilinedetect = lucilinedetect[1:nrfound]
    
    setfitskey lucilinedetect, "OMITPIX", omitpixels, "No. of pixels to omit at each end of spectrum"

    print "FINISHED LINE DETECTION"
}
