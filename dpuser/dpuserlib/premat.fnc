function premat, equinox1, equinox2 {
/*
;+
; NAME:
;       PREMAT
; PURPOSE:
;       Return the precession matrix needed to go from EQUINOX1 to EQUINOX2.  
; EXPLANTION:
;       This matrix is used by the procedures PRECESS and BARYVEL to precess 
;       astronomical coordinates
;
; CALLING SEQUENCE:
;       matrix = PREMAT( equinox1, equinox2, [ /FK4 ] )
;
; INPUTS:
;       EQUINOX1 - Original equinox of coordinates, numeric scalar.  
;       EQUINOX2 - Equinox of precessed coordinates.
;
; OUTPUT:
;      matrix - double precision 3 x 3 precession matrix, used to precess
;               equatorial rectangular coordinates
;
; OPTIONAL INPUT KEYWORDS:
;       /FK4   - If this keyword is set, the FK4 (B1950.0) system precession
;               angles are used to compute the precession matrix.   The 
;               default is to use FK5 (J2000.0) precession angles
;
; EXAMPLES:
;       Return the precession matrix from 1950.0 to 1975.0 in the FK4 system
;
;       IDL> matrix = PREMAT( 1950.0, 1975.0, /FK4)
;
; PROCEDURE:
;       FK4 constants from "Computational Spherical Astronomy" by Taff (1983), 
;       p. 24. (FK4). FK5 constants from "Astronomical Almanac Explanatory
;       Supplement 1992, page 104 Table 3.211.1.
;
; REVISION HISTORY
;       Written, Wayne Landsman, HSTX Corporation, June 1994
;       Converted to IDL V5.0   W. Landsman   September 1997
;-
*/
  deg_to_rad = pi()/180
  sec_to_rad = deg_to_rad/3600
  T = 0.001*( equinox2 - equinox1)
  ST = 0.001*( equinox1 - 2000)
//  Compute 3 rotation angles
  A = sec_to_rad * T * (23062.181 + ST*(139.656 +0.0139*ST) + T*(30.188 - 0.344*ST+17.998*T))
  B = sec_to_rad * T * T * (79.280 + 0.410*ST + 0.205*T) + A
  C = sec_to_rad * T * (20043.109 - ST*(85.33 + 0.217*ST) + T*(-42.665 - 0.217*ST -41.833*T))
  sina = sin(A) ;  sinb = sin(B)  ; sinc = sin(C)
  cosa = cos(A) ;  cosb = cos(B)  ; cosc = cos(C)
  r = doublearray(3,3)
  r[*,1] = [ cosa*cosb*cosc-sina*sinb, sina*cosb+cosa*sinb*cosc,  cosa*sinc]
  r[*,2] = [-cosa*sinb-sina*cosb*cosc, cosa*cosb-sina*sinb*cosc, -sina*sinc]
  r[*,3] = [-cosb*sinc, -sinb*sinc, cosc]
  premat = r
}
