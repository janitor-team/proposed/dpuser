procedure cirrange, ang, rad {
//  Determine the additive constant.

 if (nparams == 2) {
  cnst = 2*pi()
 } else {
  cnst = 360
 }

// Deal with the lower limit.
 if (nelements(ang) == 1) {
  ang = ang % cnst
  if (ang < 0) ang += cnst
 } else {
  for i=1, nelements(ang) {
    ang[i] = ang[i] % cnst
// Deal with negative values, if any
    if (ang[i] < 0) ang[i] += cnst
  }
  }
}
