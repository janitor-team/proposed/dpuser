procedure nutate, jd, nut_long, nut_obliq {
 dtor = pi()/180
//  form time in Julian centuries from 1900.0

 T = (jd - 2451545.0)/36525.0


// Mean elongation of the Moon

   coeff1 = [297.85036,  445267.111480, -0.0019142, 1/189474 ]
  d = poly(T, coeff1)*dtor
  cirrange d,/rad

// Sun's mean anomaly

   coeff2 = [357.52772, 35999.050340, -0.0001603, -1/3d5 ]
   M = poly(T,coeff2)*dtor
   cirrange M,/rad

// Moon's mean anomaly

   coeff3 = [134.96298, 477198.867398, 0.0086972, 1.0/5.625d4 ]
   Mprime = poly(T,coeff3)*dtor
   cirrange Mprime,/rad

// Moon's argument of latitude

    coeff4 = [93.27191, 483202.017538, -0.0036825, -1.0/3.27270d5 ]
    F = poly(T, coeff4 )*dtor 
    cirrange F,/rad

// Longitude of the ascending node of the Moon's mean orbit on the ecliptic,
//  measured from the mean equinox of the date

  coeff5 = [125.04452, -1934.136261, 0.0020708, 1./4.5d5]
  omega = poly(T, coeff5)*dtor
  cirrange omega,/rad

 d_lng = [0,-2,0,0,0,0,-2,0,0,-2,-2,-2,0,2,0,2,0,0,-2,0,2,0,0,-2,0,-2,0,0,2,-2,0,-2,0,0,2,2,0,-2,0,2,2,-2,-2,2,2,0,-2,-2,0,-2,-2,0,-1,-2,1,0,0,-1,0,0,2,0,2]

 m_lng = [0,0,0,0,1,0,1,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,2,1,0,-1,0,0,0,1,1,-1,0,0,0,0,0,0,-1,-1,0,0,0,1,0,0,1,0,0,0,-1,1,-1,-1,0,-1]

 mp_lng = [0,0,0,0,0,1,0,0,1,0,1,0,-1,0,1,-1,-1,1,2,-2,0,2,2,1,0,0,-1,0,-1,0,0,1,0,2,-1,1,0,1,0,0,1,2,1,-2,0,1,0,0,2,2,0,1,1,0,0,1,-2,1,1,1,-1,3,0]

 f_lng = [0,2,2,0,0,0,2,2,2,2,0,2,2,0,0,2,0,2,0,2,2,2,0,2,2,2,2,0,0,2,0,0,0,-2,2,2,2,0,2,2,0,2,2,0,0,0,2,0,2,0,2,-2,0,0,0,2,2,0,0,2,2,2,2]

 om_lng = [1,2,2,2,0,0,2,1,2,2,0,1,2,0,1,2,1,1,0,1,2,2,0,2,0,0,1,0,1,2,1,1,1,0,1,2,2,0,2,1,0,2,1,1,1,0,1,1,1,1,1,0,0,0,0,0,2,0,0,2,2,2,2]

 sin_lng = [-171996, -13187, -2274, 2062, 1426, 712, -517, -386, -301, 217,-158, 129, 123, 63, 63, -59, -58, -51, 48, 46, -38, -31, 29, 29, 26, -22,21, 17, 16, -16, -15, -13, -12, 11, -10, -8, 7, -7, -7, -7,6,6,6,-6,-6,5,-5,-5,-5,4,4,4,-4,-4,-4,3,-3,-3,-3,-3,-3,-3,-3 ]
 
 sdelt = [-174.2, -1.6, -0.2, 0.2, -3.4, 0.1, 1.2, -0.4, 0, -0.5, 0, 0.1,0,0,0.1, 0,-0.1,0,0,0,0,0,0,0,0,0,0, -0.1, 0, 0.1] 
 sdelt = resize(sdelt, nelements(sdelt) + 33, 1)

 cos_lng = [ 92025, 5736, 977, -895, 54, -7, 224, 200, 129, -95,0,-70,-53,0,-33, 26, 32, 27, 0, -24, 16,13,0,-12,0,0,-10,0,-8,7,9,7,6,0,5,3,-3,0,3,3,0,-3,-3,3,3,0,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

 cdelt = [8.9, -3.1, -0.5, 0.5, -0.1, 0.0, -0.6, 0.0, -0.1, 0.3]
 cdelt = resize(cdelt, nelements(cdelt) + 53, 1)


// Sum the periodic terms 

 n = nelements(jd)
 nut_long = doublearray(n)
 nut_obliq = doublearray(n)

 arg = d_lng#reform(d,1,naxis1(d)) \
     + m_lng#reform(M,1,naxis1(M)) \
	 + mp_lng#reform(Mprime,1,naxis1(Mprime)) \
	 + f_lng#reform(F,1,naxis1(F)) \
	 + om_lng#reform(omega,1,naxis1(omega))
// arg = d_lng*d + m_lng*M +mp_lng*Mprime + f_lng*F +om_lng*omega
 sarg = sin(arg)
 carg = cos(arg)
 
 if (n == 1) {
        nut_long =  0.0001*total( (sdelt*T + sin_lng)*sarg )
        nut_obliq = 0.0001*total( (cdelt*T + cos_lng)*carg )
 } else {
 for i=1,n {
        nut_long[i] =  0.0001*total( (sdelt*T[i] + sin_lng)*sarg[*,i] )
        nut_obliq[i] = 0.0001*total( (cdelt*T[i] + cos_lng)*carg[*,i] )
 }
 }
}
