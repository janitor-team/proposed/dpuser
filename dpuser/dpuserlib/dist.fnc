run "idl_compat.dpuser"

//Return a rectangular array in which each pixel = euclidian
//distance from the origin.

function dist, n, m {
  n1 = n
  m1 = (nparams == 2 ? m : n)
  x = [0:n1-1] // Make a row

  for i=1, nelements(x) {
    x[i] = min([x[i], n1-x[i]])^2 // column squares
  }

  dist = floatarray(n1,m1)           // Make array

  for i=0, m1/2 {                 // Row loop
    y = sqrt(x + i^2)             // Euclidian distance
    dist[*,i+1] = y                  // Insert the row
    if (i != 0) dist[*, m1-i+1] = y  // Symmetrical
  }
}

function dist1, n, m {
  oldbase = indexbase()
  cnotation

  n1 = n
  m1 = (nparams == 2 ? m : n)

  x=findgen(n1)		// Make a row

  for i=0, nelements(x)-1 {
    x[i] = min([x[i], n1-x[i]])^2 // column squares
  }

  a = fltarr(n1,m1)	// Make array

  for i=0, m1/2 do begin	// Row loop
	y = sqrt(x + i^2.) // Euclidian distance
	a[*,i] = y	// Insert the row
	if (i != 0) a[*, m1-i] = y // Symmetrical
  endfor

  dist1 = a

  setindexbase(oldbase)
}
