#ifndef DPUSER_H
#define DPUSER_H

#ifdef WIN
#pragma warning (disable: 4786) // disable warning for STL maps
#endif /* WIN */

#include <map>

#include "dpstring.h"

class dpStringList;
class dpuserTypeList;
class dpComplex;
class Fits;

#define DP_VERSION "4.0"
#define DPUSERVERSION "DPUSER - The Next Generation "
#define DPUSERVERSION2 " ____  ____  _   _ ____  _____ ____  \n|  _ \\|  _ \\| | | / ___|| ____|  _ \\ \n| | | | |_) | | | \\___ \\|  _| | |_) |\n| |_| |  __/| |_| |___) | |___|  _ < \n|____/|_|    \\___/|____/|_____|_| \\_\\ - The Next Generation "

// BEGIN PARSE FOR DPUSER2C

/*
typedef enum { typeUnknown = 0, typeCon = 0x01, typeId = 0x02, typeOpr = 0x04,
typeDbl = 0x08, typeFnc = 0x10, typeStr = 0x20, typeCom = 0x40,
typeFits = 0x80, typeFitsFile = 0x100, typeRng = 0x200, typePgplot = 0x400,
typeStrarr = 0x800, typeDpArr = 0x1000 } nodeEnum;
*/

extern int npgwords;
extern int nfncpro;
extern int looplock;
int keyWord(char *s);
void dpoutline(const char *);
void dpoutput(const char *);
void checkQFitsViewDisplay(int id);
int isStringAVariable(const char *);

#endif /* DPUSER_H */
