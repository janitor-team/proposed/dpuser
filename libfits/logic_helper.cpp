/*
 * Helper file for repetitive actions when comparing a FITS
 *
 * In order to use this, you have
 * #define LOGICACTION
 * and then
 * #include "logic_helper.cpp"
 *
 * The result will be stored in *this, which will be of type I1.
 * The first argument is a, the second b
 *
 */

dpint64 _x1, _y1, _z1;
dpint64 _x, _y, _z, _index;
bool _success = TRUE;

_x1 = a.Naxis(1);
_y1 = a.Naxis(2);
_z1 = a.Naxis(3);

#ifdef ARRAYS
if (b.Naxis(1) < _x1) _x1 = b.Naxis(1);
if (b.Naxis(2) < _y1) _y1 = b.Naxis(2);
if (b.Naxis(3) < _z1) _z1 = b.Naxis(3);
#endif /* ARRAYS */

if (_x1 < 1) _x1 = 1;
if (_y1 < 1) _y1 = 1;
if (_z1 < 1) _z1 = 1;

_success = create(_x1, _y1, _z1, I1);

if (_success) {
  _index = 0;

  for (_x = 0; _x < _x1; _x++) {
    for (_y = 0; _y < _y1; _y++) {
      for (_z = 0; _z < _z1; _z++) {
#ifdef ARRAYS
        if (a[a.C_I(_x, _y, _z)] LOGICACTION b[b.C_I(_x, _y, _z)]) i1data[_index] = 1;
#else
        if (a[_index] LOGICACTION b) i1data[_index] = 1;
#endif /* ARRAYS */
			  _index++;
		  }
	  }
  }
}
#undef ARRAYS

