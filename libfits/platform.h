/*********************************************************************
 * FILE:         platform.h
 * PURPOSE:      platform specific stuff
 *********************************************************************/


#ifndef PLATFORM_H
#define PLATFORM_H

/* for Microsoft Windows, include and define some stuff */

#ifdef WIN
#include <io.h>
#include <float.h>
#include <direct.h>

//#define isatty _isatty
#define strncasecmp _strnicmp
//#define finite _finite
//#define isnan _isnan
#define chdir _chdir
//#define getcwd _getcwd
//#ifdef _WINDOWS // only for Visual C++ 6.0
//#define vsnprintf _vsnprintf
//#endif

#endif /* WIN */

#endif /* PLATFORM_H */
