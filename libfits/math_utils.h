/***************************************************************************
                          math_utils.h  -  description
                             -------------------
    begin                : Wed Apr 19 2000
    copyright            : (C) 2000 by Thomas Ott
    email                : ott@mpe.mpg.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 * file: utils/math_utils.h
 * Purpose: some useful math routines not available in C - header file
 * Author: Thomas Ott
 *
 * History: 30.06.1999: file created
 */

#ifndef MATH_UTILS_H
#define MATH_UTILS_H

#include <math.h>
#include "defines.h"

#ifndef M_PI
#define M_PI (2.0*acos(0.0))
#endif

#define TWO_SQRT_LN2 2.0*sqrt(log(2.0))
#define FOUR_LN2 4.0*log(2.0)
#define EPS 1e-30
#define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr

int nint(double x);
double frac(double x);
int floatcmp(const void *a, const void *b);
double cosdeg(double x);
double sindeg(double x);
double rad2deg(double x);
double deg2rad(double x);
double sqr(double x);

#ifdef WIN
double asinh(double x);
double acosh(double x);
double atanh(double x);
#ifdef _WINDOWS // only for Visual C++ 6.0
extern "C" double rint(double x);
#endif
#endif /* WIN */

double lsqfit(double **A, double *S, int n, int m);
int trans_matrix(int ncoords, int nused, double *xref, double *yref, double *xim, double *yim, double *SX, double *SY, double *xerror, double *yerror);
int trans_matrix_errors(int ncoords, int nused, double *xref, double *yref, double *xim, double *yim, double *SX, double *SY, double *xerror, double *yerror);
double sinc(double x);
double cosc(double x);

#endif /* MATH_UTILS_H */
