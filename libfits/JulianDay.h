/******************************************************************************/
/*                                                                            */
/* JulianDay.h                                                                */
/* Klassendefinition der Klasse CJulianDay zur Verarbeitung von               */
/* JulianDay-Zeitangaben                                                      */
/*                                                                            */
/* HINWEIS: Die Klasse CJulianDay arbeitet stets mit _vollst�ndigem_          */
/*          JulianDay und FractionOfDay seit                                  */
/*          12.00 UTC 1. Januar 4713B.C. (12.00 Uhr 1.1.-4713)                */
/*          in Astronomie exitiert (im Gegensatz zur Kalenderrechnung) das    */
/*          Jahr Null                                                         */
/*                                                                            */
/******************************************************************************/

#ifndef _CJulianDay_h_
#define _CJulianDay_h_

// allgemeine Definitionen

#ifndef ULONG
typedef unsigned long	ULONG;
#endif

#ifndef USHORT
typedef unsigned short	USHORT;
#endif

#ifndef BOOL
typedef int				BOOL;
#endif

#ifndef TRUE
#define TRUE	1
#endif

#ifndef FALSE
#define FALSE	0
#endif

#ifndef NULL
#define NULL	0
#endif

// Struktur f�r JulianDay-Zeitangaben
#ifndef _JDStruct_
#define _JDStruct_
typedef struct
{
	long   lNumber;			// ganzzahliger JulianDate
	double dFraction;		// Fraction of Day
}JDStruct;
#endif

// Struktur f�r UTC-Zeiten
#ifndef _UTCStruct_
#define _UTCStruct_
typedef struct
{
	USHORT usDay;
	USHORT usMonth;
	long   lYear;
	USHORT usHour;
	USHORT usMinute;
	double dSecond;	
}UTCStruct;
#endif

// Klassendefinition f�r JulianDay-Zeitangaben
class CJulianDay
{
	// Konstanten
public:
	static const double dSecsPerDay;		// 86400.0

	// Variablen
protected:
	JDStruct JulianDay;						// ganzzahliger JulianDate

	// Funktionen
public:
	CJulianDay();							// Konstruktor
	CJulianDay(long,double);				// Konstruktor
	CJulianDay(double);						// Konstruktor
	CJulianDay(JDStruct);					// Konstruktor
	CJulianDay(USHORT,USHORT,long,USHORT = 0,USHORT = 0,double = 0.0);	// rechnet ein �bergebenes Datum und Uhrzeit (UTC) in (vollst�ndigen) Julian Day um
	~CJulianDay();							// Destruktor

	void      AddSeconds(double);				// �bergebene Sekunden hinzuaddieren
	double    ConvertToSeconds(void) const;		// liefert die verstrichenen Sekunden seit JD 0 0.0 zur�ck
	double    GetFraction(void) const;			// nur JulianDay-Fraction zur�ckgeben
	char*     GetDayOfWeekString(void) const;	// liefert den Wochentag als ASCII-String zur�ck
	short     GetDayOfWeek(void) const;			// liefert den Wochentag als Integer zur�ck
	JDStruct  GetJD(void) const;				// JulianDay auslesen
	void      GetJD(long*,double*) const;		// JulianDay auslesen
	ULONG     GetNumber(void) const;			// nur JulianDay-Number zur�ckgeben
	UTCStruct GetUTC(void) const;				// liefert den aktuellen JulianDay in UTC
	char*     GetUTCString(char*,int);			// liefert den aktuellen JulianDay als String zur�ck
	BOOL      IsEmpty(void);					// pr�fen, ob der JulianDay gleich 0 0.0 ist
	void      SetJD(JDStruct);					// JulianDay setzen
	void      SetJD(long,double);				// JulianDay setzen
	void      SetJD(USHORT,USHORT,long,USHORT,USHORT,double);	// rechnet ein �bergebenes Datum und Uhrzeit (UTC) in (vollst�ndigen) Julian Day um
	void      SetJD(UTCStruct);					// rechnet ein �bergebenes Datum und Uhrzeit (UTC) in (vollst�ndigen) Julian Day um

	// Operatoren
	CJulianDay operator= (const CJulianDay&);	// Klassen zuweisen
	CJulianDay operator= (JDStruct);			// JD-Struktur zuweisen
	CJulianDay operator+ (const CJulianDay&);	// Klassen addieren
	CJulianDay operator+ (JDStruct);			// JD-Struktur addieren
	CJulianDay operator+= (const CJulianDay&);	// Klassen addieren und zuweisen
	CJulianDay operator+= (JDStruct);			// JD-Struktur addieren und zuweisen
	CJulianDay operator- (const CJulianDay&);	// Klassen subtrahieren
	CJulianDay operator- (JDStruct);			// JD-Struktur subtrahieren
	CJulianDay operator-= (const CJulianDay&);	// Klassen subtrahieren und zuweisen
	CJulianDay operator-= (JDStruct);			// JD-Struktur subtrahieren und zuweisen
	BOOL operator> (const CJulianDay&);			// Werte-Vergleich durchf�hren
	BOOL operator> (JDStruct);				
	BOOL operator>= (const CJulianDay&);		// Werte-Vergleich durchf�hren
	BOOL operator>= (JDStruct);				
	BOOL operator< (const CJulianDay&);			// Werte-Vergleich durchf�hren
	BOOL operator< (JDStruct);				
	BOOL operator<= (const CJulianDay&);		// Werte-Vergleich durchf�hren
	BOOL operator<= (JDStruct);				
	BOOL operator== (const CJulianDay&);		// Werte-Vergleich durchf�hren
	BOOL operator== (JDStruct);				
	BOOL operator!= (const CJulianDay&);		// Werte-Vergleich durchf�hren
	BOOL operator!= (JDStruct);				
	operator JDStruct();						// type-casts
	operator double();

private:
	inline void Normalize(void);				// wandelt aktuellen JulianDayFraction gem�� Konvention
};

#endif
