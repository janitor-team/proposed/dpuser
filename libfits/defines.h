/*
 * file: utils/defines.h
 * Purpose: global defines
 * Author: Thomas Ott
 *
 * History: 15.10.1999: file created
 */

#ifndef DEFINES_H
#define DEFINES_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {double r,i;} dpCOMPLEX;
#define dpTOLERANCE 1e-10

typedef long long dpint64;
typedef unsigned long long dpuint64;

#ifdef __cplusplus
}
#endif

#endif /* DEFINES_H */
