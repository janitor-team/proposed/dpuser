================================================================================
                   ____  ____  _   _ ____  _____ ____
                  |  _ \|  _ \| | | / ___|| ____|  _ \
                  | | | | |_) | | | \___ \|  _| | |_) |
                  | |_| |  __/| |_| |___) | |___|  _ <
                  |____/|_|    \___/|____/|_____|_| \_\
                                    _
                                  _| |_
                                 |_   _|
                                   |_|
                  ___  _____ _ _     __     ___
                 / _ \|  ___(_) |_ __\ \   / (_) _____      __
                | | | | |_  | | __/ __\ \ / /| |/ _ \ \ /\ / /
                | |_| |  _| | | |_\__ \\ V / | |  __/\ V  V /
                 \__\_\_|   |_|\__|___/ \_/  |_|\___| \_/\_/

================================================================================

DPUSER is a command line program which provides an interactive language that is
capable of handling numbers (both real and complex), strings, and matrices. Its
main aim is to do astronomical image analysis, for which it provides a compre-
hensive set of functions, but it can also be used for many other applications.
For help on dpuser see http://www.mpe.mpg.de/~ott/dpuser/index.html

QFitsView is a FITS file viewer based on DPUSER. In addition to displaying
two-dimensional images, it also provides means to explore three-dimensional
data cubes. It integrates the functionality of DPUSER which can be accessed di-
rectly out of QFitsView.

================================================================================
    Contents
================================================================================
    1) Supported operating systems
    2) Project structure
    3) Compiling
        3.1) Linux & MacOS
            3.1.1) Needed libraries
            3.1.2) Compiling DPUSER
            3.1.3) Compiling QFitsView
        3.2) Windows
            3.2.1) Installing needed libraries
            3.2.2) MinGW
                3.2.2.1) Compiling DPUSER
                3.2.2.2) Compiling QFitsView
            3.2.3) Visual C++ 2008 Express Edition
                3.2.3.1) Compiling DPUSER
                3.2.3.2) Compiling QFitsView
            3.2.4) CYGWIN
                3.2.4.1) Compiling DPUSER
    4) DPUSER Documentation
    5) Create a distribution
    6) Using ds9 together with DPUSER
    7) Functionality/Problem table
    8) make-targets

================================================================================
    1) Supported operating systems
================================================================================
DPUSER and QFitsView can be used with following operating systems:
    - Linux                    (Kubuntu 12.04 Precise Pangolin)
    - MacOS X (Intel only)     (MacOS X 10.6.8)
    - Win32                    (Windows 7)
Each both in 32bit and 64bit

================================================================================
    2) Project structure
================================================================================
A short description of the contents of the directories is given:

- QFitsView     The QFitsView source code.
- bin           Utilities needed to build DPUSER correctly.
- dpuser        The DPUSER source code.
- external      Source code of needed external libraries.
- include       Needed include-files.
- lib           Needed libraries, in different versions for the different
                supported operating systems.
- libfits       A collection of functions for astronomical and mathematical cal-
                culations. Furthermore the fits-class is defined here, with nu-
                merous functions performing any kind of calculations on FITS
                images.
- QFitsView     The QFitsView source code.
- utils         Various utilities like regular expressions, string class etc.

================================================================================
    3) Compiling
================================================================================
ATTENTION:
  The files dpuser.y and dpuser.l in dpuser/parser should be edited ONLY UNDER
  WINDOWS! Compilation will not fail if edited under Linux, but at runtime
  DPUSER will not work correctly! (If they are compiled under Linux, there will
  be anyway a compile error when compiling DPUSER). Or use the todos-command to
  convert whitespaces from Linux format to DOS format.

In order to compile QFitsView the Qt-library has to be compiled first. The
sources are located in dpuser/external, read the README in this folder for
instructions. The version of the provided Qt distribution is guaranteed to work
together with the QFitsView source code.
All other libraries in dpuser/external are already precompiled for a variety of
operating systems.

NOTE: Qt and VTK
  The vtk-library used by QFitsView is compiled with Qt-GUI-support. So the Qt-
  version used for compiling QFitsView must be the same as the one used to
  compile the VTK-library!

NOTE:
  If configuring Qt doesn't recognise OpenGL try adding following lines to
  qt-everywhere-opensource-src-5.1.1/qtbase/mkspecs/linux-g++/qmake.conf:
  QMAKE_INCDIR_OPENGL  = /usr/include/GL
  QMAKE_LIBDIR_OPENGL  = /usr/lib/x86_64-linux-gnu
  QMAKE_LIBS_OPENGL    = -lGL
================================================================================
        3.1) Linux & MacOS
================================================================================
ATTENTION:
Under Linux following packages must be installed:
    libx11-dev
    libxt-dev
    libglu1-mesa-dev
    libxext-dev
    xfonts-scalable

Under MacOS following command has to be executed before compiling:
    $ ranlib lib/*.a

================================================================================
            3.1.1) Needed libraries
================================================================================
- When compiling DPUSER or QFitsView fails, check if any of the following
  libraries are missing:
    - libx11-dev
    - libgl1-mesa-dev
    - libglu1-mesa-dev
    - libxext6
    - libext-dev
    - zlib1g-dev
    - libx11-xcb-dev
    - libxrender-dev
    - libtiff4
    - libtiff4-dev
    - libmng1
    - libmng-dev

================================================================================
            3.1.2) Compiling DPUSER
================================================================================
Change into the "dpuser" directory and execute following command:
    $ make 

For a debug version type:
    $ make debug

================================================================================
            3.1.3) Compiling QFitsView
================================================================================
Change into the "QFitsView" directory and execute following command:
Linux & Mac (with Intel-processor) (release):
    $ make

Linux & Mac (with Intel-processor) (debug):
    $ make debug

================================================================================
        3.2) Windows
================================================================================
            3.2.1) Installing needed libraries
================================================================================
- To compile QFitsView Qt 5.2.0 or newer must be installed. Qt must be compiled
  with the desired compiler suite.
- DPUSER and QFitsView are principally compiled statically so a single binary
  is produced and can be distributed easily. This works well under Linux and
  MacOS.Unfortunaltely with Windows this approach is only feasible with some
  drawbacks. These are listed in Section 7. According this table Qt can either be
  compiled statically or dynamically.
 - TortoiseSVN has to be installed with command line tools installed as well (svnversion.exe)

    MinGW (static):
    ---------------
    $ configure.exe -fast -release -static -qt-zlib -qt-libmng -qt-libtiff
        -qt-gif -qt-libpng -qt-libjpeg -platform win32-g++

    $ configure.exe -fast -debug -static -qt-zlib -qt-libmng -qt-libtiff
        -qt-gif -qt-libpng -qt-libjpeg -platform win32-g++

    MinGW (shared):
    ---------------
    $ configure.exe -fast -release -shared -qt-zlib -qt-libmng -qt-libtiff
        -qt-gif -qt-libpng -qt-libjpeg -platform win32-g++

    $ configure.exe -fast -debug -shared -qt-zlib -qt-libmng -qt-libtiff
        -qt-gif -qt-libpng -qt-libjpeg -platform win32-g++

    MinGW64 (shared): (note: configure.exe is in the qtbase directory, have to download the .zip, not the .tar.gz!)
    -----------------
    $ configure.bat -debug -shared -platform win32-g++ -nomake tests -nomake examples -skip declarative -opengl desktop

    Visual C++ 2008 (static):
    -------------------------
    $ "c:\Program Files\Microsoft Visual Studio 9.0\VC\bin\vcvars32.bat"
    $ configure.exe -fast -release -static -qt-zlib -qt-libmng -qt-libtiff
        -qt-gif -qt-libpng -qt-libjpeg -platform win32-msvc2005
        -no-style-windowsvista

    $ configure.exe -fast -debug -static -qt-zlib -qt-libmng -qt-libtiff
        -qt-gif -qt-libpng -qt-libjpeg -platform win32-msvc2005
        -no-style-windowsvista -no-qmake
- When Qt has been installed the environment variables have to be updated (only
  needed for MinGW):

    set QTDIR=<qt_installation_path>
    set PATH=%SystemRoot%\System32
    set PATH=%PATH%;<qt_installation_path>\bin
    set QMAKESPEC=win32-g++

    (open a command prompt and copy/paste following commands, use then this
    prompt to compile. Alternatively a bat-file with the following commands can
    be created)
================================================================================
            3.2.2) MinGW
================================================================================
- Install MSys and MingGW from http://www.mingw.org
  (When installing MinGW following components have to be selected:
    - C-compiler
    - Fortran-compiler
    - MinGW-make)

- MinGW must be installed in C:\Programme\MinGW !!!
  (Necessary when dpuser-scripts should be converted and compiled as C-code)

- add 'C:\Programme\MinGW\bin' and 'C:\Programme\msys\1.0\bin' to the
  PATH environment variable

================================================================================
                3.2.2.1) Compiling DPUSER
================================================================================
- Change into the 'dpuser' directory and execute following command:
    $ mingw32-make

- For a debug version type:
    $ mingw32-make DEBUG=-g

================================================================================
                3.2.2.2) Compiling QFitsView
================================================================================
- Change into the 'QFitsView' directory and execute following commands:
    $ mingw32-make

- For a debug version type:
    $ mingw32-make debug

================================================================================
            3.2.3) Visual C++ 2008 Express Edition
================================================================================
- Download and install Microsoft Visual C++ 2008 Express Edition
- If you want to create parallel code with openmp, copy the following files:
    bin/WIN/VC2008distrib/vcomp.lib,vcompd.lib to
    Program Files/Microsoft Visual Studio 9.0/VC/lib

    bin/WIN/VC2008distrib/omp.h,ompassem.h to
    Program Files/Microsoft Visual Studio 9.0/VC/include

    bin/WIN/VC2008distrib/vcomp90.dll,vcomp90d.dll to
    C:/WINDOWS
- environment variable QTDIR has to be set to C:\Qt\4.3.5.vs.static

================================================================================
                3.2.3.1) Compiling DPUSER
================================================================================
- open dpuser/dpuser.sln and type F7

================================================================================
                3.2.3.2) Compiling QFitsView
================================================================================
- open QFitsView/QFitsView.sln and type F7

If QFitsView.pro-file has been updated, follwing steps have to be performed to
update QFitsView.sln automatically (resp. QFitsView_VS.vcproj will be updated):
    - in cmd-shell call
      "c:\Program Files\Microsoft Visual Studio 9.0\VC\bin\vcvars32.bat"
    - cd dpuser\QFitsView
    - set QMAKESPECS=win32-msvc2005
    - \qt\4.3.5.vs.static\bin\qmake -t vcapp QFitsView.pro CONFIG+=vs_proj
    - then open QFitsView.sln in VS
    - update following settings in the QFitsView-Project Settings:
        Linker > Input> Ignore Specific Library
                        in Release Configuration:
                                add "libcmt.lib"
            in Debug Configuration:
                                add "libcmt.lib;libcmtd.lib;msvcrt.lib"
    - type F7

================================================================================
            3.2.4) CYGWIN
================================================================================
- Install cygwin from www.cygwin.com
- Additional packages:
    Devel -> gcc-g++, gcc-g77, make
    Libs  -> libpng12-devel, libncurses-devel, readline

================================================================================
                3.2.4.1) Compiling DPUSER
================================================================================
- cd to dpuser directory and "make CYGWIN=-y"

================================================================================
    4) DPUSER Documentation
================================================================================
There exist two sources to get help on DPUSER:
    - Type help in DPUSER
    or
    - http://www.mpe.mpg.de/~ott/dpuser/index.html

The documentation data itself is located in dpuser/doc. It is created via
dpuser-scripts. The ACTUAL HELP TEXTS are stored in "functions.txt" and
"procedures.txt". It is important that all text belonging to a specific keyword
is located on a single line for correct parsing. Accepted keywords are:
"Syntax", "Arguments", "Switches", "Returns", "Examples", "See also", "Notes".
Additionally "<br>" can be added at the end of a line to insert a line break. If
a function or procedure is obsolete, add " (obsolete)" after the name. The
obsolete-string will only appear in the DPUSER-help and in the html-file of the
corresponding function or procedure.

How to create help:
-------------------
- start dpuser in directory /doc and execute makehelp.dpuser
- lookup index.html in directory www with your favourite web browser!
- recompile your DPUSER and help will also be available there

What makehtml_master.dpuser does:
- HTML help:
    - Html-files of each function and procedure are generated via
      makehtml.dpuser (calls makehtmlsum.dpuser to create the summary
      html-files, one for all functions and one for all procedures)
    - If an explaining picture should be created for a function, it has to be
      defined in the script graphs.dpuser, it will then be generated automati-
      cally when calling makehtml.dpuser
      ATTENTION: Works only when pgplot has been compiled with PNG.
    - Then the complete webpage can be created with the script makewww.html in
      the www directory
    - At last call makesearch.dpuser to create the search index database (takes
      a while, creates index.js)
    - All pg...-html-files are supplied statically.

- DPUSER help:
    - makec.dpuser creates help.h, which can be compiled into DPUSER

================================================================================
    5) Create a distribution
================================================================================
A distribution can be created under LINUX. Best practice is to checkout a virgin
version of the dpuser-package! This way all accumulated superfluous files are
disposed and even more important: when creating the distribution all .svn- and
obsolete-folders are deleted! Change to dpuser-folder and execute:
    $ make dist
The distribution is saved in the top folder as dpuser-rXXX.tar.gz, where XXX is
the actual revision number of subversion.

================================================================================
    6) Using ds9 together with DPUSER
================================================================================
DPUSER provides a host interface allowing to display images or overlay graphics.
(search DPUSER-help for sao). Therefore following software has to be downloaded
from http://hea-www.harvard.edu/RD/ds9/
    - ds9 binaries, version 5.2 or newer
    - XPA binaries, version 2.1.9 or newer
    (both available for Linux, Win and MacOS X)

XPA provides a little program called xpans which has to run before starting ds9.
Either start xpans manually or set environment variables, so it can be found
automatically when ds9 is started.
It doesn't matter whether DPUSER or ds9 is started first. ds9 must be running at
latest, when a sao-function is called.

================================================================================
    7) Functionality/Problem table
================================================================================

|-----------------------------------------------------------------------------|
|                 | Problems                 | Reasons                        |
|-----------------------------------------------------------------------------|
| Linux           | none                     |                                |
|-----------------------------------------------------------------------------|
| Mac(Intel & PPC)| none                     |                                |
|-----------------------------------------------------------------------------|
| Win/MinGW       | - dpuser-scripts can't   | - dpuser.dll needs exported    |
| (Qt static)     |   be compiled            |   functions and variables      |
|-----------------------------------------------------------------------------|
| Win/MinGW       | - DPUSER and QFitsView   | - our aim is to provide a      |
| (Qt shared)     |   need shared libraries  |   monolithic binary            |
|                 |   to run                 |                                |
|-----------------------------------------------------------------------------|
| Win/VS          | - dpuser-scripts can't   | - dpuser.dll needs exported    |
| (Qt static)     |   be compiled            |   functions and variables      |
|                 | - no ds9 support         | - xpa-lib has to be            |
|                 |                          |   compiled with VS             |
|-----------------------------------------------------------------------------|
| Win/VS          | - NOT SUPPORTED !        | - use MinGW shared             |
| (Qt shared)     |                          |   version instead              |
|-----------------------------------------------------------------------------|
| Win/Cygwin      | - read/write of          | - libz from cygwin             |
|                 |   compressed files       |   doesn't work ?!?             |
|-----------------------------------------------------------------------------|

The make targets available to adress the different solutions are listed in
section 8.

================================================================================
    8) make-targets
================================================================================

|-----------------------------------------------------------------------------|
| LINUX / MACOS                                                               |
|-----------------------------------------------------------------------------|
|        | DPUSER          | QFitsView             | QT-library               |
|-----------------------------------------------------------------------------|
|        | (make)          | (make)                | static                   |
|        | dpuser          | (make release)        |                          |
|        |                 | QFitsView             |                          |
|        |                 |                       |                          |
|        | (make DEBUG=-g) | (make debug)          | static                   |
|        | dpuserD         | QFitsViewD            |                          |
|-----------------------------------------------------------------------------|

|-----------------------------------------------------------------------------|
| WINDOWS                                                                     |
|-----------------------------------------------------------------------------|
|        | DPUSER          | QFitsView             | QT-library               |
|-----------------------------------------------------------------------------|
| mingw  | (make)          | (make)                | shared                   |
|        | dpuser.exe      | (make release)        |                          |
|        |                 | QFitsView.exe         |                          |
|        |                 |                       |                          |
|        | (make DEBUG=-g) | (make debug)          | shared                   |
|        | dpuserD.exe     | QFitsViewD.exe        |                          |
|        |                 |                       |                          |
|        |                 | (make release_static) | static                   |
|        |                 | QFitsView.exe         |                          |
|-----------------------------------------------------------------------------|
| VS     | dpuser.exe      | QFitsView.exe         | static                   |
|        | dpuserD.exe     | QFitsViewD.exe        |                          |
|-----------------------------------------------------------------------------|
