Source: dpuser
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: science
Priority: optional
Build-Depends: bison,
               debhelper-compat (= 12),
               flex,
               giza-dev,
               libfftw3-dev,
               libgsl-dev,
               libqt5datavisualization5-dev,
               libqt5opengl5-dev,
               libqt5svg5-dev,
               libqt5x11extras5-dev,
               libreadline-dev,
               libxpa-dev,
               qtbase5-dev (>= 5.8~),
               qtchooser,
	       libz-dev
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/dpuser
Vcs-Git: https://salsa.debian.org/debian-astro-team/dpuser.git
Homepage: https://www.mpe.mpg.de/~ott/dpuser/index.html

Package: dpuser
Architecture: any
Depends: g++,
         libc6-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: dpuser-doc
Description: Interactive language for handling numbers, strings, and matrices
 DPUSER is an interactive language capable of handling numbers (both real and
 complex), strings, and matrices. Its main aim is to do astronomical image
 analysis, for which it provides a comprehensive set of functions, but it can
 also be used for many other applications.
 .
 Note that this package was compiled with the giza library instead of PGPLOT,
 since the latter is non-free software.

Package: qfitsview
Architecture: any
Depends: g++,
         libc6-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: dpuser-doc
Description: FITS file viewer based on DPUSER
 QFitsView is a FITS file viewer based on DPUSER. In addition to displaying
 two-dimensional images, it also provides means to explore three-dimensional
 data cubes. It integrates the functionality of DPUSER which can be accessed
 directly out of QFitsView.
 .
 Note that this package was compiled with the giza library instead of PGPLOT,
 since the latter is non-free software.

Package: dpuser-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Description: Documentation for DPUSER and QFitsView
 DPUSER is an interactive language capable of handling numbers (both real and
 complex), strings, and matrices. Its main aim is to do astronomical image
 analysis, for which it provides a comprehensive set of functions, but it can
 also be used for many other applications.
 .
 This package contains the documentation for DPUSER and QFitsView.
