#include <QWidget>
#include <QDialog>

class QPgplot : public QWidget {
    Q_OBJECT
public:
    QPgplot(QWidget *parent = NULL);
    QPixmap pixmap;
protected:
    void paintEvent(QPaintEvent *);
//    void closeEvent(QCloseEvent *);

};

class QPgplotDialog : public QDialog {
    Q_OBJECT
public:
    QPgplotDialog(QWidget *parent, const int &num = 0);
    QPgplot *plotter;
protected:
    void resizeEvent(QResizeEvent *);
};

/*
  This is the PNG (Portable Network Graphics) driver for PGPLOT.
  For more information on the PNG standard, and to get the
  necessary libraries, see http://www.cdrom.com/pub/png/

  This driver is intended to be used in the same ways as one would
  use PGPLOT's  GIF driver, and as such uses many of the same conventions.

  The default plotting dimensions are 850x680, and can be
  manipulated via the PGPLOT_PNG_WIDTH and PGPLOT_PNG_HEIGHT
  environment variables.

  The driver can be opened as many times as the caller likes (i.e., more
  than one device number available). Associated with each device
  is a single filename, and after each page advance the filename is
  modified to have a trailing "_X", where "X" is the current
  page number. This does not apply to the first page output,
  however.

  For compilation, both libpng and zlib must be installed. These
  libraries are Free Software, and can be obtained at the following
  URLs:
        libpng: http://www.cdrom.com/pub/png/
        zlib:   http://www.cdrom.com/pub/infozip/zlib/


  March, 1999
  Pete Ratzlaff <pratzlaff@cfa.harvard.edu>

*/

extern "C" {

/* simple way of specifiying the current device structure pointer */
#define ACTIVE_DEVICE  (all_devices.devices[all_devices.active])
#define boolean unsigned char
#define NCOLORS 256

typedef unsigned char ColorComponent; /* red, green, or blue component of a colortable entry */
typedef unsigned char ColorIndex;  /* index into a color table */

/* data for a single open device */
typedef struct _DeviceData DeviceData, *DeviceDataPtr;
struct _DeviceData {
  int w, h;
  long npix; /* w*h */
  boolean trans; /* transparent background flag */
  boolean error; /* if true, we can plot no more on this device */
  ColorIndex *pixmap; /* image consisting of array of color indicies */
  int npages; /* running total of plot pages */
  char *filename;
  ColorComponent ctable[NCOLORS * 3];
  ColorIndex cindex; /* current plotting color index */
  int devnum; /* this device's identifier */
};

/* global data holding all devices */
typedef struct _Devices Devices;
struct _Devices {
  DeviceDataPtr *devices;
  int nallocated;
  int active;
};

extern Devices all_devices;
extern int QPGPLOT_DEFAULT_WIDTH;
extern int QPGPLOT_DEFAULT_HEIGHT;

};
