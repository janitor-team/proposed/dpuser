#ifndef QFITSWEDGE_H
#define QFITSWEDGE_H

#include <QWidget>

class QFitsMainView;

class QFitsWedge : public QWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsWedge(QFitsMainView *parent);
    ~QFitsWedge() {}

protected:
    void paintEvent(QPaintEvent *e);
    void resizeEvent(QResizeEvent *e);

//----- Slots -----
//----- Signals -----
//----- Members -----
private:
    QFitsMainView   *myParent;
    QImage          *image;
};

#endif // QFITSWEDGE_H
