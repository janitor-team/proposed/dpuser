#ifndef QFITSHEADERVIEW_H
#define QFITSHEADERVIEW_H

#include <QPushButton>
#include <QString>
#include <QLineEdit>
#include <QLabel>
#include <QTextEdit>
#include <QDialog>

class EscLineEdit;
class QFitsMainWindow;
class Fits;

class QFitsHeaderContent : public QTextEdit {
    Q_OBJECT
public:
    QFitsHeaderContent(QWidget *parent);
    EscLineEdit *keyValue, *newKeyValue;
private:
    QString key, value;
    int oldYvalue;
protected:
    void mouseMoveEvent(QMouseEvent *);
    void mouseDoubleClickEvent(QMouseEvent *);
private slots:
    void changeKey();
    void addKey();
    void adjustEditorPosition(int);
signals:
    void keyChanged(QString, QString);
};

class QFitsHeaderView : public QDialog {
    Q_OBJECT
//----- Functions -----
public:
    QFitsHeaderView(QWidget*);
    ~QFitsHeaderView() {}

    virtual void showUp(Fits*);
protected:
    void resizeEvent(QResizeEvent *);
    void keyPressEvent(QKeyEvent *);

//----- Slots -----
protected slots:
    void dosearch(const QString &);
    void dosearchagain();
    void fontchanged();
private slots:
    void editWCS();
public slots:
    virtual void changeKey(QString, QString);

//----- Signals -----
//----- Members -----
protected:
//    QFitsMainWindow     *myParent;
    QFitsHeaderContent  *content;
    QPushButton         *closeButton;
    QLineEdit           *searchTerm;
    QLabel              *searchLabel;
    QPushButton         *searchAgain,
                        *editWCSButton;
    QString             oldfont;
    int                 oldfontsize;
    int                 scrollPosition;
};

class QFitsHeaderViewExt : public QFitsHeaderView {
    Q_OBJECT
//----- Functions -----
public:
    QFitsHeaderViewExt(QWidget*);
    ~QFitsHeaderViewExt() {}

    void showUp(Fits*, QString);
//----- Slots -----
public slots:
    void reject();
//----- Signals -----
//----- Members -----
public slots:
    virtual void changeKey(QString, QString);
private:
    QString header;
    bool modified;
    bool readonly;
    QString filename;
    size_t fpos;
};

#endif /* QFITSHEADERVIEW_H */
