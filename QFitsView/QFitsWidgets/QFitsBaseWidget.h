#ifndef QFITSWIDGET_H
#define QFITSWIDGET_H

#include <QObject>
#include <QTimer>

#include "QFitsGlobal.h"
#include "QFitsBaseView.h"

class QFitsMainView;
class QFitsBaseBuffer;
class QFitsSingleBuffer;
class QFitsMarkers;
class Fits;

class QFitsBaseWidget : public QWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsBaseWidget(QFitsBaseBuffer *bb = NULL);
    virtual ~QFitsBaseWidget();

    std::string getBufferIndex();
    QFitsSingleBuffer* getMyBuffer();
    QFitsSingleBuffer* getBuffer(std::string i);
    virtual void calculateScaling(Fits *work);
    void enterBuffer();
    void leaveBuffer();

    void setMovieSpeed(int);
    void enableMovie(bool);

    //
    // functions being overloaded
    //
    virtual void setFlipX(bool)                             {}
    virtual void setFlipY(bool)                             {}
    virtual void setRotation(int)                           {}
    virtual void orientationChanged()                       {}
    virtual void setZoom(double)                            {}
    virtual void updateScaling()                            {}
    virtual void setXRange(const double&, const double&)    {}
    virtual void setYRange(const double&, const double&)    {}
    virtual void newData3D()                                {}
    virtual void setupColours()                             {}
    virtual void setImageCenter(double, double)             {}
    virtual void setMouseTrackingView(bool)                 {}
    virtual QFitsMarkers* getSourceMarkers()                { return NULL; }
    virtual QFitsBaseView* getView()                        = 0;
    virtual void copyImage(int);

//----- Slots -----
//----- Signals -----
signals:
    void statusbartext(const QString &);

//----- Members -----
protected:
    QFitsSingleBuffer     *myBuffer;
    int                    movieSpeed;
public:
    QTimer                *movieTimer;
};

#endif /* QFITSWIDGET_H */
