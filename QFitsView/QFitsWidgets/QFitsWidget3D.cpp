﻿#include "QFitsWidget3D.h"
#include "QFitsView3D.h"

//------------------------------------------------------------------------------
//         QFitsWidget3D
//------------------------------------------------------------------------------
QFitsWidget3D::QFitsWidget3D(QFitsBaseBuffer *bb) : QFitsBaseWidget(bb) {
    cubeViewer = new QFitsView3D(this);

//connect(fitsMainWindow->mytoolbar, SIGNAL(updateZRange3D(double, double)),
//        cubeViewer, SLOT(updateZRange3D(double, double)));
}

QFitsWidget3D::~QFitsWidget3D() {
    if (cubeViewer != NULL) {
        delete cubeViewer;
        cubeViewer = NULL;
    }
}

//void QFitsWidget3D::setZoom(double zoomValue) {
//    getMyBuffer()->setZoomFactor_3Dwire(zoomValue);
//    cubeViewer->applyZoom();
//}

//void QFitsWidget3D::updateScaling() {
//    cubeViewer->updateScaling();
//}

void QFitsWidget3D::newData3D() {
    cubeViewer->newData();
}

void QFitsWidget3D::setupColours()
{
    cubeViewer->updateColourtable();
}

//void QFitsWidget3D::setImageCenter(double x, double y) {
//    cubeViewer->setImageCenter(x, y);
//}

void QFitsWidget3D::resizeEvent(QResizeEvent *r) {
    cubeViewer->setGeometry(0, 0, r->size().width(), r->size().height());
}
