#ifndef WIREGRID_H
#define WIREGRID_H

#include <QImage>
#include <QWidget>

#include "QFitsBaseWidget.h"
#include "gsl/gsl_matrix.h"
//#include "QFitsWidgetContour.h"

class QFitsMainView;
class QFitsMarkers;

class QFitsWidgetWiregrid : public QFitsBaseWidget {
	Q_OBJECT
//----- Functions -----
public:
    QFitsWidgetWiregrid(QFitsBaseBuffer*);
    ~QFitsWidgetWiregrid();

    //
    // overloaded abstract base functions
    //
    void setImageCenter(double, double);
    QFitsBaseView* getView()                        { return NULL; }

    void setZoom(double);

protected:
    void resizeEvent(QResizeEvent *e);
    void mouseMoveEvent(QMouseEvent *m);
    void mousePressEvent(QMouseEvent *m);
    void wheelEvent(QWheelEvent* event);
    void paintEvent(QPaintEvent *e);
    void keyPressEvent( QKeyEvent *e );
    void enterEvent(QEvent*);
    void leaveEvent(QEvent*);
//    void calcMesh(int x1, int x2, int y1, int y2, double alpha, double beta);
    void drawMesh(int x1, int x2, int y1, int y2, double alpha, double beta, QPainter &p);

//----- Slots -----
public slots:
    void setXRotation( int degrees );
    void setYRotation( int degrees );
    void setZRotation( int degrees );
    void setScrollerImageCenter(double, double);
    void setCenter(int, int)    {}

//----- Signals -----
signals:
    void updateMagnifier(QPixmap &pix);

//----- Members -----
private:
    double              xRot, yRot, zRot, scale;
    int                 xcen, ycen, radius, mousex, mousey;
    double              *zvalues,
                        minyvalue;
    long                *indices,
                        nvalues;
    QImage              *image;
    QList<QPolygonF>    mesh;
    gsl_matrix          *xv, *yv, *zv;
};

#endif /* WIREGRID_H */
