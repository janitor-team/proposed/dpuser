#ifndef QFITSWIDGET1D_H
#define QFITSWIDGET1D_H

#include <QWidget>
#include <QDialog>
#include <QTableWidget>
#include <QLabel>
#include <QPushButton>

#include "QFitsBaseWidget.h"
#include "QFitsView1D.h"
#include "QFitsGlobal.h"
#include "fits.h"

class QFitsMainView;
class QFitsSimplestButton;
class QFitsMarkers;

class QFitsTransparentLabel;    // declared below
class QLabelWithoutMouse;       // declared below
class QWidgetWithoutMouse;      // declared below
class QFitsTransparentCaption;  // declared below

class QFitsWidget1D : public QFitsBaseWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsWidget1D(QFitsBaseBuffer*);
    ~QFitsWidget1D();

    void updateTable(const Fits &values);
    void updateHeight(int);
    void updatePos(int);
    void updateFWHM(int);
    void updateBase(int);

    //
    // overloaded abstract base functions
    //
    void updateScaling();
    void setXRange(const double &, const double &);
    void setYRange(const double &, const double &);
    void setData();
    virtual QFitsBaseView* getView()                        { return plotter; }

    void plotterKeyPressEvent(QKeyEvent*);
    void plotterMouseMoveEvent(QMouseEvent*);
    bool handleMousePressEvent(QMouseEvent*);

protected:
    void resizeEvent(QResizeEvent*);
    void paintEvent(QPaintEvent*);
    void keyReleaseEvent(QKeyEvent*);
    void enterEvent(QEvent*);

//----- Slots -----
public slots:
    void hide();
    void dofit();
    void handleescape();
    void subtractfit();
    void addfit();
    void copyfit();

//----- Signals -----
//----- Members -----
public:
    double                  fitx[2], fity[2];
    int                     bstart, bend,
                            ngauss,
                            heightupdate, posupdate, fwhmupdate, baseupdate,
                            currentStatusBarText;
    bool                    dagain, gagain, bagain;
    Fits                    fitgx, fitgy, types,
                            params, fitparams;
    QFitsView1D             *plotter;
    QTableWidget            *fitresults;
    QFitsTransparentLabel   *fitestimates;
    QVector<QString>        dpQtPlotStatusBarText;
};

class QFitsTransparentLabel : public QDialog {
    Q_OBJECT
//----- Functions -----
public:
    QFitsTransparentLabel(QFitsWidget1D*, QFitsView1D*);
    ~QFitsTransparentLabel() {}

    void setFitEstimates(int nparams, double *fitx, double *fity, Fits &types, Fits &fitgx, Fits &fitgy);
    void setFitResults(double *fitx, double *fity, const Fits &values);
    void showValues();
    void deleteFitResults();
    void removeMarker2();
    bool getShowEstimates() { return showEstimates; }
    int getNlines() { return nlines; }
    double getEstimateFitX(int i);
    double getEstimateFitY(int i);
    double getEstimateFitGX(int i) { return estimatefitgx[i];}
    double getEstimateFitGY(int i) { return estimatefitgy[i];}
    const Fits* const getEstimateFitGX() { return &estimatefitgx;}
    const Fits* const getEstimateFitGY() { return &estimatefitgy;}
    const Fits* const getFitResult() { return &fitResult;}

protected:
    void mouseMoveEvent(QMouseEvent *);
    void mouseDoubleClickEvent(QMouseEvent *);
    void leaveEvent(QEvent *);

//----- Slots -----
public slots:
    void fitestimatesClicked();
    void fitresultsClicked();

//----- Signals -----
//signals:
//    void mouseMoved(const QPoint &);

//----- Members -----
private:
    QFitsWidget1D* myParent;
    QFitsView1D* myView;
    QFitsTransparentCaption *theCaption;
    QWidgetWithoutMouse *viewArea;
    QLabelWithoutMouse *l1, *l2, *l3, *l4, *l5;
    QLabel *marker, *marker2;
    QFitsSimplestButton *fitestimatesTab, *fitresultsTab, *closeButton;
    QPushButton *fitButton, *addButton, *subButton, *clipboardButton;
    int nlines;
    bool showEstimates;
    bool hasFitResult;
    // values of the fit estimate
    double estimatefitx[2], estimatefity[2];
    int estimatenparams;
    Fits estimatetypes, estimatefitgx, estimatefitgy;
    // values of the fit result
    double resultfitx[2], resultfity[2];
    Fits fitResult;
};

class QFitsTransparentCaption : public QLabel {
    Q_OBJECT
//----- Functions -----
public:
    QFitsTransparentCaption(QFitsTransparentLabel *parent);
    ~QFitsTransparentCaption() {}

protected:
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);

//----- Slots -----
//----- Signals -----
//----- Members -----
private:
    QFitsTransparentLabel *myParent;
    QPoint p;
};

class QWidgetWithoutMouse : public QWidget {
    Q_OBJECT
//----- Functions -----
public:
    QWidgetWithoutMouse(QWidget *parent);
    ~QWidgetWithoutMouse() {}
protected:
    void mouseMoveEvent(QMouseEvent *);

//----- Slots -----
//----- Signals -----
//----- Members -----
};

class QLabelWithoutMouse : public QLabel {
    Q_OBJECT
//----- Functions -----
public:
    QLabelWithoutMouse(QWidget *parent);
protected:
    void mouseMoveEvent(QMouseEvent *);

//----- Slots -----
//----- Signals -----
//----- Members -----
};

#endif /* QFITSWIDGET1D_H */
