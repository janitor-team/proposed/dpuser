#ifndef CONTOUR_H
#define CONTOUR_H

#include <QWidget>
#include <QScrollArea>
#include <QToolBar>
#include <QToolButton>
#include <QLineEdit>

#include "QFitsBaseWidget.h"
#include "QFitsSingleBuffer.h"
#include "QFitsGlobal.h"

class QFitsMainView;
class QFitsWidgetContour;

class EscLineEdit : public QLineEdit {
    Q_OBJECT
public:
    EscLineEdit(QWidget *parent);
    QToolButton *okButton, *cancelButton;
protected:
    void keyPressEvent(QKeyEvent *);
    void resizeEvent(QResizeEvent *);
public slots:
    void okButtonClicked();
    void cancelButtonClicked();
signals:
    void escapePressed();
};

class dpContour : public QWidget {
    Q_OBJECT
public:
    dpContour(QWidget *parent, QFitsWidgetContour *parent2);
    void paintContour(QPainter &p, QTransform &T);
    void extMouseMoved(QMouseEvent *m) { mouseMoveEvent(m); }
    void applyZoom();
    QTransform T;
protected:
    void paintEvent(QPaintEvent *e);
    void mouseMoveEvent(QMouseEvent *m);
    void mousePressEvent(QMouseEvent *m);
    void keyPressEvent(QKeyEvent*);
    void enterEvent(QEvent*);
    void leaveEvent(QEvent*);
private:
    QFitsWidgetContour *myParent;
    QTransform trans;
    int nlevels;
    Fits *levels;
};

class QFitsWidgetContour : public QFitsBaseWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsWidgetContour(QFitsBaseBuffer*);
    ~QFitsWidgetContour();

    //
    // overloaded abstract base functions
    //
    void orientationChanged();
    void setZoom(double);
    void updateScaling();
    void setScaling(int);
    void setImageCenter(double, double);
    void setScrollerImageCenter(double, double);
    QFitsBaseView* getView()                { return NULL; }

protected:
    void resizeEvent(QResizeEvent*);
    void mouseMoveEvent(QMouseEvent*);

//----- Slots -----
public slots:
    void newManualLevels();
    void discardManualLevels();

//----- Signals -----
//----- Members -----
public:
    QScrollArea *scroller;
    dpContour   *contour;
    EscLineEdit *levels;
    bool        manualLevels;
};

#endif /* CONTOUR_H */
