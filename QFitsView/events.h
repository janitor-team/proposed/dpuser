/*
 * Project: QFitsView
 * File:    events.h
 * Purpose: Communication events between QFitsView GUI and DPUSER
 * Author:  Thomas Ott
 * History: August 20, 2004: File created
 */

#ifndef EVENTS_H
#define EVENTS_H

#include <QWidget>
#include <QApplication>
#include <QEvent>

class Fits;
//class QFitsMainWindow;

//extern QWidget *mw;

/* custom Event to request display of certain help */
#define dpDpuserID ((int)QEvent::User+1)

class dpDpuserEvent: public QEvent {
public:
	dpDpuserEvent(int onWhich)
	           : QEvent((QEvent::Type)dpDpuserID), what(onWhich) {}
	int requestedVar() const { return what; }
private:
	int what;
};

/* custom Event to show progress */
#define dpProgressID ((int)QEvent::User+5)

class dpProgressEvent: public QEvent {
public:
	dpProgressEvent(int onWhich, QString onText)
	           : QEvent((QEvent::Type)dpProgressID), what(onWhich) { text = onText; }
	int progress() const { return what; }
	QString message() const { return text; }
private:
	int what;
	QString text;
};

/* Utility function to easily request help */
void dpUpdateVar(const std::string what);
void dpUpdateView(const std::string what);
void dpCommandQFitsView(const char *what, const char *args, const int &option);

/* Utility function to easily display progress */
void dpProgress(const int what, const char *text);

/* injection of a new variable */
std::string injectVariable(const QString &name, const Fits &data);

#endif /* EVENTS_H */
