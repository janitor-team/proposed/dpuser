# To disable VTK, search for
# "contains(QT, opengl):DEFINES += HAS_VTK"
# and uncomment that line

#### set build dir for mingw ################
CONFIG += win32_setup
include(QFitsView_common.pro)
CONFIG -= win32_setup

#### set ARCH ################
ARCH = WIN
contains(QMAKE_HOST.arch, x86_64):{
	ARCHBIT = 64
} else {
	ARCHBIT = 32
}

#### set TEMPLATE ################
TEMPLATE = lib

#### set QT ################
QT += script \
      network \
      printsupport

!win32 {
   QT += opengl
} else:win32:!qf_shared {
   QT += opengl
}

#### set CONFIG ################
CONFIG += qt \
          warn_off \
          dll

#### set VTK_PATH ################
VTK_PATH = vtk/MinGW/qt_shared

#### set INCLUDEPATH ################
INCLUDEPATH += ../utils \
               ../libfits \
               ../include \
               ../include/vtk \
               ../dpuser \
               ../dpuser/parser \
               ../QFitsView/QFitsBuffers \
               ../QFitsView/QFitsWidgets \
               ../QFitsView/QFitsViews
			   
#### set MAKEFILE ################
MAKEFILE = QFitsView_dll.mk

#### set TARGET ################
CONFIG(release, debug|release) {
   TARGET = QFitsView
}
CONFIG(debug, debug|release) {
   TARGET = QFitsViewD
}

#### set DEFINES ################
win32:!vs_proj:DEFINES += HAVE_MINGW32  # needed for xpa
DEFINES -= UNICODE
DEFINES += DPQT \
           HAS_PGPLOT \
		   HAS_DPPGPLOT \
           NO_READLINE \
           $$ARCH

contains(QT, opengl):DEFINES += HAS_VTK

CONFIG(debug, debug|release) {
   DEFINES += DBG
}

#### set LIBS ################
CONFIG += qf_libs_common
include(QFitsView_common.pro)
CONFIG -= qf_libs_common

contains(DEFINES, HAS_VTK) {
   CONFIG += qf_libs_shared
   include(QFitsView_common.pro)
   CONFIG -= qf_libs_shared
}

LIBS += -lws2_32 -lcomdlg32

contains(DEFINES, HAS_PGPLOT) {
   CONFIG += qf_libs_pgplot_win32
   include(QFitsView_common.pro)
   CONFIG -= qf_libs_pgplot_win32

   LIBS  += -lgfortran -lgdi32
}

#### add sources and headers ################
CONFIG += qf_sources dp_sources
include(QFitsView_common.pro)
CONFIG -= qf_sources dp_sources
