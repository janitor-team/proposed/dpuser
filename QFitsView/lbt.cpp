#include "lbt.h"

#include <QFitsGlobal.h>
#include <QLabel>
#include <QPushButton>
#include <QDoubleValidator>
#include <QMessageBox>
#include <QStringList>

#include <qtdpuser.h>

QStringList lucitools, alignLuci1, alignLuci2;

void initialiseScripts(void) {
    lucitools += "function outline, mask, n {";
    lucitools += "  outline = shift(mask, n, 0)";
    lucitools += "  outline += shift(mask, 0, n)";
    lucitools += "  outline += shift(mask, -n, 0)";
    lucitools += "  outline += shift(mask, 0, -n)";
    lucitools += "  clip outline, 0, 1";
    lucitools += "  outline -= mask";
    lucitools += "}";
    lucitools += "";
    lucitools += "function preparediff, fnames {";
    lucitools += "med_size_im=1    // ***pixel for median smoothing of Diff image";
    lucitools += "";
    lucitools += "Diff = readfits(fnames[2])-readfits(fnames[1])";
    lucitools += "Diff -= median(Diff)";
    lucitools += "";
    lucitools += "preparediff=boxcar(Diff,med_size_im,/median)";
    lucitools += "}";
    lucitools += "";
    lucitools += "function preparemask, MaskIm, highval {";
    lucitools += "Mask=boxcar(MaskIm,1,/median)";
    lucitools += "";
    lucitools += "highpos=where(Mask > highval)";
    lucitools += "Lowpos=where(Mask <= highval)";
    lucitools += "";
    lucitools += "Mask[highpos]=1";
    lucitools += "Mask[Lowpos]=0";
    lucitools += "";
    lucitools += "preparemask = Mask";
    lucitools += "}";
    lucitools += "";
    lucitools += "function preparesim, Diff, Mask {";
    lucitools += "preparesim = Diff + 2 * max(Diff) * Mask";
    lucitools += "}";
    lucitools += "";
    lucitools += "function offsets, cent, Diff, MaskIm, luci {";
    lucitools += "offsets = 1";
    lucitools += "med_size=3    // ***pixel for median smoothing of mask";
    lucitools += "gauss_width=4 // ***npix FWHM for gaussian smoothing of mask";
    lucitools += "";
    lucitools += "//Mim = boxcar(MaskIm,med_size,/median)";
    lucitools += "";
    lucitools += "//Gim = smooth(Mim, gauss_width)";
    lucitools += "";
    lucitools += "";
    lucitools += "starcent = maskcent = fits(2, int(naxis2(cent)/2))";
    lucitools += "";
    lucitools += "j = 1";
    lucitools += "for (i = 1; i <= naxis2(cent); i += 2) {";
    lucitools += "  starcent[*,j] = cent[*,i]";
    lucitools += "  maskcent[*,j] = cent[*,i+1]";
    lucitools += "  j++";
    lucitools += "}";
    lucitools += "";
    lucitools += "//starcent = markpos(Sim)";
    lucitools += "//starcent=centroids(Diff, starcent, 8) // could also use gausspos";
    lucitools += "starcent = gausspos(Diff, maxima(Diff, starcent, 8), 5)";
    lucitools += "";
    lucitools += "//'StarCenters.fits' = starcent";
    lucitools += "//view Mask,/log,/scale95";
    lucitools += "//message \"Please mark mask positions in the same order\"";
    lucitools += "//maskcent = markpos(Mask)";
    lucitools += "//maskcent = centroids(Gim, maskcent, 50)";
    lucitools += "maskcent = centroids(MaskIm, maskcent, 50)";
    lucitools += "";
    lucitools += "//'MaskCenters.fits' = maskcent";
    lucitools += "";
    lucitools += "CRpix=[1009.5,1033.4]   // **center of rotation pixel  Luci1";
    lucitools += "";
    lucitools += "Pixscale1=0.1178     // arcsec per pix Luci1";
    lucitools += "Pixscale2=0.1190     // arcsec per pix Luci2";
    lucitools += "";
    lucitools += "Pixscale = Pixscale1";
    lucitools += "";
    lucitools += "N_stars=naxis2(starcent)";
    lucitools += "";
    lucitools += "angle_matrix_ref=floatarray(N_stars,N_stars)";
    lucitools += "angle_matrix_im=floatarray(N_stars,N_stars)";
    lucitools += "";
    lucitools += "ref=maskcent-1";
    lucitools += "im=starcent-1      // convert to idl coordinates....not needed due to differnces only count";
    lucitools += "";
    lucitools += "im_rot=floatarray(2,N_stars)";
    lucitools += "im_S=floatarray(2,N_stars)";
    lucitools += "";
    lucitools += "";
    lucitools += "";
    lucitools += "for k=1,N_stars {  ";
    lucitools += "  for L=1,N_stars {";
    lucitools += "    ";
    lucitools += "  angle_matrix_ref[k,L]=atan((ref[2,k]-ref[2,L]) / (ref[1,k]-ref[1,L]))";
    lucitools += "  angle_matrix_im[k,L]=atan((im[2,k]-im[2,L]) / (im[1,k]-im[1,L]))  ";
    lucitools += "  ";
    lucitools += "  }";
    lucitools += "}";
    lucitools += "  ";
    lucitools += "angle_diff=angle_matrix_ref-angle_matrix_im";
    lucitools += "angle=avg(angle_diff)";
    lucitools += "";
    lucitools += "d_angle=angle";
    lucitools += "";
    lucitools += "// derotate the measured coordinates:";
    lucitools += "// shift the coordinate system onto center of rotation first:";
    lucitools += "";
    lucitools += "im_S[1,*]=im[1,*]-CRpix[1]";
    lucitools += "im_S[2,*]=im[2,*]-CRpix[2]";
    lucitools += "";
    lucitools += "// rotate";
    lucitools += "for k=1,N_stars {";
    lucitools += "  ";
    lucitools += "  im_rot[1,k]=im_S[1,k]*cos(d_angle)-im_S[2,k]*sin(d_angle)";
    lucitools += "  im_rot[2,k]=im_S[1,k]*sin(d_angle)+im_S[2,k]*cos(d_angle)    ";
    lucitools += "  ";
    lucitools += "}";
    lucitools += "  ";
    lucitools += "// shift back:";
    lucitools += "im_S[1,*]=im_rot[1,*]+CRpix[1]";
    lucitools += "im_S[2,*]=im_rot[2,*]+CRpix[2]  ";
    lucitools += "";
    lucitools += "// X Y shifts";
    lucitools += "diff_rot=ref-im_S";
    lucitools += "";
    lucitools += "";
    lucitools += "print \"angles \"";
    lucitools += "print angle_diff, /values";
    lucitools += "print \"mean_angle in rad \" + angle*(-1)";
    lucitools += "print \"mean_angle in Grad \" + rad2deg(angle)*(-1)";
    lucitools += "";
    lucitools += "print \"\"";
    lucitools += "print \"single shift values\"";
    lucitools += "print diff_rot, /values";
    lucitools += "";
    lucitools += "print \"mean X shift rot \" + avg(diff_rot[1,*])";
    lucitools += "print \"mean Y shift rot \" + avg(diff_rot[2,*])";
    lucitools += "";
    lucitools += "print \"median X shift rot \" + median(diff_rot[1,*])";
    lucitools += "print \"median Y shift rot \" + median(diff_rot[2,*])";
    lucitools += "print \"\"";
    lucitools += "";
    lucitools += "print \"DetXY offsets / rotation to apply\"";
    lucitools += "print \"\"";
    lucitools += "print \"mean_angle in Grad \" + rad2deg(angle)*(-1)";
    lucitools += "print \"\"";
    lucitools += "if (luci == 1) {";
    lucitools += "print \"median X shift LUCI1 \" + median(diff_rot[1,*])*Pixscale1";
    lucitools += "print \"median Y shift LUCI1 \" + median(diff_rot[2,*])*Pixscale1";
    lucitools += "} else {";
    lucitools += "print \"median X shift LUCI2 \" + median(diff_rot[1,*])*Pixscale2";
    lucitools += "print \"median Y shift LUCI2 \" + median(diff_rot[2,*])*Pixscale2";
    lucitools += "Pixscale = Pixscale2";
    lucitools += "}";
    lucitools += "print \"\"";
    lucitools += "offsets = [median(diff_rot[1,*])*Pixscale, median(diff_rot[2,*])*Pixscale, rad2deg(angle)*(-1), median(diff_rot[1,*]), median(diff_rot[2,*])]";
    lucitools += "}";
    lucitools += "";
    lucitools += "procedure createxml, luci1dx, luci1dy, luci1dphi, luci2dx, luci2dy, luci2dphi {";
    lucitools += "output = stringarray(0)";
    lucitools += "";
    lucitools += "useluci1 = 0";
    lucitools += "if (luci1dx != 0 || luci1dy!= 0 || luci1dphi != 0) useluci1 = 1";
    lucitools += "useluci2 = 0";
    lucitools += "if (luci2dx != 0 || luci2dy!= 0 || luci2dphi != 0) useluci2 = 1";
    lucitools += "";
    lucitools += "if (useluci1 != 0 || useluci2 != 0) {";
    lucitools += "output += \"<observationItem idItem=\\\"1\\\">\"";
    lucitools += "output += \"    <!--\"";
    lucitools += "output += \"    Label of the observation item. Keep string: ARGOS aquisition to identify a source of item.\"";
    lucitools += "output += \"    -->\"";
    lucitools += "output += \"    <label>ARGOS acquisition</label>\"";
    lucitools += "if (useluci1 != 0 && useluci2 != 0) {";
    lucitools += "output += \"    <itemType>REGULAR</itemType>\"";
    lucitools += "} else {";
    lucitools += "output += \"    <itemType>ACQUISITION_REALTIME</itemType>\"";
    lucitools += "}";
    lucitools += "output += \"    <useBinocularMode>false</useBinocularMode>\"";
    lucitools += "output += \"    <synchronizeInstruments>false</synchronizeInstruments>\"";
    lucitools += "output += \"    <!--\"";
    lucitools += "output += \"    Stop LUCI2 side after this ARGOS correction. Put false if we do not want stop after correction.\"";
    lucitools += "output += \"    LUCI SW RTD has false for pause.\"";
    lucitools += "output += \"    If usageType attribute NULL or empty, apply pause for LUCI1 and LUCI2 sides.\"";
    lucitools += "output += \"    If usageType attribute specified, apply pause just for given side. \"";
    lucitools += "output += \"    -->\"";
//    lucitools += "if (useluci1 != 0 && useluci2 != 0) {";
//    lucitools += "output += \"    <pauseQueue usageType=\\\"\\\">true</pauseQueue>\"";
//    lucitools += "} else {";
//    lucitools += "if (useluci1 != 0) {";
//    lucitools += "output += \"    <pauseQueue usageType=\\\"1\\\">true</pauseQueue>\"";
//    lucitools += "}";
//    lucitools += "if (useluci2 != 0) {";
//    lucitools += "output += \"    <pauseQueue usageType=\\\"2\\\">true</pauseQueue>\"";
//    lucitools += "}";
//    lucitools += "}";
    lucitools += "output += \"    <telescope>\"";
    lucitools += "output += \"        <mirrors>\"";
    lucitools += "if (useluci1 != 0) {";
    lucitools += "output += \"            <!--\"";
    lucitools += "output += \"            Apply correction for LUCI1 side.\"";
    lucitools += "output += \"            -->\"";
    lucitools += "output += \"            <mirror usageType=\\\"1\\\">\"";
    lucitools += "output += \"                <!--\"";
    lucitools += "output += \"                Here it is relative XY\"";
    lucitools += "output += \"                -->\"";
    lucitools += "output += \"                <offset absoluteOffset=\\\"false\\\" equatorialCoordinate=\\\"false\\\">\"";
    lucitools += "output += \"                    <coord>\" + luci1dx + \"</coord>\"";
    lucitools += "output += \"                    <coord>\" + luci1dy + \"</coord>\"";
    lucitools += "output += \"                </offset>\"";
    lucitools += "output += \"                <rotationAngleOffset absoluteOffset=\\\"false\\\">\" + luci1dphi + \"</rotationAngleOffset>\"";
    lucitools += "output += \"                <updatePointingOrigin equatorialCoordinate=\\\"false\\\">true</updatePointingOrigin>\"";
    lucitools += "output += \"                <guideStarsAuto>false</guideStarsAuto>\"";
    lucitools += "output += \"            </mirror>\"";
    lucitools += "}";
    lucitools += "if (useluci2 != 0) {";
    lucitools += "output += \"            <mirror usageType=\\\"2\\\">\"";
    lucitools += "output += \"                <!--\"";
    lucitools += "output += \"                Here it is relative XY\"";
    lucitools += "output += \"                -->\"";
    lucitools += "output += \"                <offset absoluteOffset=\\\"false\\\" equatorialCoordinate=\\\"false\\\">\"";
    lucitools += "output += \"                    <coord>\" + luci2dx + \"</coord>\"";
    lucitools += "output += \"                    <coord>\" + luci2dy + \"</coord>\"";
    lucitools += "output += \"                </offset>\"";
    lucitools += "output += \"                <rotationAngleOffset absoluteOffset=\\\"false\\\">\" + luci2dphi + \"</rotationAngleOffset>\"";
    lucitools += "output += \"                <updatePointingOrigin equatorialCoordinate=\\\"false\\\">true</updatePointingOrigin>\"";
    lucitools += "output += \"                <guideStarsAuto>false</guideStarsAuto>\"";
    lucitools += "output += \"            </mirror>\"";
    lucitools += "}";
    lucitools += "output += \"        </mirrors>\"";
    lucitools += "output += \"    </telescope>\"";
    lucitools += "output += \"</observationItem>\"";
    lucitools += "";
    lucitools += "export \"/tmp/lucioffset.xml\", output";
    lucitools += "exec \"/lbt/lucifer/lcsp/bin/commitXML.sh insert /tmp/lucioffset.xml\"";
    lucitools += "}";
    lucitools += "}";

    alignLuci1 += "cd \"/newdata\"";
    alignLuci1 += "//cd \"c:/Users/ott/Desktop/LBT/LUCI/20171024\"";
    alignLuci1 += "";
    alignLuci1 += "ooo1 = \"\"";
    alignLuci1 += "";
    alignLuci1 += "luci = 1";
    alignLuci1 += "";
    alignLuci1 += "{";
    alignLuci1 += "if (nelements(manualfilenames) > 0) {";
    alignLuci1 += "filenames_ = manualfilenames";
    alignLuci1 += "} else {";
    alignLuci1 += "filenames_ = findfile(\"luci\" + luci + \"*.fits\")";
    alignLuci1 += "n = nelements(filenames_)";
    alignLuci1 += "filenames_ = filenames_[n-2:n]";
    alignLuci1 += "updategui \"luci1files\", filenames_";
    alignLuci1 += "}";
    alignLuci1 += "Diff_ = preparediff(filenames_)";
    alignLuci1 += "MaskIm_ = readfits(filenames_[3])";
    alignLuci1 += "Mask_ = preparemask(MaskIm_, median(readfits(filenames_[1]))/3)";
    alignLuci1 += "OMask_ = outline(Mask_, 2)";
    alignLuci1 += "Sim = preparesim(Diff_, OMask_)";
    alignLuci1 += "view Sim,/scale995,/linear";
    alignLuci1 += "message \"Please mark star and mask positions using the \\\"m\\\" key\"";
    alignLuci1 += "cent_ = markpos(Sim)";
    alignLuci1 += "result1_ = offsets(cent_, Diff_, Mask_, luci)";
    alignLuci1 += "updategui \"luci1align\", result1_";
    alignLuci1 += "luci1dx = result1_[1]";
    alignLuci1 += "luci1dy = result1_[2]";
    alignLuci1 += "luci1dphi = result1_[3]";
    alignLuci1 += "ooo1 += \"luci1: xshift: \" + result1_[1] + \" yshift: \" + result1_[2] + \" angle: \" + result1_[3]";
    alignLuci1 += "preview1 = shift(rotate(Diff_, -result1_[3], 1009.5, 1033.4), result1_[4], result1_[5]) + 2 * max(Diff_) * OMask_";
    alignLuci1 += "view preview1, /scale995, /linear";
    alignLuci1 += "}";

    alignLuci2 += "cd \"/newdata\"";
    alignLuci2 += "//cd \"c:/Users/ott/Desktop/LBT/LUCI/20171024\"";
    alignLuci2 += "";
    alignLuci2 += "ooo2 = \"\"";
    alignLuci2 += "";
    alignLuci2 += "luci = 2";
    alignLuci2 += "";
    alignLuci2 += "{";
    alignLuci2 += "if (nelements(manualfilenames) > 0) {";
    alignLuci2 += "filenames_ = manualfilenames";
    alignLuci2 += "} else {";
    alignLuci2 += "filenames_ = findfile(\"luci\" + luci + \"*.fits\")";
    alignLuci2 += "n = nelements(filenames_)";
    alignLuci2 += "filenames_ = filenames_[n-2:n]";
    alignLuci2 += "updategui \"luci2files\", filenames_";
    alignLuci2 += "}";
    alignLuci2 += "Diff_ = preparediff(filenames_)";
    alignLuci2 += "MaskIm_ = readfits(filenames_[3])";
    alignLuci2 += "Mask_ = preparemask(MaskIm_, median(readfits(filenames_[1]))/3)";
    alignLuci2 += "OMask_ = outline(Mask_, 2)";
    alignLuci2 += "Sim = preparesim(Diff_, OMask_)";
    alignLuci2 += "view Sim,/scale995,/linear";
    alignLuci2 += "message \"Please mark star and mask positions using the \\\"m\\\" key\"";
    alignLuci2 += "cent_ = markpos(Sim)";
    alignLuci2 += "result2_ = offsets(cent_, Diff_, Mask_, luci)";
    alignLuci2 += "updategui \"luci2align\", result2_";
    alignLuci2 += "luci2dx = result2_[1]";
    alignLuci2 += "luci2dy = result2_[2]";
    alignLuci2 += "luci2dphi = result2_[3]";
    alignLuci2 += "ooo2 += \"luci2: xshift: \" + result2_[1] + \" yshift: \" + result2_[2] + \" angle: \" + result2_[3]";
    alignLuci2 += "preview2 = shift(rotate(Diff_, -result2_[3], 1009.5, 1033.4), result2_[4], result2_[5]) + 2 * max(Diff_) * OMask_";
    alignLuci2 += "view preview2, /scale995, /linear";
    alignLuci2 += "}";
}

dpLuciMaskAlignDialog::dpLuciMaskAlignDialog(QWidget *parent) : QDialog(parent) {
    initialiseScripts();
    firsttime = true;
    mainLayout = new QVBoxLayout;

    setLayout(mainLayout);

    QLabel *luci1label = new QLabel("<font size=+3>LUCI 1</font>", this);
    QLabel *luci2label = new QLabel("<font size=+3>LUCI 2</font>", this);
    autoFile1 = new QCheckBox("Use last 3 files in /newdata", this);
    autoFile1->setChecked(true);
    autoFile2 = new QCheckBox("Use last 3 files in /newdata", this);
    autoFile2->setChecked(true);

    QLabel *sky1label = new QLabel("Sky:", this);
    QLabel *obj1label = new QLabel("Object:", this);
    QLabel *mask1label = new QLabel("Mask:", this);
    QLabel *sky2label = new QLabel("Sky:", this);
    QLabel *obj2label = new QLabel("Object:", this);
    QLabel *mask2label = new QLabel("Mask:", this);
    sky1 = new QLineEdit(this);
    sky1->setEnabled(false);
    obj1 = new QLineEdit(this);
    obj1->setEnabled(false);
    mask1 = new QLineEdit(this);
    mask1->setEnabled(false);
    sky2 = new QLineEdit(this);
    sky2->setEnabled(false);
    obj2 = new QLineEdit(this);
    obj2->setEnabled(false);
    mask2 = new QLineEdit(this);
    mask2->setEnabled(false);

    align1 = new QPushButton("Align LUCI 1", this);
    align2 = new QPushButton("Align LUCI 2", this);

    QLabel *dx1label = new QLabel("dx:", this);
    QLabel *dy1label = new QLabel("dy:", this);
    QLabel *dphi1label = new QLabel("dphi:", this);
    QLabel *dx2label = new QLabel("dx:", this);
    QLabel *dy2label = new QLabel("dy:", this);
    QLabel *dphi2label = new QLabel("dphi:", this);
    dx1 = new QLineEdit(this);
    dx1->setValidator(new QDoubleValidator(dx1));
    dy1 = new QLineEdit(this);
    dy1->setValidator(new QDoubleValidator(dy1));
    dphi1 = new QLineEdit(this);
    dphi1->setValidator(new QDoubleValidator(dphi1));
    dx2 = new QLineEdit(this);
    dx2->setValidator(new QDoubleValidator(dx2));
    dy2 = new QLineEdit(this);
    dy2->setValidator(new QDoubleValidator(dy2));
    dphi2 = new QLineEdit(this);
    dphi2->setValidator(new QDoubleValidator(dphi2));

    apply1 = new QPushButton("Move LUCI 1", this);
    apply2 = new QPushButton("Move LUCI 2", this);
    applyboth = new QPushButton("Move both LUCIs", this);

    QGridLayout *gridLayout = new QGridLayout();
    gridLayout->setVerticalSpacing(10);
    gridLayout->addWidget(luci1label, 0, 0, 1, 1);

    gridLayout->addWidget(autoFile1, 0, 1, 1, 5);

    gridLayout->addWidget(sky1label, 1, 0);
    gridLayout->addWidget(sky1, 1, 1, 1, 5);
    gridLayout->addWidget(obj1label, 2, 0);
    gridLayout->addWidget(obj1, 2, 1, 1, 5);
    gridLayout->addWidget(mask1label, 3, 0);
    gridLayout->addWidget(mask1, 3, 1, 1, 5);
    gridLayout->addWidget(align1, 5, 1, 1, 2);

    gridLayout->addWidget(dx1label, 6, 0);
    gridLayout->addWidget(dx1, 6, 1);
    gridLayout->addWidget(dy1label, 6, 2);
    gridLayout->addWidget(dy1, 6, 3);
    gridLayout->addWidget(dphi1label, 6, 4);
    gridLayout->addWidget(dphi1, 6, 5);

    gridLayout->addWidget(luci2label, 7, 0, 1, 1);

    gridLayout->addWidget(autoFile2, 7, 1, 1, 5);

    gridLayout->addWidget(sky2label, 8, 0);
    gridLayout->addWidget(sky2, 8, 1, 1, 5);
    gridLayout->addWidget(obj2label, 9, 0);
    gridLayout->addWidget(obj2, 9, 1, 1, 5);
    gridLayout->addWidget(mask2label, 10, 0);
    gridLayout->addWidget(mask2, 10, 1, 1, 5);

    gridLayout->addWidget(align2, 11, 1, 1, 2);

    gridLayout->addWidget(dx2label, 17, 0);
    gridLayout->addWidget(dx2, 17, 1);
    gridLayout->addWidget(dy2label, 17, 2);
    gridLayout->addWidget(dy2, 17, 3);
    gridLayout->addWidget(dphi2label, 17, 4);
    gridLayout->addWidget(dphi2, 17, 5);

    gridLayout->addWidget(apply1, 18, 1);
    gridLayout->addWidget(apply2, 18, 3);
    gridLayout->addWidget(applyboth, 18, 5);

    mainLayout->addLayout(gridLayout);

    QPushButton *closeButton = new QPushButton("Close");


    connect(align1, SIGNAL(clicked()), SLOT(align1Clicked()));
    connect(align2, SIGNAL(clicked()), SLOT(align2Clicked()));
    connect(closeButton, SIGNAL(clicked()), SLOT(hide()));

    connect(autoFile1, SIGNAL(clicked(bool)), sky1, SLOT(setDisabled(bool)));
    connect(autoFile1, SIGNAL(clicked(bool)), obj1, SLOT(setDisabled(bool)));
    connect(autoFile1, SIGNAL(clicked(bool)), mask1, SLOT(setDisabled(bool)));
    connect(autoFile2, SIGNAL(clicked(bool)), sky2, SLOT(setDisabled(bool)));
    connect(autoFile2, SIGNAL(clicked(bool)), obj2, SLOT(setDisabled(bool)));
    connect(autoFile2, SIGNAL(clicked(bool)), mask2, SLOT(setDisabled(bool)));

    connect(apply1, SIGNAL(clicked()), SLOT(apply1Clicked()));
    connect(apply2, SIGNAL(clicked()), SLOT(apply2Clicked()));
    connect(applyboth, SIGNAL(clicked()), SLOT(applybothClicked()));

    mainLayout->addWidget(closeButton);

    adjustSize();
    setFixedSize(size());
}

void dpLuciMaskAlignDialog::align1Clicked() {
//    hide();
    if (!autoFile1->isChecked()) {
        if ((sky1->text() == "") || (obj1->text() == "") || (mask1->text() == "")) {
            QMessageBox::information(this, "LUCI Mask Align", "Not all file names set");
            return;
        } else {
            dpuser_widget->executeCommand("manualfilenames = [\"" + sky1->text() + "\", \"" + obj1->text() + "\", \"" + mask1->text() + "\"]");
        }
    } else {
        dpuser_widget->executeCommand("manualfilenames = \"\"");
    }
    dx1->setText("");
    dy1->setText("");
    dphi1->setText("");
//    dpuser_widget->executeCommand("run getenv(\"DPUSER\") + \"/maskalign1.dpuserscript\"");
    dpuser_widget->executeScript(alignLuci1);
}

void dpLuciMaskAlignDialog::align2Clicked() {
//    hide();
    if (!autoFile2->isChecked()) {
        if ((sky2->text() == "") || (obj2->text() == "") || (mask2->text() == "")) {
            QMessageBox::information(this, "LUCI Mask Align", "Not all file names set");
            return;
        } else {
            dpuser_widget->executeCommand("manualfilenames = [\"" + sky2->text() + "\", \"" + obj2->text() + "\", \"" + mask2->text() + "\"]");
        }
    } else {
        dpuser_widget->executeCommand("manualfilenames = \"\"");
    }
    dx2->setText("");
    dy2->setText("");
    dphi2->setText("");
//    dpuser_widget->executeCommand("run getenv(\"DPUSER\") + \"/maskalign2.dpuserscript\"");
    dpuser_widget->executeScript(alignLuci2);
}

void dpLuciMaskAlignDialog::apply1Clicked() {
    if ((dx1->text() == "") || (dy1->text() == "") || (dphi1->text() == "")) {
        QMessageBox::information(this, "LUCI Mask Align", "Not all values present");
    } else {
        dpuser_widget->executeCommand("createxml " + dx1->text() + ", " + dy1->text() + ", " + dphi1->text() + ", 0, 0, 0");
    }
}

void dpLuciMaskAlignDialog::apply2Clicked() {
    if ((dx2->text() == "") || (dy2->text() == "") || (dphi2->text() == "")) {
        QMessageBox::information(this, "LUCI Mask Align", "Not all values present");
    } else {
        dpuser_widget->executeCommand("createxml 0, 0, 0, " + dx2->text() + ", " + dy2->text() + ", " + dphi2->text());
    }
}

void dpLuciMaskAlignDialog::applybothClicked() {
    if ((dx1->text() == "") || (dy1->text() == "") || (dphi1->text() == "") || (dx2->text() == "") || (dy2->text() == "") || (dphi2->text() == "")) {
        QMessageBox::information(this, "LUCI Mask Align", "Not all values present");
    } else {
        dpuser_widget->executeCommand("createxml " + dx1->text() + ", " + dy1->text() + ", " + dphi1->text() + ", " + dx2->text() + ", " + dy2->text() + ", " + dphi2->text());
    }
}

void dpLuciMaskAlignDialog::updateValue(int which, double value) {
    switch(which) {
        case 0:
            dx1->setText(QString::number(value));
        break;
        case 1:
            dy1->setText(QString::number(value));
        break;
        case 2:
            dphi1->setText(QString::number(value));
            show();
        break;
        case 3:
            dx2->setText(QString::number(value));
        break;
        case 4:
            dy2->setText(QString::number(value));
        break;
        case 5:
            dphi2->setText(QString::number(value));
            show();
        break;
        default:
        break;
    }
}
void dpLuciMaskAlignDialog::updateFilename(int which, QString value) {
    switch(which) {
        case 0:
            sky1->setText(value);
        break;
        case 1:
            obj1->setText(value);
        break;
        case 2:
            mask1->setText(value);
        break;
        case 3:
            sky2->setText(value);
        break;
        case 4:
            obj2->setText(value);
        break;
        case 5:
            mask2->setText(value);
        break;
        default:
        break;
    }
}

void dpLuciMaskAlignDialog::show() {
    if (firsttime) {
        dpuser_widget->executeScript(lucitools);
        firsttime = false;
    }
    QDialog::show();
}
