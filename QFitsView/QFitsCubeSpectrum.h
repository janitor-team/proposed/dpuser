#ifndef QFITSPLOT_H
#define QFITSPLOT_H

#include <QWidget>
#include <QString>
#include <QCheckBox>
#include <QComboBox>
#include <QSpinBox>
#include <QLineEdit>
#include <QScrollBar>

#include "fits.h"
#include "QFitsView1D.h"
#include "dialogs.h"
#include "QFitsToolBar.h"

class QFitsMainWindow;
class QFitsSingleBuffer;
class QFitsCubeSpectrumViewer;   // declared below
class QFitsMarkers;
class QFitsSpectrumRangeControl;

enum dpSpectrumMode { SinglePixel,
                      Circular,
                      CircularAnnular };

//------------------------------------------------------------------------------
//         QFitsCubeSpectrum
//------------------------------------------------------------------------------
class QFitsCubeSpectrum : public QWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsCubeSpectrum(QFitsMainWindow *parent);
    ~QFitsCubeSpectrum() {}

    void setCentre(int, int);
    void saveAscii(QFitsSingleBuffer*);
    void saveFitsSpectrum(QFitsSingleBuffer*);
    void newBufferSpectrum(QFitsSingleBuffer*);
    void saveStandardStar(QFitsSingleBuffer*, QFitsMarkers*);
    void changeWorkArray();
    void changeWorkArrayTable();
    void setIgnore(bool, double);
    QFitsCubeSpectrumViewer* getPlotter()       { return plotter; }
    int getCenX()                               { return cenx; }
    int getCenY()                               { return ceny; }
    bool getAutoScale()                         { return autoScaleButton->isChecked(); }
    dpSpectrumMode  getSpectrumMethod()         { return method; }
    int getStatistics()                         { return statistics; }
    void spectrumRangeChanged(double, double);
    void enableControls(bool);
    void resetRangeControl();

protected:
    void paintEvent(QPaintEvent *);
    void closeEvent(QCloseEvent *);
    void resizeEvent(QResizeEvent *);
    void enterEvent(QEvent *);

//----- Slots -----
public slots:
    void setR1(int);
    void setR2(int);
    void setMin(const QString &);
    void setMax(const QString &);
    void useStandardStarClicked();
    void newSlice(int);
    void setMethod(int);
    void setStatistics(int);
    void setupMarkers();

//----- Signals -----
signals:
    void closing();
    void sliceSelected(int);
    void statusbartext(const QString &);
    void statisticsChanged();

//----- Members -----
private:
    dpSpectrumMode              method;
    int                         statistics;
    int                         r1, r2,
                                cenx, ceny;
    bool                        ignoreInCube;
    double                      ignoreValue;
    Fits                        standardStar;
    QLineEdit                   *minEdit,
                                *maxEdit;
    QSpinBox                    *r1edit,
                                *r2edit;
    QCheckBox                   *autoScaleButton,
                                *useStandardStar;
    QLabel                      *r1label,
                                *r2label,
                                *minlabel,
                                *maxlabel;
    QComboBox                   *methodButton,
                                *statisticsButton;
    QFitsCubeSpectrumViewer     *plotter;
    QFitsSimplestButton         *hideButton;
    QFitsSpectrumRangeControl   *rangeControl;
};

//------------------------------------------------------------------------------
//         QFitsSpectrumRangeControl
//------------------------------------------------------------------------------
class QFitsSpectrumRangeControl : public QWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsSpectrumRangeControl(QWidget*);
    ~QFitsSpectrumRangeControl() {}

    void setSpectrum(int, double, double, double);
    void setPhysicalRange(double, double);
    void updateScroller();

protected:
    void resizeEvent(QResizeEvent *);

private:
    void setChannelRange(int, int);
    void resizeControls(int, int);

//----- Slots -----
private slots:
    void setChannelMin(int min);
    void setChannelMax(int max);
    void setPhysicalMin(double min);
    void setPhysicalMax(double max);
    void scrollerMoved(int);

//----- Signals -----
signals:
    void channelRangeChanged();

//----- Members -----
 private:
    QSpinBox        *channelMin,
                    *channelMax;
    QDoubleSpinBox  *physicalMin,
                    *physicalMax;
    QScrollBar      *rangeScroller;
    double          crpix,
                    crval,
                    cdelt;
    int             nelements;
    bool            updateAll,
                    enablePhysicalRange;
};

//------------------------------------------------------------------------------
//         QFitsCubeSpectrumViewer
//------------------------------------------------------------------------------
class QFitsCubeSpectrumViewer : public QFitsView1D {
    Q_OBJECT
//----- Functions -----
public:
    QFitsCubeSpectrumViewer(QFitsCubeSpectrum *parent);
    ~QFitsCubeSpectrumViewer();

    void setCData(const Fits &);
    void removeCData();
    void doUpdate();
    void updateYRange(int vMin, int vMax);
    virtual void setXRange(const double &min, const double &max);

protected:
    void paintEvent(QPaintEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void enterEvent(QEvent *e);
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);

//----- Slots -----
//----- Signals -----
//----- Members -----
public:
    QFitsCubeSpectrum* myParent;
    double *cdata,
           cmin,
           cmax;
    int pressedKey,
        pressedKeyLeft,
        pressedKeyRight;
};

#endif /* QFITSPLOT_H */
