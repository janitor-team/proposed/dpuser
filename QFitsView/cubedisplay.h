/****************************************************************************
** Form interface generated from reading ui file 'cubedisplay.ui'
**
** Created: Fri Dec 22 15:00:08 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.5   edited Aug 31 12:13 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef CUBEDISPLAY_H
#define CUBEDISPLAY_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QButtonGroup;
class QRadioButton;
class QGroupBox;
class QLabel;
class QSpinBox;
class QCheckBox;
class QLineEdit;
class QSlider;
class QPushButton;

class CubeDisplay : public QDialog
{
    Q_OBJECT

public:
    CubeDisplay( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~CubeDisplay();

    QButtonGroup* Display;
    QRadioButton* DisplayAsMedian;
    QRadioButton* DisplayAsAvg;
    QRadioButton* DisplayAsLinemap;
    QRadioButton* DisplayAsSingle;
    QGroupBox* groupBox1;
    QLabel* TextLabel1_3_2;
    QSpinBox* MovieSpeed;
    QCheckBox* autoScale;
    QSpinBox* LinemapCenter;
    QLabel* TextLabel1_2_2_2_3;
    QLabel* TextLabel1_2_2_2_2_2;
    QLineEdit* LinemapWidth1W;
    QLabel* TextLabel1_2_2_3;
    QSpinBox* LinemapWidth;
    QCheckBox* LinemapDoCont2;
    QLineEdit* LinemapCont1W;
    QLabel* textLabel2_2;
    QSpinBox* LinemapCont1;
    QLineEdit* LinemapCenterW;
    QLineEdit* LinemapCont2W;
    QSpinBox* LinemapCont2;
    QLabel* textLabel5_2;
    QCheckBox* LinemapDoCont1;
    QLabel* textLabel1_2;
    QLineEdit* LinemapWidth2W;
    QSpinBox* LinemapWidth2;
    QLabel* textLabel4_2;
    QSpinBox* LinemapWidth1;
    QLabel* textLabel3_2;
    QLabel* TextLabel1_2_3;
    QLineEdit* LinemapWidthW;
    QSlider* WavelengthSlider;
    QLabel* LinemapInfo;
    QLabel* WavelengthSliderLabel1;
    QLabel* WavelengthSliderLabel2;
    QPushButton* PushButton1;

protected:
    QVBoxLayout* CubeDisplayLayout;
    QVBoxLayout* layout23;
    QGridLayout* layout21;
    QVBoxLayout* DisplayLayout;
    QGridLayout* layout6;
    QSpacerItem* spacer1;
    QSpacerItem* spacer30;
    QSpacerItem* spacer2;
    QVBoxLayout* groupBox1Layout;
    QVBoxLayout* layout20;
    QGridLayout* layout8;
    QSpacerItem* spacer6_2;
    QSpacerItem* spacer11_2;
    QSpacerItem* spacer5_2;
    QSpacerItem* spacer10_2;
    QGridLayout* layout9;
    QHBoxLayout* layout10;
    QSpacerItem* spacer3;
    QSpacerItem* spacer3_2;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;

};

#endif // CUBEDISPLAY_H
