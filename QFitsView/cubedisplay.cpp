/****************************************************************************
** Form implementation generated from reading ui file 'cubedisplay.ui'
**
** Created: Fri Dec 22 15:00:08 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.5   edited Aug 31 12:13 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "cubedisplay.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qbuttongroup.h>
#include <qradiobutton.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qspinbox.h>
#include <qcheckbox.h>
#include <qlineedit.h>
#include <qslider.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

static const char* const image0_data[] = { 
"16 16 5 1",
"b c #000000",
"# c #0000ff",
". c #009a9c",
"a c #00ffff",
"c c #cecfce",
"...........##...",
".........##aa#..",
"........#aaaa#..",
".......#aaaaaa#.",
".....##aaaaa##..",
"....#aaaaa##....",
"...#aaaa##......",
".##aaa##b.......",
"#aaa##..b.......",
"#a##...bbb......",
".#.....bbb......",
"......b.b.b.....",
"......b.b.b.....",
".....b..b..b....",
".....b.ccc.b....",
"....ccc...ccc..."};


/*
 *  Constructs a CubeDisplay as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
CubeDisplay::CubeDisplay( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl ),
      image0( (const char **) image0_data )
{
    if ( !name )
	setName( "CubeDisplay" );
    setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, sizePolicy().hasHeightForWidth() ) );
    setIcon( image0 );
    CubeDisplayLayout = new QVBoxLayout( this, 11, 6, "CubeDisplayLayout"); 
    CubeDisplayLayout->setResizeMode( QLayout::Fixed );

    layout23 = new QVBoxLayout( 0, 0, 6, "layout23"); 

    layout21 = new QGridLayout( 0, 1, 1, 0, 6, "layout21"); 

    Display = new QButtonGroup( this, "Display" );
    Display->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, Display->sizePolicy().hasHeightForWidth() ) );
    Display->setExclusive( TRUE );
    Display->setRadioButtonExclusive( FALSE );
    Display->setColumnLayout(0, Qt::Vertical );
    Display->layout()->setSpacing( 6 );
    Display->layout()->setMargin( 11 );
    DisplayLayout = new QVBoxLayout( Display->layout() );
    DisplayLayout->setAlignment( Qt::AlignTop );

    layout6 = new QGridLayout( 0, 1, 1, 0, 6, "layout6"); 
    spacer1 = new QSpacerItem( 294, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout6->addMultiCell( spacer1, 0, 0, 1, 2 );
    spacer30 = new QSpacerItem( 227, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout6->addItem( spacer30, 2, 1 );

    DisplayAsMedian = new QRadioButton( Display, "DisplayAsMedian" );
    DisplayAsMedian->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, DisplayAsMedian->sizePolicy().hasHeightForWidth() ) );
    DisplayAsMedian->setChecked( FALSE );
    Display->insert( DisplayAsMedian, -1 );

    layout6->addWidget( DisplayAsMedian, 2, 0 );

    DisplayAsAvg = new QRadioButton( Display, "DisplayAsAvg" );
    DisplayAsAvg->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, DisplayAsAvg->sizePolicy().hasHeightForWidth() ) );
    DisplayAsAvg->setChecked( FALSE );
    Display->insert( DisplayAsAvg, -1 );

    layout6->addWidget( DisplayAsAvg, 1, 0 );

    DisplayAsLinemap = new QRadioButton( Display, "DisplayAsLinemap" );
    DisplayAsLinemap->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, DisplayAsLinemap->sizePolicy().hasHeightForWidth() ) );
    DisplayAsLinemap->setChecked( FALSE );
    Display->insert( DisplayAsLinemap, -1 );

    layout6->addMultiCellWidget( DisplayAsLinemap, 3, 3, 0, 2 );

    DisplayAsSingle = new QRadioButton( Display, "DisplayAsSingle" );
    DisplayAsSingle->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, DisplayAsSingle->sizePolicy().hasHeightForWidth() ) );
    DisplayAsSingle->setChecked( TRUE );

    layout6->addWidget( DisplayAsSingle, 0, 0 );
    spacer2 = new QSpacerItem( 294, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout6->addMultiCell( spacer2, 1, 1, 1, 2 );
    DisplayLayout->addLayout( layout6 );

    layout21->addWidget( Display, 0, 0 );

    groupBox1 = new QGroupBox( this, "groupBox1" );
    groupBox1->setColumnLayout(0, Qt::Vertical );
    groupBox1->layout()->setSpacing( 6 );
    groupBox1->layout()->setMargin( 11 );
    groupBox1Layout = new QVBoxLayout( groupBox1->layout() );
    groupBox1Layout->setAlignment( Qt::AlignTop );

    layout20 = new QVBoxLayout( 0, 0, 6, "layout20"); 

    TextLabel1_3_2 = new QLabel( groupBox1, "TextLabel1_3_2" );
    TextLabel1_3_2->setEnabled( TRUE );
    TextLabel1_3_2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, TextLabel1_3_2->sizePolicy().hasHeightForWidth() ) );
    TextLabel1_3_2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignLeft ) );
    layout20->addWidget( TextLabel1_3_2 );

    MovieSpeed = new QSpinBox( groupBox1, "MovieSpeed" );
    MovieSpeed->setEnabled( TRUE );
    MovieSpeed->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, MovieSpeed->sizePolicy().hasHeightForWidth() ) );
    MovieSpeed->setMaxValue( 9999 );
    MovieSpeed->setLineStep( 100 );
    MovieSpeed->setValue( 200 );
    layout20->addWidget( MovieSpeed );

    autoScale = new QCheckBox( groupBox1, "autoScale" );
    autoScale->setEnabled( TRUE );
    autoScale->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, autoScale->sizePolicy().hasHeightForWidth() ) );
    autoScale->setChecked( TRUE );
    layout20->addWidget( autoScale );
    groupBox1Layout->addLayout( layout20 );

    layout21->addWidget( groupBox1, 0, 1 );
    layout23->addLayout( layout21 );

    layout8 = new QGridLayout( 0, 1, 1, 0, 6, "layout8"); 

    LinemapCenter = new QSpinBox( this, "LinemapCenter" );
    LinemapCenter->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, LinemapCenter->sizePolicy().hasHeightForWidth() ) );
    LinemapCenter->setMaxValue( 9999 );
    LinemapCenter->setMinValue( 1 );
    LinemapCenter->setValue( 1 );

    layout8->addWidget( LinemapCenter, 1, 1 );

    TextLabel1_2_2_2_3 = new QLabel( this, "TextLabel1_2_2_2_3" );
    TextLabel1_2_2_2_3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, TextLabel1_2_2_2_3->sizePolicy().hasHeightForWidth() ) );

    layout8->addWidget( TextLabel1_2_2_2_3, 2, 2 );
    spacer6_2 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout8->addItem( spacer6_2, 1, 4 );

    TextLabel1_2_2_2_2_2 = new QLabel( this, "TextLabel1_2_2_2_2_2" );
    TextLabel1_2_2_2_2_2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, TextLabel1_2_2_2_2_2->sizePolicy().hasHeightForWidth() ) );

    layout8->addWidget( TextLabel1_2_2_2_2_2, 3, 2 );

    LinemapWidth1W = new QLineEdit( this, "LinemapWidth1W" );

    layout8->addWidget( LinemapWidth1W, 2, 7 );

    TextLabel1_2_2_3 = new QLabel( this, "TextLabel1_2_2_3" );
    TextLabel1_2_2_3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, TextLabel1_2_2_3->sizePolicy().hasHeightForWidth() ) );

    layout8->addWidget( TextLabel1_2_2_3, 1, 2 );

    LinemapWidth = new QSpinBox( this, "LinemapWidth" );
    LinemapWidth->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, LinemapWidth->sizePolicy().hasHeightForWidth() ) );
    LinemapWidth->setMaxValue( 9999 );
    LinemapWidth->setMinValue( 1 );
    LinemapWidth->setValue( 1 );

    layout8->addWidget( LinemapWidth, 1, 3 );
    spacer11_2 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout8->addItem( spacer11_2, 3, 4 );

    LinemapDoCont2 = new QCheckBox( this, "LinemapDoCont2" );
    LinemapDoCont2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, LinemapDoCont2->sizePolicy().hasHeightForWidth() ) );

    layout8->addWidget( LinemapDoCont2, 3, 0 );

    LinemapCont1W = new QLineEdit( this, "LinemapCont1W" );

    layout8->addWidget( LinemapCont1W, 2, 5 );

    textLabel2_2 = new QLabel( this, "textLabel2_2" );

    layout8->addWidget( textLabel2_2, 0, 5 );

    LinemapCont1 = new QSpinBox( this, "LinemapCont1" );
    LinemapCont1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, LinemapCont1->sizePolicy().hasHeightForWidth() ) );
    LinemapCont1->setMaxValue( 9999 );
    LinemapCont1->setMinValue( -9999 );
    LinemapCont1->setValue( -10 );

    layout8->addWidget( LinemapCont1, 2, 1 );

    LinemapCenterW = new QLineEdit( this, "LinemapCenterW" );

    layout8->addWidget( LinemapCenterW, 1, 5 );

    LinemapCont2W = new QLineEdit( this, "LinemapCont2W" );

    layout8->addWidget( LinemapCont2W, 3, 5 );

    LinemapCont2 = new QSpinBox( this, "LinemapCont2" );
    LinemapCont2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, LinemapCont2->sizePolicy().hasHeightForWidth() ) );
    LinemapCont2->setMaxValue( 9999 );
    LinemapCont2->setMinValue( -9999 );
    LinemapCont2->setValue( 10 );

    layout8->addWidget( LinemapCont2, 3, 1 );

    textLabel5_2 = new QLabel( this, "textLabel5_2" );
    textLabel5_2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, textLabel5_2->sizePolicy().hasHeightForWidth() ) );

    layout8->addWidget( textLabel5_2, 3, 6 );
    spacer5_2 = new QSpacerItem( 110, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout8->addMultiCell( spacer5_2, 0, 0, 2, 4 );

    LinemapDoCont1 = new QCheckBox( this, "LinemapDoCont1" );
    LinemapDoCont1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, LinemapDoCont1->sizePolicy().hasHeightForWidth() ) );

    layout8->addWidget( LinemapDoCont1, 2, 0 );

    textLabel1_2 = new QLabel( this, "textLabel1_2" );

    layout8->addWidget( textLabel1_2, 0, 1 );

    LinemapWidth2W = new QLineEdit( this, "LinemapWidth2W" );

    layout8->addWidget( LinemapWidth2W, 3, 7 );

    LinemapWidth2 = new QSpinBox( this, "LinemapWidth2" );
    LinemapWidth2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, LinemapWidth2->sizePolicy().hasHeightForWidth() ) );
    LinemapWidth2->setMaxValue( 9999 );
    LinemapWidth2->setMinValue( 1 );
    LinemapWidth2->setValue( 1 );

    layout8->addWidget( LinemapWidth2, 3, 3 );

    textLabel4_2 = new QLabel( this, "textLabel4_2" );
    textLabel4_2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, textLabel4_2->sizePolicy().hasHeightForWidth() ) );

    layout8->addWidget( textLabel4_2, 2, 6 );
    spacer10_2 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout8->addItem( spacer10_2, 2, 4 );

    LinemapWidth1 = new QSpinBox( this, "LinemapWidth1" );
    LinemapWidth1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, LinemapWidth1->sizePolicy().hasHeightForWidth() ) );
    LinemapWidth1->setMaxValue( 9999 );
    LinemapWidth1->setMinValue( 1 );
    LinemapWidth1->setValue( 1 );

    layout8->addWidget( LinemapWidth1, 2, 3 );

    textLabel3_2 = new QLabel( this, "textLabel3_2" );
    textLabel3_2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, textLabel3_2->sizePolicy().hasHeightForWidth() ) );

    layout8->addWidget( textLabel3_2, 1, 6 );

    TextLabel1_2_3 = new QLabel( this, "TextLabel1_2_3" );
    TextLabel1_2_3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, TextLabel1_2_3->sizePolicy().hasHeightForWidth() ) );

    layout8->addWidget( TextLabel1_2_3, 1, 0 );

    LinemapWidthW = new QLineEdit( this, "LinemapWidthW" );

    layout8->addWidget( LinemapWidthW, 1, 7 );
    layout23->addLayout( layout8 );

    layout9 = new QGridLayout( 0, 1, 1, 0, 6, "layout9"); 

    WavelengthSlider = new QSlider( this, "WavelengthSlider" );
    WavelengthSlider->setMinValue( 1 );
    WavelengthSlider->setMaxValue( 9999 );
    WavelengthSlider->setValue( 500 );
    WavelengthSlider->setTracking( TRUE );
    WavelengthSlider->setOrientation( QSlider::Horizontal );
    WavelengthSlider->setTickmarks( QSlider::NoMarks );
    WavelengthSlider->setTickInterval( 1000 );

    layout9->addMultiCellWidget( WavelengthSlider, 1, 1, 0, 2 );

    LinemapInfo = new QLabel( this, "LinemapInfo" );
    LinemapInfo->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)1, 0, 0, LinemapInfo->sizePolicy().hasHeightForWidth() ) );
    LinemapInfo->setAlignment( int( QLabel::AlignCenter ) );

    layout9->addWidget( LinemapInfo, 0, 1 );

    WavelengthSliderLabel1 = new QLabel( this, "WavelengthSliderLabel1" );
    WavelengthSliderLabel1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, WavelengthSliderLabel1->sizePolicy().hasHeightForWidth() ) );
    WavelengthSliderLabel1->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignLeft ) );

    layout9->addWidget( WavelengthSliderLabel1, 0, 0 );

    WavelengthSliderLabel2 = new QLabel( this, "WavelengthSliderLabel2" );
    WavelengthSliderLabel2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, WavelengthSliderLabel2->sizePolicy().hasHeightForWidth() ) );
    WavelengthSliderLabel2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout9->addWidget( WavelengthSliderLabel2, 0, 2 );
    layout23->addLayout( layout9 );

    layout10 = new QHBoxLayout( 0, 0, 6, "layout10"); 
    spacer3 = new QSpacerItem( 91, 31, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout10->addItem( spacer3 );

    PushButton1 = new QPushButton( this, "PushButton1" );
    PushButton1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)1, 0, 0, PushButton1->sizePolicy().hasHeightForWidth() ) );
    layout10->addWidget( PushButton1 );
    spacer3_2 = new QSpacerItem( 91, 31, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout10->addItem( spacer3_2 );
    layout23->addLayout( layout10 );
    CubeDisplayLayout->addLayout( layout23 );
    languageChange();
    resize( QSize(514, 412).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( PushButton1, SIGNAL( clicked() ), this, SLOT( accept() ) );
    connect( WavelengthSlider, SIGNAL( valueChanged(int) ), LinemapCenter, SLOT( setValue(int) ) );

    // tab order
    setTabOrder( DisplayAsSingle, DisplayAsAvg );
    setTabOrder( DisplayAsAvg, DisplayAsMedian );
    setTabOrder( DisplayAsMedian, DisplayAsLinemap );
    setTabOrder( DisplayAsLinemap, MovieSpeed );
    setTabOrder( MovieSpeed, autoScale );
    setTabOrder( autoScale, LinemapCenter );
    setTabOrder( LinemapCenter, LinemapWidth );
    setTabOrder( LinemapWidth, LinemapCenterW );
    setTabOrder( LinemapCenterW, LinemapWidthW );
    setTabOrder( LinemapWidthW, LinemapDoCont1 );
    setTabOrder( LinemapDoCont1, LinemapCont1 );
    setTabOrder( LinemapCont1, LinemapWidth1 );
    setTabOrder( LinemapWidth1, LinemapCont1W );
    setTabOrder( LinemapCont1W, LinemapWidth1W );
    setTabOrder( LinemapWidth1W, LinemapDoCont2 );
    setTabOrder( LinemapDoCont2, LinemapCont2 );
    setTabOrder( LinemapCont2, LinemapWidth2 );
    setTabOrder( LinemapWidth2, LinemapCont2W );
    setTabOrder( LinemapCont2W, LinemapWidth2W );
    setTabOrder( LinemapWidth2W, WavelengthSlider );
    setTabOrder( WavelengthSlider, PushButton1 );
}

/*
 *  Destroys the object and frees any allocated resources
 */
CubeDisplay::~CubeDisplay()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void CubeDisplay::languageChange()
{
    setCaption( tr( "QFitsView cube display" ) );
    Display->setTitle( tr( "CUBE display" ) );
    DisplayAsMedian->setText( tr( "Median" ) );
    DisplayAsAvg->setText( tr( "Average" ) );
    DisplayAsLinemap->setText( tr( "Linemap" ) );
    DisplayAsSingle->setText( tr( "Single" ) );
    groupBox1->setTitle( tr( "CUBE movie" ) );
    TextLabel1_3_2->setText( tr( "Movie speed" ) );
    MovieSpeed->setSuffix( tr( " ms" ) );
    MovieSpeed->setSpecialValueText( tr( "fastest" ) );
    autoScale->setText( tr( "rescale each image" ) );
    TextLabel1_2_2_2_3->setText( tr( "Width" ) );
    TextLabel1_2_2_2_2_2->setText( tr( "Width" ) );
    TextLabel1_2_2_3->setText( tr( "Width" ) );
    LinemapDoCont2->setText( tr( "Cont. Offset" ) );
    textLabel2_2->setText( tr( "Wavelength" ) );
    textLabel5_2->setText( tr( "Width" ) );
    LinemapDoCont1->setText( tr( "Cont. Offset" ) );
    textLabel1_2->setText( tr( "Channels" ) );
    textLabel4_2->setText( tr( "Width" ) );
    textLabel3_2->setText( tr( "Width" ) );
    TextLabel1_2_3->setText( tr( "Central Frame" ) );
    LinemapInfo->setText( tr( "No wavelength information available" ) );
    WavelengthSliderLabel1->setText( tr( "1" ) );
    WavelengthSliderLabel2->setText( tr( "9999" ) );
    PushButton1->setText( tr( "Ok" ) );
}

