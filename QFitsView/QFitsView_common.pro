# For Win32 MinGW setup the object files er stored in seprarate folders
win32_setup {
    win32:qf_static {
        CONFIG(debug, debug|release):MY_DIR = debug
        CONFIG(release, debug|release):MY_DIR = release
    }
    win32:qf_shared {
        CONFIG(debug, debug|release):MY_DIR = debug/shared
        CONFIG(release, debug|release):MY_DIR = release/shared
    }
    win32:vs_proj {
        CONFIG(debug, debug|release):MY_DIR = debug/vs
        CONFIG(release, debug|release):MY_DIR = release/vs
    }
    DESTDIR = $$MY_DIR
    OBJECTS_DIR = $$MY_DIR
    MOC_DIR = $$MY_DIR
}

# ### Common Libraries ################
qf_libs_common{
    contains(DEFINES, HAS_GDL) {
        LIBS += ../lib/$$ARCH$$ARCHBIT/libgdl.a \
                ../lib/$$ARCH$$ARCHBIT/libplplotcxx.a \
                   ../lib/$$ARCH$$ARCHBIT/libplplot.a \
									 ../lib/$$ARCH$$ARCHBIT/libfftw3f.a \
#		   ../lib/$$ARCH$$ARCHBIT/libplplotqt.a \
                   ../lib/$$ARCH$$ARCHBIT/libqsastime.a \
#                   ../lib/$$ARCH$$ARCHBIT/libcsironn.a \
									 ../lib/$$ARCH$$ARCHBIT/libcsirocsa.a
        win32 {
           LIBS += ../lib/$$ARCH$$ARCHBIT/libgnurx.a \
               ../lib/$$ARCH$$ARCHBIT/libxdr.a -lshlwapi -lcomdlg32
        }
#        mac {
#           LIBS += -L/opt/X11/lib -lX11
#        }
    }
    contains(DEFINES, HAS_PYTHON) {
        win32 {
	   INCLUDEPATH += /Programme/Python36/include
           INCLUDEPATH += /Programme/Python36/Lib/site-packages/numpy/core/include
#	   INCLUDEPATH += /Programme/Anaconda3/Lib/site-packages/numpy/core/include
        }
        mac {
           INCLUDEPATH += /Library/Frameworks/Python.framework/Versions/3.6/include/python3.6m
           INCLUDEPATH += /Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/numpy/core/include
        }
        unix:!mac {
            INCLUDEPATH += /usr/include/python3.5m
#						LIBS += -lpython3.5m
        }
		}
    LIBS += ../lib/$$ARCH$$ARCHBIT/libfftw3.a \
#            ../lib/$$ARCH$$ARCHBIT/librfftw.a \
#            ../lib/$$ARCH$$ARCHBIT/libfftw.a \
            ../lib/$$ARCH$$ARCHBIT/libgsl.a \
            ../lib/$$ARCH$$ARCHBIT/libgslcblas.a \
            ../lib/$$ARCH$$ARCHBIT/libxpa.a
    unix:!mac {
        contains(QMAKE_HOST.arch, x86_64):{
            LIBS += ../lib/$$ARCH$$ARCHBIT/libmng.a
        }
    }
    contains(DEFINES, HAS_PGPLOT) {
        LIBS += ../lib/$$ARCH$$ARCHBIT/libcpgplot.a
        mac {
            LIBS += ../lib/$$ARCH$$ARCHBIT/libpgplot_qf.a
        } else {
            LIBS += ../lib/$$ARCH$$ARCHBIT/libpgplot.a
        }
        LIBS += ../lib/$$ARCH$$ARCHBIT/libf2c.a
    }
}

# ### QFitsView sources & headers ################
qf_sources {
    SOURCES =       main.cpp \
                    QFitsMainWindow.cpp \
                    QFitsMainView.cpp \
                    QFitsBuffers/QFitsBaseBuffer.cpp \
                    QFitsBuffers/QFitsSingleBuffer.cpp \
                    QFitsBuffers/QFitsMultiBuffer.cpp \
                    QFitsScroller.cpp \
                    QFitsWidgets/QFitsBaseWidget.cpp \
                    QFitsWidgets/QFitsWidget1D.cpp \
                    QFitsWidgets/QFitsWidget2D.cpp \
                    QFitsWidgets/QFitsWidgetWiregrid.cpp \
                    QFitsWidgets/QFitsWidgetContour.cpp \
                    QFitsWidgets/QFitsWidgetTable.cpp \
                    QFitsViews/QFitsBaseView.cpp \
                    QFitsViews/QFitsView1D.cpp \
                    QFitsViews/QFitsView2D.cpp \
                    QFitsGlobal.cpp \
                    QFitsMarkers.cpp \
                    QFitsTools.cpp \
                    QFitsWedge.cpp \
                    lut.cpp \
                    QFitsCubeSpectrum.cpp \
                    RGBDialog.cpp \
                    QFitsHeaderView.cpp \
                    QFitsToolBar.cpp \
                    dialogs.cpp \
                    events.cpp \
                    imred.cpp \
                    QFitsViewingTools.cpp \
                    qtdpuser.cpp \
                    qt_mainwindow.cpp \
                    qt_mdichild.cpp \
                    highlighter.cpp \
                    QFitsPreferences.cpp \
                    extra/tetrixboard.cpp \
                    extra/tetrixpiece.cpp \
                    extra/tetrixwindow.cpp \
                    QCustomPlot/qcustomplot.cpp \
                    QPgplot.cpp

    HEADERS =       QFitsMainWindow.h \
                    QFitsMainView.h \
                    QFitsBuffers/QFitsBaseBuffer.h \
                    QFitsBuffers/QFitsSingleBuffer.h \
                    QFitsBuffers/QFitsMultiBuffer.h \
                    QFitsScroller.h \
                    QFitsWidgets/QFitsBaseWidget.h \
                    QFitsWidgets/QFitsWidget1D.h \
                    QFitsWidgets/QFitsWidget2D.h \
                    QFitsWidgets/QFitsWidgetWiregrid.h \
                    QFitsWidgets/QFitsWidgetContour.h \
                    QFitsWidgets/QFitsWidgetTable.h \
                    QFitsViews/QFitsBaseView.h \
                    QFitsViews/QFitsView1D.h \
                    QFitsViews/QFitsView2D.h \
                    QFitsGlobal.h \
                    QFitsMarkers.h \
                    QFitsTools.h \
                    QFitsWedge.h \
                    lut.h \
                    QFitsCubeSpectrum.h \
                    RGBDialog.h \
                    QFitsHeaderView.h \
                    QFitsToolBar.h \
                    dialogs.h \
                    events.h \
                    imred.h \
                    QFitsViewingTools.h \
                    qtdpuser.h \
                    qt_mainwindow.h \
                    qt_mdichild.h \
                    highlighter.h \
                    QFitsPreferences.h \
                    extra/tetrixboard.h \
                    extra/tetrixpiece.h \
                    extra/tetrixwindow.h \
                    QCustomPlot/qcustomplot.h \
                    QPgplot.h
}

contains(DEFINES, HAS_VTK) {
  SOURCES += QFitsWidgets/QFitsWidget3D.cpp \
             QFitsViews/QFitsView3D.cpp
  HEADERS += QFitsWidgets/QFitsWidget3D.h \
             QFitsViews/QFitsView3D.h
}

contains(DEFINES, LBT) {
  SOURCES += lbt.cpp
  HEADERS += lbt.h
}

# ### DPUSER sources & headers ################
dp_sources {
    SOURCES +=      ../dpuser/dpuser.yacchelper.cpp \
                    ../dpuser/functions.cpp \
                    ../dpuser/parser/y.tab.cpp \
                    ../dpuser/dpuser.input.cpp \
                    ../dpuser/procedures.cpp \
                    ../dpuser/parser/lex.yy.cpp \
                    ../dpuser/parser/svn_revision.cpp \
                    ../dpuser/dpuser_utils.cpp \
                    ../dpuser/dpuser.procs.cpp \
                    ../dpuser/mpfit/y.mpfit.cpp \
                    ../dpuser/mpfit/lex.mpfit.cpp \
                    ../dpuser/mpfit/mpfitAST.cpp \
                    ../dpuser/dpuserType.cpp \
                    ../dpuser/dpuserAST.cpp \
										../dpuser/gdl_dpuser.cpp \
										../dpuser/python_dpuser.cpp \
                    ../dpuser/arithmetics.cpp \
                    ../dpuser/boolean.cpp \
                    ../dpuser/doc/helpmap.cpp
#                    ../dpuser/dpuser.cpp \

    contains(DEFINES, HAS_PGPLOT) {
        contains(DEFINES, HAS_DPPGPLOT) {
            SOURCES += ../utils/cpg3d.c
            HEADERS += ../utils/cpg3d.h
        }
    }

    SOURCES +=      ../utils/dpstring.cpp \
                    ../utils/dpstringlist.cpp \
                    ../utils/regex/regex_sr.cpp \
                    ../utils/cmpfit/mpfit.cpp \
		    ../utils/kabsch/kabsch2d.cpp \
                    ../libfits/3d_stuff.cpp \
                    ../libfits/cube.c \
                    ../libfits/dpheader.cpp \
                    ../libfits/fits.cpp \
                    ../libfits/fits_cube.cpp \
                    ../libfits/fits_dpl.cpp \
                    ../libfits/fits_exc.cpp \
                    ../libfits/fits_file.cpp \
                    ../libfits/fits_filters.cpp \
                    ../libfits/fits_funcs.cpp \
                    ../libfits/fits_mem.cpp \
                    ../libfits/fits_ops.cpp \
                    ../libfits/fits_procs.cpp \
                    ../libfits/fits_range.cpp \
                    ../libfits/fits_red.cpp \
                    ../libfits/JulianDay.cpp \
                    ../libfits/math_utils.cpp \
                    ../libfits/dpComplex.cpp \
                    ../libfits/fitting.cpp \
                    ../libfits/voronoi.cpp \
                    ../libfits/fits_logic.cpp \
                    ../libfits/astrolib.cpp

    HEADERS +=      ../dpuser/dpuser.h \
                    ../dpuser/dpuser.yacchelper.h \
                    ../dpuser/functions.h \
                    ../dpuser/procedures.h \
                    ../dpuser/dpuser.procs.h \
                    ../dpuser/dpuser_utils.h \
                    ../dpuser/parser/y.tab.h \
                    ../dpuser/parser/svn_revision.h \
                    ../dpuser/dpuserType.h \
                    ../dpuser/dpuserAST.h \
                    ../dpuser/mpfit/y.tab.h \
                    ../dpuser/mpfit/mpfitAST.h \
                    ../utils/dpstring.h \
                    ../utils/dpstringlist.h \
                    ../utils/regex/regex_sr.h \
                    ../utils/cmpfit/mpfit.h \
		    ../utils/kabsch/kabsch2d.h \
                    ../libfits/cube.h \
                    ../libfits/defines.h \
                    ../libfits/fits.h \
                    ../libfits/collapse_helper.cpp \
                    ../libfits/logic_helper.cpp \
                    ../libfits/JulianDay.h \
                    ../libfits/math_utils.h \
                    ../libfits/platform.h \
                    ../libfits/dpComplex.h \
                    ../libfits/fitting.h \
                    ../libfits/astrolib.h
}

HEADERS +=

SOURCES +=
