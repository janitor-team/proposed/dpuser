#include <QFileDialog>
#include <QDialogButtonBox>
#include <QFontComboBox>
#include <QVBoxLayout>
#include <QSpacerItem>
#include <QPushButton>
#include <QLineEdit>
#include <QSpinBox>

#include "QFitsGlobal.h"
#include "QFitsPreferences.h"

QFitsPrefDialog::QFitsPrefDialog(QWidget* parent) : QDialog(parent)
{
    QWidget *tmpTab = NULL;

    //
    // Create & fill main vertical layout
    //
    QVBoxLayout *mainVLayout = new QVBoxLayout();
    setLayout(mainVLayout);

    // create tab widget
    tabWidget = new QTabWidget();
    mainVLayout->addWidget(tabWidget);

    // create buttons: Help, Cancel, Ok
    buttonHelp = new QPushButton("Help");
    buttonHelp->setAutoDefault(true);
    buttonHelp->setEnabled(false);
    QSpacerItem *spacerButton = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    buttonOk = new QPushButton("Ok");
    buttonOk->setAutoDefault(true);
    buttonOk->setDefault(true);
    buttonCancel = new QPushButton("Cancel");
    buttonCancel->setAutoDefault(true);

    QHBoxLayout *buttonHLayout = new QHBoxLayout();
    buttonHLayout->addWidget(buttonHelp);
    buttonHLayout->addItem(spacerButton);
    buttonHLayout->addWidget(buttonOk);
    buttonHLayout->addWidget(buttonCancel);
    mainVLayout->addLayout(buttonHLayout);

    //
    // 1st tab: Image Display
    //
    tmpTab = new QWidget();
    tabWidget->addTab(tmpTab, "Image Display");

    QLabel *labelImgScaleLim = new QLabel("Image scaling limits");
    imageScalingLimits = new QComboBox();
    imageScalingLimits->insertItem(0, "minmax");
    imageScalingLimits->insertItem(1, "99.9%");
    imageScalingLimits->insertItem(2, "99.5%");
    imageScalingLimits->insertItem(3, "99%");
    imageScalingLimits->insertItem(4, "98%");
    imageScalingLimits->insertItem(5, "95%");
    imageScalingLimits->setCurrentIndex(settings.defaultLimits);
    QLabel *labelInitialZoom = new QLabel("Initial zoom");
    initialZoom = new QComboBox();
    initialZoom->clear();
    initialZoom->insertItem(0, "25%");
    initialZoom->insertItem(1, "50%");
    initialZoom->insertItem(2, "100%");
    initialZoom->insertItem(3, "200%");
    initialZoom->insertItem(4, "400%");
    initialZoom->insertItem(5, "Fit window");

//    initialZoom->insertItem(0, "Fit window");
//    initialZoom->insertItem(1, "Fit width");
//    initialZoom->insertItem(2, "Fit height");
//    initialZoom->insertSeparator(3);
//    initialZoom->insertItem(4, "3.125%");
//    initialZoom->insertItem(5, "6.25%");
//    initialZoom->insertItem(6, "12.5%");
//    initialZoom->insertItem(7, "25%");
//    initialZoom->insertItem(8, "50%");
//    initialZoom->insertItem(9, "100%");
//    initialZoom->insertItem(10, "200%");
//    initialZoom->insertItem(11, "400%");
//    initialZoom->insertItem(12, "800%");
//    initialZoom->insertItem(13, "1600%");
//    initialZoom->insertItem(14, "3200%");

    initialZoom->setCurrentIndex(settings.defaultZoom);

    QGridLayout *tab1GridLayout = new QGridLayout();
    tab1GridLayout->addWidget(labelImgScaleLim, 0, 0);
    tab1GridLayout->addWidget(imageScalingLimits, 0, 1);
    tab1GridLayout->addWidget(labelInitialZoom, 1, 0);
    tab1GridLayout->addWidget(initialZoom, 1, 1);
    tmpTab->setLayout(tab1GridLayout);

    //
    // 2nd tab: Appearance
    //
    tmpTab = new QWidget();
    tabWidget->addTab(tmpTab,"Appearance");

    QLabel *labelViewingTools = new QLabel("Viewing Tools");
    viewingTools = new QComboBox();
    viewingTools->insertItem(0, "Hide");
    viewingTools->insertItem(1, "In Dock");
    viewingTools->insertItem(2, "Floating");
    viewingTools->setCurrentIndex(settings.showViewingTools);
    QLabel *labelTools = new QLabel("Tools");
    tools = new QComboBox();
    tools->insertItem(0, "Hide");
    tools->insertItem(1, "In Dock");
    tools->insertItem(2, "Floating");
    tools->setCurrentIndex(settings.showTools);
    QLabel *labelDpuserConsole = new QLabel("Dpuser Console");
    dpuserConsole = new QComboBox();
    dpuserConsole->insertItem(0, "Hide");
    dpuserConsole->insertItem(1, "In Dock");
    dpuserConsole->insertItem(2, "Floating");
    dpuserConsole->setCurrentIndex(settings.showDpuser);
    QLabel *labelTextFont = new QLabel("Text Font");
    textFont = new QFontComboBox();
    textFont->setCurrentFont(QFont(settings.textfont));
    textFontSize = new QSpinBox();
    textFontSize->setValue(settings.textsize);
    textFontSample = new QLabel("DPUSER> Sample text");

    QGridLayout *tab2GridLayout = new QGridLayout();
    tab2GridLayout->addWidget(labelViewingTools, 0, 0);
    tab2GridLayout->addWidget(viewingTools, 0, 1);
    tab2GridLayout->addWidget(labelTools, 1, 0);
    tab2GridLayout->addWidget(tools, 1, 1);
    tab2GridLayout->addWidget(labelDpuserConsole, 2, 0);
    tab2GridLayout->addWidget(dpuserConsole, 2, 1);
    tab2GridLayout->addWidget(labelTextFont, 3, 0);
    tab2GridLayout->addWidget(textFont, 3, 1);
    tab2GridLayout->addWidget(textFontSize, 3, 2);
    tab2GridLayout->addWidget(textFontSample, 4, 1);
    tmpTab->setLayout(tab2GridLayout);

    //
    // 3rd tab: Paths
    //
    tmpTab = new QWidget();
    tabWidget->addTab(tmpTab, "Paths");

    QLabel *pythonLibraryPathLabel = new QLabel("Python library location:");
    pythonLibraryPath = new QLineEdit();
    pythonLibraryPath->setText(settings.pythonLibraryPath);
    pythonLibraryButton = new QPushButton("...");

    QLabel *pythonPathLabel = new QLabel("PYTHONPATH");
    pythonPath = new QLineEdit();
    pythonPath->setText(settings.pythonPath);

    QLabel *gdlPathLabel = new QLabel("GDL_PATH");
    gdlPath = new QLineEdit();
    gdlPath->setText(settings.GDLPath);

    QGridLayout *tab3GridLayout = new QGridLayout();
    tab3GridLayout->addWidget(pythonLibraryPathLabel, 0, 0, Qt::AlignTop);
    tab3GridLayout->addWidget(pythonLibraryPath, 1, 0, 1, 1, Qt::AlignTop);
    tab3GridLayout->addWidget(pythonLibraryButton, 1, 1, 1, 1, Qt::AlignTop);
    tab3GridLayout->addWidget(pythonPathLabel, 2, 0, Qt::AlignTop);
    tab3GridLayout->addWidget(pythonPath, 3, 0, 1, 2, Qt::AlignTop);
    tab3GridLayout->addWidget(gdlPathLabel, 4, 0, Qt::AlignTop);
    tab3GridLayout->addWidget(gdlPath, 5, 0, 10, 2, Qt::AlignTop);

//    QWidget* tab3empty = new QWidget();
//    tab3empty->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
//    tab3GridLayout->addWidget(tab3empty, 2, 0);

    tmpTab->setLayout(tab3GridLayout);

    //
    // signals and slots connections
    //
    connect(buttonOk, SIGNAL(clicked()),
            this, SLOT(accept()));
    connect(buttonCancel, SIGNAL(clicked()),
            this, SLOT(reject()));
    connect(textFont, SIGNAL(activated(int)),
            this, SLOT(updateFontSample(int)));
    connect(textFontSize, SIGNAL(valueChanged(int)),
            this, SLOT(updateFontSample(int)));
    connect(pythonLibraryButton, SIGNAL(clicked()),
            this, SLOT(pythonLibraryButtonClicked()));

    //
    // tab order
    //
    setTabOrder(buttonHelp, buttonOk);
    setTabOrder(buttonOk, buttonCancel);
    setTabOrder(buttonCancel, imageScalingLimits);
    setTabOrder(imageScalingLimits, initialZoom);
    setTabOrder(initialZoom, viewingTools);
    setTabOrder(viewingTools, tools);
    setTabOrder(tools, dpuserConsole);
    setTabOrder(dpuserConsole, pythonLibraryPath);

updateFontSample(0);
}

QFitsPrefDialog::~QFitsPrefDialog()
{
}

void QFitsPrefDialog::accept() {
    settings.defaultLimits = imageScalingLimits->currentIndex();
    settings.defaultZoom = initialZoom->currentIndex();
    settings.showViewingTools = viewingTools->currentIndex();
    settings.showTools = tools->currentIndex();
    settings.showDpuser = dpuserConsole->currentIndex();
    settings.textfont = textFont->currentText();
    settings.textsize = textFontSize->value();
    settings.pythonLibraryPath = pythonLibraryPath->text();
    settings.pythonPath = pythonPath->text();
    settings.GDLPath = gdlPath->text();

    addPythonGDLPath();

    hide();
}

void QFitsPrefDialog::updateFontSample(int value) {
    textFontSample->setStyleSheet("font-family: " + textFont->currentText() +
                                  "; font-size: " + QString::number(textFontSize->value()) +
                                  "pt");
}

void QFitsPrefDialog::pythonLibraryButtonClicked() {
    QString f = QFileDialog::getOpenFileName(this, "Location of python library", "", "Dynamic Link Libraries (*.so *.dylib *.dll);;All files (*)");
    if ( !f.isEmpty() ) {
        pythonLibraryPath->setText(f);
        settings.pythonLibraryPath = f;
    }
}

QString QFitsPrefDialog::GetInitialZoom() {
    QString ret = initialZoom->currentText();
    if (ret.size() == 0) {
        ret = "100%";
    }
    return ret;
}
