#ifndef LUT_H
#define LUT_H

#include <QString>
#include <map>
#include <vector>

extern char          *colornames[];
extern unsigned char colormaps[];

typedef struct type_singlecolormap {
	unsigned char red[256];
	unsigned char green[256];
	unsigned char blue[256];
} _type_singlecolormap;

typedef std::map<QString,type_singlecolormap> type_colormap;

extern type_colormap mapcolors;

void init_colormaps(void);

#endif /* LUT_H */
