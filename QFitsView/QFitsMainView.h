#ifndef QFITSMAINVIEW_H
#define QFITSMAINVIEW_H

#include <QWidget>
#include <QTime>
#include <QMap>

#include "QFitsGlobal.h"

class QGridLayout;
class QFitsMainWindow;
class QFitsWidget1D;
class QFitsWidget2D;
#ifdef HAS_VTK
class QFitsWidget3D;
#endif
class QFitsBaseBuffer;
class QFitsWidgetWiregrid;
class QFitsWidgetContour;
class QFitsWedge;
class Fits;
class dpuserType;
class dpuserTypeList;
class dpStringList;
class QFitsMainView;

class dummyBuffer : public QWidget {
    Q_OBJECT
public:
    dummyBuffer(QWidget *parent, int myID);
protected:
    void mousePressEvent(QMouseEvent *);
private:
    int theID;
public:
signals:
    void currentID(int);
};

class QFitsGrid : public QWidget {
    Q_OBJECT
public:
    QFitsGrid(QFitsMainView *parent, int howmany);
    QGridLayout *grid;
    dummyBuffer *widgets[4];
    QMap<int, QWidget*> gridMap;
    int activeID;
    int activeRow();
    int activeColumn();
    int indexOf(int, int);
    void addWidget(QWidget *);
    void addWidget(QWidget *, int, int);
    void removeWidget(QWidget *);
protected:
    void paintEvent(QPaintEvent *);
public slots:
    void activeIDChanged(int);
private:
    QFitsMainView *myParent;
};

class QFitsMainView : public QWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsMainView(QFitsMainWindow *parent);
    ~QFitsMainView();

    bool bufferMapContainsKey(std::string);
    std::string getCurrentBufferIndex();
    int getBufferSize();
    void setCurrentBufferFromWidget(QFitsBaseBuffer *);
    void setCurrentBufferIndex(std::string val);
    void showMultiple(int howMany);
    QFitsBaseBuffer* getCurrentBuffer();
    QFitsBaseBuffer* getBuffer(std::string i);
    QFitsWedge* getWedge() { return wedge; }
    bool getAutoScale() { return autoScale; }
//    void showFitsBuffer(QFitsBaseBuffer *which);
    void deleteBaseBuffer(std::string id);
    bool hasBaseBuffer(std::string id);
    void hideAllBuffers();

    QFitsBaseBuffer* addNewBuffer   (std::string, dpuserType*);
    QFitsBaseBuffer* recreateBuffer (std::string, dpuserType*);
    QFitsBaseBuffer* updateBuffer   (std::string, dpuserType*);

    void copyImage(int copymode = 2);

    void keyPressEvent(QKeyEvent*);
    void resizeEvent(QResizeEvent*);
    void initialResize();

    bool hasGrid() { return grid != NULL; }

protected:
private:
    void myResizeEvent(int, int);


//----- Slots -----
public slots:
    void setAutoScale(bool scale);
    void saveImage();
    void saveFits();
    void printImage();
    void updateOrientation();
    void setColormap(QAction*);
    void setInvertColormap(bool);
    void setBrightness(int);
    void setContrast(int);
    void setMovieSpeed(int);
    void enableMovie(bool);
    void playTetris();
    void slotRedoManualSpectrum();

//----- Signals -----
//----- Members -----
public:
    std::map<std::string, QFitsBaseBuffer*> bufferMap;
    QList<std::string>                  recentBuffers;
    QFitsGrid                   *grid;

private:
    QFitsMainWindow             *myParent;
    QFitsWedge                  *wedge;
    std::string                 currentBuf;
    int                         numberOfBuffersToShow;
    bool                        autoScale;
};

#endif /* QFITSMAINVIEW_H */
