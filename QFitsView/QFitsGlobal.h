#ifndef QFITSGLOBAL_H
#define QFITSGLOBAL_H

#include <QReadWriteLock>
#include <QImage>
#include <QLabel>
#include <QPixmap>
#ifdef WIN
#include <QSettings>
#endif

#define NCOLORS 256

class QFitsMainWindow;
class qtdpuser;
class qFitsViewSettings;

//------------------------------------------------------------------------------
//         Global variables
//------------------------------------------------------------------------------
extern QFitsMainWindow      *fitsMainWindow;
extern qtdpuser             *dpuser_widget;
extern int                  zoomIndex;
extern QString              appDirPath;
extern QRgb                 colourTable[NCOLORS],
                            currentTable[NCOLORS];
extern QReadWriteLock       buffersLock;
extern qFitsViewSettings    settings;
extern bool                 resetGUIsettings;

//------------------------------------------------------------------------------
//         Global enums
//------------------------------------------------------------------------------
namespace QFV {
    enum Orientation {
        Default     = 0,
        Horizontal  = 1,
        Vertical    = 2,
        Wavelength  = 3
    };
}

typedef enum {
    FitZoomNone,
    FitZoomWindow,
    FitZoomWidth,
    FitZoomHeight
} FitZoom;

typedef enum {
    DisplayCubeSingle,
    DisplayCubeAverage,
    DisplayCubeMedian,
    DisplayCubeLinemap
} dpCubeMode;

typedef enum {
    ScaleMinMax = 0,
    Scale999 = 1,
    Scale995 = 2,
    Scale99 = 3,
    Scale98 = 4,
    Scale95 = 5,
    ScaleManual = 6
} dpScaleMode;

typedef enum {
    ViewImage = 0,
    ViewWiregrid = 1,
    ViewContour = 2,
    ViewTable = 3,
//#ifdef HAS_VTK
    View3D = 4,
//#endif
    ViewUndefined = -1
} dpViewMode;

QString searchForDocumentation();
void addPythonGDLPath();

class QFitsScriptable : public QObject {
    Q_OBJECT
public:
    QFitsScriptable(QObject *parent) : QObject(parent) { }
signals:
    void zoomin();
    void zoomout();
};

//------------------------------------------------------------------------------
//         Global struct
//------------------------------------------------------------------------------
typedef enum { Tools_None = 0, Tools_2Dfit = 1, Tools_Markpos = 2, Tools_Deblend = 4 } ToolsVisible;

typedef struct _BufferAppearance BufferAppearance;
struct _BufferAppearance {
    QString windowTitle;
    bool    hideWedge,
            hideViewingtools,
            hideViewingtoolsCutsplot,
            hideSpectrum,
            enableSpectrumControls,
            enableToolbarPhysicalRange,
            showToolbarZoom,
            showToolbarOrientation,
            showToolbarScaling,
            showToolbarMovie,
            showToolbarArithmetics,
            showToolbarCube,
            enableImredMenu,
            enableArithmeticButtons,
            enableColourmapMenu,
            enableScaleMenu,
            enableZoomMenu;
    ToolsVisible toolsVisible;
};

//------------------------------------------------------------------------------
//         well... Global class?
//------------------------------------------------------------------------------
class QFitsSimplestButton : public QLabel {
    Q_OBJECT
//----- Functions -----
public:
    QFitsSimplestButton(QPixmap pm, QWidget *parent);
    QFitsSimplestButton(const QString &te, QWidget *parent);
    ~QFitsSimplestButton() {}

protected:
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void mouseDoubleClickEvent(QMouseEvent *);

//----- Slots -----
//----- Signals -----
signals:
    void clicked();
    void doubleClicked();

//----- Members -----
};

class qFitsViewSettings {
//----- Functions -----
public:
        qFitsViewSettings();
        ~qFitsViewSettings();
private:
        void readSettings();
        void writeSettings();

//----- Slots -----
//----- Signals -----
//----- Members -----
public:
        int defaultLimits;
        int defaultZoom;
        int showViewingTools;
        int showDpuser;
        int showTools;
        int xorigin, yorigin, width, height;
        int wiregridheight, wiregridwidth;
        bool maximized;
        int plotstyle;
        QString textfont;
        int textsize;
        QString pythonLibraryPath;
        QString pythonPath;
        QString GDLPath;
        QString lastOpenPath;
        QString lastSavePath;
        bool linux_start_home;
        bool loadAllExt;
        int loadExtWidth, loadExtHeight;
        bool loadExtMaximized;
};

#endif // QFITSGLOBAL_H
