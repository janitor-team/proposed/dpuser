#ifndef QFITSTOOLBAR_H
#define QFITSTOOLBAR_H

#include <QPushButton>
#include <QComboBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QToolButton>
#include <QString>
#include <QMenu>
#include <QMutex>
#include <QSlider>
#include <QActionGroup>
#include <QCheckBox>

#include "QFitsGlobal.h"

class QFitsMainWindow;
class QFitsBaseBuffer;
class QFitsSingleBuffer;
class ActionComboBox;   //declaration below

class QFitsToolBar : public QWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsToolBar(QFitsMainWindow *parent);
    ~QFitsToolBar() {}

    void updateValues();
    void setRangeValues();
    void blankRangeValues();
    void setZoomValue(double);
    void setZoomTextCombo(double);
    void showControls();
    void enableControlsView1D(bool);
    void enableControlsView2D(bool);
    void enableControlsPhysX(bool);
    void enableControlsArithmetic(bool);
    void setPaletteControls(QPalette&);
    void updateImageMinMax(QFitsBaseBuffer*);
    void updateSpecMinMaxY(QFitsBaseBuffer*);
    QComboBox* getActualZoomCombo();
    double getZoomTextCombo(FitZoom*);

private:
    QFitsSingleBuffer *getMyBuffer();
    void showControlsZoom(dpViewMode);
    void showControlsOrientation(bool);
    void showControlsScaling(bool);
    void showControlsMovie(bool);
    void showControlsArithmetics(bool);
    void showControlsCube(bool);
    void keyPressEvent( QKeyEvent*);

//----- Slots -----
public slots:
    void incZoom();
    void decZoom();
//#ifdef HAS_VTK
    void incZoom_3Dwire();
    void decZoom_3Dwire();
//#endif

private slots:
    void comboZoomTriggered(const QString&);
    void movieButtonToggled(bool);
    void setImageLimitsManual();
    void setSpecYLimitsManual();
    void setImageLimitsScaleRange(int);
    void incRotation();
    void decRotation();
    void flipped(bool);
    void flopped(bool);
    void setOrientation();
    void setRotation(int);
    void setSpecChannelMinXVal(int);
    void setSpecChannelMinXVal2();
    void setSpecChannelMaxXVal(int);
    void setSpecChannelMaxXVal2();
    void setSpecPhysicalMinXVal();
    void setSpecPhysicalMaxXVal();
    void createArithPopupMenu(int arithmethics, QMenu *arithPopupMenu);
    void createPlusPopupMenu();
    void createMinusPopupMenu();
    void createMulPopupMenu();
    void createDivPopupMenu();
    void createPowPopupMenu();
    void createConvPopupMenu();
    void doArithmetics(QAction*);

//----- Signals -----
signals:
    void orientationChanged();
//#ifdef HAS_VTK
    void updateZRange3D(double, double);
//#endif

//----- Members -----
public:
    QSpinBox        *cubeImageSlice;
    QToolButton     *movieButton;
    QComboBox       *comboZoom,
                    *comboZoom_3Dwire;
    QSlider         *alphaMultiplierSlider,
                    *limitSliderZ;
    QCheckBox       *drawSliceFramesCheckBox,
                    *limitZCheckBox;
    QSpinBox        *limitSpinboxZ;

private:
    bool            updateAll;
    QToolButton     *buttonOpen,
                    *buttonReload,
                    *buttonSave,
                    *buttonCopy,
                    *buttonPrint,
                    *buttonRotateCCW,
                    *buttonRotateCW,
                    *buttonFlip,
                    *buttonFlop,
                    *buttonZoomIn,
                    *buttonZoomOut,
                    *buttonZoomIn_3Dwire,
                    *buttonZoomOut_3Dwire,
                    *imredPlus,
                    *imredMinus,
                    *imredMul,
                    *imredDiv,
                    *imredPow,
                    *imredConv;
    QComboBox       *comboRotate,
                    *cubeMode,
                    *imageScaleRange;
    ActionComboBox  *imageScaleMethod;
    QSpinBox        *specChannelMinXValue,
                    *specChannelMaxXValue;
    QLineEdit       *specPhysMinXValue,
                    *specPhysMaxXValue,
                    *specPhysMinYValue,
                    *specPhysMaxYValue,
                    *imageMinValue,
                    *imageMaxValue;
    QSlider         *movieSlider;
    QMenu           *plusPopupMenu,
                    *minusPopupMenu,
                    *mulPopupMenu,
                    *divPopupMenu,
                    *powPopupMenu,
                    *convPopupMenu;
    QLabel          *alphaMultiplierLabel;
    int             requestedArithmetics;
};

class ActionComboBox : public QComboBox {
    Q_OBJECT
//----- Functions -----
public:
    ActionComboBox(QWidget *parent);
    ~ActionComboBox() {}
    void setActions(QActionGroup *actions);
    QActionGroup *comboActions;

//----- Slots -----
public slots:
    void actionActivated(QAction *);
    void itemTriggered(int);
//----- Signals -----
//----- Members -----
};

#endif /* QFITSTOOLBAR_H */
