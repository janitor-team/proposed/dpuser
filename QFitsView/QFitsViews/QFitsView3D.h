#ifndef QFITSVIEW3D_H
#define QFITSVIEW3D_H

#include <Q3DScatter>
#include <QCustom3DVolume>
#include "QFitsBaseView.h"
#include "fits.h"

class QFitsWidget3D;
class VolumetricModifier;
class QSlider;
class QLabel;
class QSpinBox;

using namespace QtDataVisualization;

//------------------------------------------------------------------------------
//         QFits3DScatter
//------------------------------------------------------------------------------
class QFits3DScatter: public Q3DScatter {
    Q_OBJECT
public:
    QFits3DScatter();
protected:
    void mouseMoveEvent(QMouseEvent*);
signals:
    void setBrightnessContrast(QMouseEvent *);
};

//------------------------------------------------------------------------------
//         QFitsView3D
//------------------------------------------------------------------------------
class QFitsView3D : public QFitsBaseView {
    Q_OBJECT
//----- Functions -----
public:
    QFitsView3D(QFitsWidget3D* parent);
    ~QFitsView3D();

    void updateColourtable();
private:
    void handleSlicingChanges();
    void adjustSliceLabels();
    Fits work;

//----- Slots -----
public slots:
    void newData();
    void setData();
    void newData2() {}  // TO DO: CubeSpectrum!!! um z-range auszuwählen Würg!
    void updateBrightnessContrast(QMouseEvent* event);
    void adjustSliceX(int value);
    void adjustSliceY(int value);
    void adjustSliceZ(int value);
    void sliceX(int enabled);
    void sliceY(int enabled);
    void sliceZ(int enabled);
    void setPreserveOpacity(bool enabled);
    void setUseHighDefShader(bool enabled);
    void adjustAlphaMultiplier(int value);
    void setDrawSliceFrames(int enabled);
    void limitZ();
    void updateSpectrum(const QVector3D &pos);

//----- Signals -----
signals:
    void updateMagnifier(QPixmap &pix);

//----- Members -----
private:
    QFitsWidget3D   *myParent;
    QFits3DScatter  *scatterWindow;
    QCustom3DVolume *m_volume;
    int             m_sliceIndexX,
                    m_sliceIndexY,
                    m_sliceIndexZ;
    bool            m_slicingX,
                    m_slicingY,
                    m_slicingZ;
    QSlider         *m_sliceSliderX,
                    *m_sliceSliderY,
                    *m_sliceSliderZ;
    QVector<QRgb>   colortable;
    QLabel          *m_sliceImageX,
                    *m_sliceImageY,
                    *m_sliceImageZ;

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// OLD: FROM VTK
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//public:
//    void updateScaling();
//protected:
//    void paintEvent(QPaintEvent *e);
//    void keyPressEvent( QKeyEvent *e );
//    void updateData();
//    void calcZoomedRanges(int *x1, int *x2, int *y1, int *y2,
//                                  int *z1, int *z2);
//    void mouseReleaseEvent(QMouseEvent* event);
//    void calcZoomedVisibleArea();
//    void newColourtable();
//    void enterEvent(QEvent*);
//    void leaveEvent(QEvent*);
//
//----- Slots -----
//public slots:
//    void setImageCenter(double, double);
//    void applyZoom();
//    void updateZRange3D(double, double);
//
//----- Signals -----
//signals:
//    void flipX();
//    void flipY();
//    void decRot();
//
//----- Members -----
//    private:
//    int previousBuffer;
//    bool initialized;
//    double default_cam_viewUp[3];
//    double default_cam_position[3];
//    Fits subcube;
//    vtkImageImport *importer;
//    vtkRenderer *renderer;
//    vtkPiecewiseFunction *opacityTransferFunction;
//    vtkColorTransferFunction *colorTransferFunction;
//    vtkVolumeProperty *volProperty;
//    vtkVolumeRayCastMapper *volMapper;
//    vtkVolumeRayCastCompositeFunction *compositeFunction;
//    vtkVolume *vol;
//    QVTKInteractor *iren;
//    vtkCamera *camera;
//    bool colourtableDirty;
//    bool scalingDirty;
//    vtkCubeAxesActor2D *axes;
//    double zmin;
//    double zmax;
};

#endif /* QFITSVIEW3D_H */
