#ifndef QFITSVIEWTABLEHEADER_H
#define QFITSVIEWTABLEHEADER_H

#include <QtGui>
#include <QHeaderView>
#include <QItemDelegate>
#include <QItemEditorFactory>

class QFitsViewTableHeader : public QHeaderView
{
    Q_OBJECT

public:
    QFitsViewTableHeader(Qt::Orientation orientation, QWidget* parent = 0);
    QRect sectionRect(int logicalIndex) const;

protected:
    void mouseDoubleClickEvent(QMouseEvent* event);
    void paintSection(QPainter* painter, const QRect& rect, int logicalIndex) const;

private:
    int pick(const QPoint& pos) const;
    mutable QHash<int, QRect> sections;

signals:
    void renamePlotLabels();
protected slots:
    void headerDataChanged(Qt::Orientation orientation,
                           int logicalFirst, int logicalLast);
};

class QFitsHeaderDelegate : public QItemDelegate
{
public:
    QFitsHeaderDelegate(Qt::Orientation orientation, QObject* parent = 0);

    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
                          const QModelIndex& index) const;
    void     setEditorData(QWidget* editor, const QModelIndex& index) const;
    void     setModelData(QWidget* editor, QAbstractItemModel* model,
                          const QModelIndex& index) const;
    void     updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option,
                                  const QModelIndex& index) const;

private:
    const QItemEditorFactory* editorFactory() const;
    Qt::Orientation orientation;
};

#endif // QFITSVIEWTABLEHEADER_H
