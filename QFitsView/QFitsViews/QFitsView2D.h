#ifndef QFITSVIEW2D_H
#define QFITSVIEW2D_H

#include <QWidget>

#include "QFitsBaseView.h"
#include "QFitsGlobal.h"

class QMenu;
class Fits;
class QFitsSingleBuffer;
class QFitsScroller;
class QFitsMarkers;
class QFitsWidget2D;

class QFitsView2D : public QFitsBaseView {
    Q_OBJECT
//----- Functions -----
public:
    QFitsView2D(QFitsScroller*, QFitsWidget2D*);
    ~QFitsView2D();

    void initPos();
    QFitsMarkers* getSourceMarkers();

    void orientationChanged()       {}
    void setFlipX(bool);
    void setFlipY(bool);
    void setScrollerWidgetCenter(double, double);
    void setScrollerImageCenter(double, double);
    void applyZoom();
    void drawLongslitMarker(QPainter &, int, int, double, int, double);
    void drawEllipticalProfileMarker(QPainter &, int, int, double, double, int);
    void updateLongslitMarker(int, int, double, int, double);
    void updateEllipticalProfileMarker(int, int, double, double, int);
    void updateMarkposMarker(QVector<int>);
    void drawMarkposMarker(QPainter &);
    void printImage();
    void setRotation(int);
    void scrollerMoved(int, int);
    int getXCenter() { return xcenter; }
    int getYCenter() { return ycenter; }
    void setMarkposDrawMode(bool enable);

protected:
    void paintEvent(QPaintEvent*);
    void mouseMoveEvent(QMouseEvent*);
    void mousePressEvent(QMouseEvent*);
    void mouseReleaseEvent(QMouseEvent*);
    void keyPressEvent(QKeyEvent*);
    void keyReleaseEvent(QKeyEvent*);
    void enterEvent(QEvent*);
    void leaveEvent(QEvent*);

    void widgetToImage(int, int);
    void widgetToImageNoSignal(int, int, double*, double*,
                               double *xorig = NULL, double *yorig = NULL);
    void drawDistanceMarker(QPainter&);
    void drawRectMarker(QPainter&, QColor);

private:
    std::string getBufferIndex();
    QFitsSingleBuffer* getMyBuffer();
    void lockUnlockSpectrum(QPoint&);
    bool completelyVisibleX();
    bool completelyVisibleY();
    QVector<int> markpos_Positions;

//----- Slots -----
private slots:
    void exportSpecAsciiSlot();
    void exportSpecFitsSlot();
    void copySpecBufferSlot();
    void copyImageBufferSlot();
    void copyCubeBufferSlot();
    void saveImageFitsSlot();
    void saveMBbufferSlot();
    void copyMBbufferSlot();
    void saveMarkersSlot();
    void exportMarkersSlot();
    void loadMarkersSlot();
    void radialProfileSlot();
    void lockPosSlot();
    void useStdStarSlot();

//----- Signals -----
signals:
    void mouseClicked(int, int);
    void statusbartext(const QString&);

//----- Members -----
private:
    enum dpFitsDisplayMode  { Simple,
                              distanceMarker,
                              longslitMarker,
                              ellipticalProfileMarker,
                              markposMarker };

    enum dpMouseMode        { None,
                              drawDistMarker,
                              drawRectGreen,
                              drawRectRed,
                              drawRectWhite };

    QFitsMarkers        *sourceContinuumMarkers;

    QFitsWidget2D       *myParent;
    dpMouseMode         mousemode;
    dpFitsDisplayMode   drawMode;
    QPoint              initialpos,
                        currentpos;
    unsigned char       colors[NCOLORS];
    bool                leftWidget,
                        initialZoom,
                        doScrollerUpdate;
    int                 Mode,
                        xcenter, ycenter,
                        xoffset, yoffset,
                        CubeCount,
                        oldx, oldy, oldwidth, oldlength, oldangle,
                        extra_int[5];
    double              extra_double[5];

//----- Right-click popup-menu -----
    void createPopupMenu();
    QMenu               *rightPopupMenu;
    QAction             *exportSpecAsciiAct,
                        *exportSpecFitsAct,
                        *copySpecBufferAct,
                        *copyImageBufferAct,
                        *saveImageFitsAct,
                        *saveMBbufferAct,
                        *copyMBbufferAct,
                        *saveMarkersAct,
                        *exportMarkersAct,
                        *loadMarkersAct,
                        *radialProfileAct,
                        *lockPosAct,
                        *useStdStarAct;
    int                 mouseX,
                        mouseY;
    QPoint              mousePos;
};

#endif /* QFITSVIEW2D_H */
