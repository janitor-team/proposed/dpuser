
#include "QFitsBaseView.h"

QFitsBaseView::QFitsBaseView(QWidget *parent) : QWidget(parent) {
}

void QFitsBaseView::switchBackgroundColor() {
//    if (backgroundRole() == QPalette::Window) {
//        setBackgroundRole(QPalette::Dark);
//    } else {
        setBackgroundRole(QPalette::Window);
//    }
}

void QFitsBaseView::setMousePressedPos(int x, int y) {
    mousePressedPosX = x;
    mousePressedPosY = y;
}

void QFitsBaseView::getMousePressedPos(int *x, int *y) {
    *x = mousePressedPosX;
    *y = mousePressedPosY;
}
