#ifndef QFITSVIEW1D_H
#define QFITSVIEW1D_H

#include <QWidget>
#include <QMenu>
#include <QMatrix>
#include <QScrollBar>
#include <QSpinBox>
#include <QDoubleSpinBox>

#include "QFitsBaseView.h"

//------------------------------------------------------------------------------
//         QFitsView1D
//------------------------------------------------------------------------------
class Fits;
class QFitsSingleBuffer;

class QFitsView1D : public QFitsBaseView {
    Q_OBJECT
//----- Functions -----
public:
    QFitsView1D(QWidget *parent);
    virtual ~QFitsView1D();

    void    setData();
    void    setData2(const double*, const double*, const unsigned long);
    void    removeData2();
    double  pixelToWavelength(int number);
    double  pixelToValue(int number);
    int     wavelengthToPixel(double wavelength);
    int     valueToPixel(double value);
    void    updateScaling();
    void    mouseMovedInSubwindow(const QPoint&);

    QFitsSingleBuffer* getMyBuffer();

    virtual void    setXRange(const double&, const double&);
    void            setYRange(const double&, const double&);
    double          getCrpix()                  { return crpix; }
    void            setCrpix(double d)          { crpix = d; }
    double          getCrval()                  { return crval; }
    void            setCrval(double d)          { crval = d; }
    double          getCdelt()                  { return cdelt; }
    void            setCdelt(double d)          { cdelt = d; }
    double*         getYdata()                  { return ydata; }
    void            setYdata(int i, double d)   { ydata[i] = d; }
    void            setSelectstart(QPoint p)    { selectstart = p; }
    int             getMargin()                 { return margin; }
    int             getFw()                     { return fw; }
    int             getFh()                     { return fh; }
    unsigned long   getNdata()                  { return ndata; }

protected:
    virtual void paintEvent( QPaintEvent*);
    void resizeEvent(QResizeEvent*);
    void mouseMoveEvent(QMouseEvent*);
    void mousePressEvent(QMouseEvent*);
    void mouseReleaseEvent(QMouseEvent*);
    void keyPressEvent(QKeyEvent*);
    void enterEvent(QEvent*);
    void leaveEvent(QEvent*);
    void calcAxisScale(double, double, int*, double*, double*);
    void drawAxis(QPainter*, double, double);
    void drawSelection(QPainter*);
    int calcPolyline(int, double*, double*, QPolygon&, double, double, bool close = false);
    int calcPos(QPoint);

private:
    void createPopupMenu();

//----- Slots -----
public slots:
    void setAutoScale(bool);

private slots:
    void zoomOut();
    void setPlotStyleLines();
    void setPlotStyleHistogram();
    void exportSpecAsciiSlot();
    void exportSpecFitsSlot();
    void saveMBbufferSlot();
    void copyMBbufferSlot();

//----- Signals -----
signals:
    void statusbartext(const QString&);

//----- Members -----
public:
// myParent is not declared because QFitsWidget1D and QFitsCubeSpectrumViewer
// are derived from QFitsView1D (different parents)!
//    QFitsWidget1D *myParent;
protected:
    QStringList     messages;
    double          *xdata,
                    *ydata,
                    *x2data,
                    *y2data,
                    crval,
                    cdelt,
                    crpix;
    unsigned long   ndata,
                    n2data;
    QMatrix         m;
    int             plotwidth,
                    plotheight,
                    margin,
                    fw,
                    fh;
    QPoint          selectstart,
                    selectend;
    QString         posinfo;

private:
    bool            autoScaleSpectrum;
    QMenu           *rightPopupMenu;
    QAction         *zoomOutAct,
                    *plotStyleLinesAct,
                    *plotStyleHistogramAct,
                    *exportSpecAsciiAct,
                    *exportSpecFitsAct,
                    *saveMBbufferAct,
                    *copyMBbufferAct;
};

#endif /* QFITSVIEW1D_H */
