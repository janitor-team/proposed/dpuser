#ifndef QFITS_PREFERENCES_H
#define QFITS_PREFERENCES_H

//#include <QVariant>
//#include <QPixmap>
#include <QDialog>
//#include <QLineEdit>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QTabWidget;
class QWidget;
class QSpinBox;
class QLabel;
class QComboBox;
class QLineEdit;
class QPushButton;

class QFitsPreferences : public QDialog {
    Q_OBJECT

public:
    QFitsPreferences( QWidget* parent = 0, const char* name = 0, bool modal = FALSE);
    ~QFitsPreferences();

    QTabWidget* tabWidget2;
    QWidget* tab;
    QSpinBox* cube3dx;
    QLabel* textLabel2_3;
    QSpinBox* cube3dy;
    QLabel* textLabel3;
    QSpinBox* cube3dz;
    QComboBox* defaultZoom;
    QLabel* textLabel1;
    QLabel* textLabel1_5;
    QSpinBox* wiregridwidth;
    QLabel* textLabel2_4;
    QSpinBox* wiregridheight;
    QLabel* textLabel1_6;
    QLabel* textLabel1_4;
    QComboBox* defaultLimits;
    QWidget* tab_2;
    QLabel* textLabel2;
    QComboBox* viewingTools;
    QLabel* textLabel2_2;
    QComboBox* Dpuser;
    QWidget* TabPage;
    QLabel* textLabel1_2;
    QLineEdit* docuPath;
    QPushButton* docuButton;
    QPushButton* buttonHelp;
    QPushButton* buttonOk;
    QPushButton* buttonCancel;
    QLabel* textLabel1_3;

protected:
    QHBoxLayout* preferencesLayout;
    QVBoxLayout* layout13;
    QVBoxLayout* tabLayout;
    QSpacerItem* spacer4;
    QGridLayout* layout12;
    QHBoxLayout* layout9;
    QHBoxLayout* layout11;
    QSpacerItem* spacer5_2;
    QVBoxLayout* tabLayout_2;
    QGridLayout* layout2;
    QSpacerItem* spacer3;
    QHBoxLayout* TabPageLayout;
    QVBoxLayout* layout9_2;
    QSpacerItem* spacer5;
    QHBoxLayout* layout8;
    QHBoxLayout* Layout1;
    QSpacerItem* Horizontal_Spacing2;

protected slots:
    virtual void languageChange();
    void docuSearchButtonPressed();

private:
    QPixmap image0;

};

#endif // QFITS_PREFERENCES_H
