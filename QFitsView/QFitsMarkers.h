#ifndef QFITSMARKERS_H
#define QFITSMARKERS_H

#include <QWidget>

class QFitsSingleBuffer;

class QFitsMarkers {
public:
//----- Functions -----
    QFitsMarkers(QColor, QColor, QColor, QColor, QColor, QColor,
                 Qt::PenStyle ps = Qt::SolidLine,
                 Qt::BrushStyle bs = Qt::SolidPattern);
    ~QFitsMarkers() {}

    bool hasSourceMarkers();
    bool hasAnyMarkers();
    void addMarkerSource(long, long);
    void addMarkerContinuum(long, long);
    void addMarkerLock(long, long);
    void setupPointMarker();
    void setupCircleMarkers(int);
    void setupCircleAnnularMarkers(int, int);
    int getSizeSource();
    int getSizeContinuum();
    void getMarkerSource(int, long*, long*);
    void getMarkerContinuum(int, long*, long*);
    void deleteMarker(long, long);
    void deleteMarkerLock();
    void deleteAllMarkers();
    void drawAllMarkers(QPainter*, int, int, QFitsSingleBuffer*);
    void initCenter();
    void setCenter(QFitsSingleBuffer*, long, long);
    void setBufferToPaint(QFitsSingleBuffer *sb);

//----- Slots -----
//----- Signals -----
//----- Members -----
private:
    QVector<long>       markersSourceX,
                        markersSourceY,
                        markersContinuumX,
                        markersContinuumY;
    long                markerLockX,
                        markerLockY,
                        centerX,
                        centerY;
    int                 initVal;
    QPen                *sourcePen,
                        *continuumPen,
                        *lockPen;
    QBrush              *sourceBrush,
                        *continuumBrush,
                        *lockBrush;
    QFitsSingleBuffer   *bufferToPaint;
};

#endif // QFITSMARKERS_H
