
# To disable VTK, search for
# "contains(QT, opengl):DEFINES += HAS_VTK"
# and uncomment that line

##### enable dFitsDebug() output by uncommenting the following line #####
#DEFINES += QFITSVIEW_SUPERDUPER_DEBUG_OUTPUT

##### set TEMPLATE #####
TEMPLATE = app

RESOURCES = doc/QFitsViewDoc.qrc \
            resources/QFitsViewRes.qrc

##### set QT #####
QT += network \
      printsupport \
			svg
#greaterThan(QT_VERSION, 5.7) {
  QT += datavisualization
	DEFINES += HAS_VTK
#} 

#DEFINES += LBT

DEFINES += HAS_GDL

DEFINES += HAS_PYTHON

# This is necessare so python_dpuser.cpp can compile in debug mode
#CONFIG(debug):QMAKE_CXXFLAGS +=" -O2 "
mac {
    CONFIG(release, debug|release):QMAKE_INFO_PLIST = resources/Info.plist
    CONFIG(debug, debug|release):QMAKE_INFO_PLIST = resources/InfoD.plist
}

##### set ARCH #####
win32 {
    ARCH = WIN
    contains(QMAKE_HOST.arch, x86_64):{
        ARCHBIT = 64
    } else {
        ARCHBIT = 32
    }
} else:mac {
    ARCH = MACOSX
    QMAKE_CFLAGS += " -fvisibility=hidden "
    QMAKE_CXXFLAGS += " -fvisibility=hidden "
    contains(QMAKE_HOST.arch, x86_64):{
        ARCHBIT = 64
    } else {
        ARCHBIT = 32
        QMAKE_CFLAGS += " -arch i386 "
        QMAKE_CXXFLAGS += " -arch i386 "
        QMAKE_LFLAGS += " -arch i386 "
    }
} else:unix {
    ARCH = LINUX
    contains(QMAKE_HOST.arch, x86_64):{
        ARCHBIT = 64
    } else {
        ARCHBIT = 32
    }
    QMAKE_LFLAGS += " -static-libstdc++ -static-libgcc "
} else:message(">>> ERROR: Unsupported platform!")

##### set OpenGL #####
#!win32:QT += opengl
#else:win32:!qf_shared:QT += opengl

# ### set CONFIG ################
CONFIG += qt c++11 \
          warn_off \
          thread
win32:qf_static {
    # MinGW, Win32, static QFitsView
    CONFIG += static
}

win32:!vs_proj:DEFINES += HAVE_MINGW32  # needed for xpa

# ### set DEFINES ################
DEFINES += DPQT \
           HAS_PGPLOT \
           HAS_DPPGPLOT \
           NO_READLINE \
           $$ARCH
#contains(QT, opengl):DEFINES += HAS_VTK
win32:vs_proj:DEFINES += NOXPA \
                         STATICQT
else:CONFIG(debug, debug|release) {
    # Win32, MinGW, debug-versions
    DEFINES += DBG
}
win32:qf_static:DEFINES += STATICQT

# ### set INCLUDEPATH ################
INCLUDEPATH += ../utils \
               ../libfits \
               ../include \
               ../dpuser \
               ../dpuser/parser \
               ../QFitsView/QFitsBuffers \
               ../QFitsView/QFitsWidgets \
               ../QFitsView/QFitsViews

contains(DEFINES, HAS_GDL) {
    INCLUDEPATH += ../include/gdl
}

# ### set MAKEFILE ################
win32:vs_proj:MAKEFILE = QFitsView
else:MAKEFILE = qfitsview.mk

# ### set TARGET ################
win32:vs_proj:TARGET = QFitsView
else { 
    CONFIG(release, debug|release):TARGET = QFitsView
    CONFIG(debug, debug|release):TARGET = QFitsViewD
}

# ### set LIBS ################
    CONFIG += qf_libs_common
    include(QFitsView_common.pro)
    CONFIG -= qf_libs_common

# ### settings specific to WINDOWS ################
win32 {
	QMAKE_LFLAGS += -static
    ##### set build dir for mingw ($$MY_DIR) #####
    CONFIG += win32_setup
    include(QFitsView_common.pro)
    CONFIG -= win32_setup

    DEFINES -= UNICODE
    RC_FILE  = QFitsView.rc
    LIBS    += -lgdi32
    vs_proj:contains(DEFINES, HAS_PGPLOT) {
        LIBS += ../lib/$$ARCH$$ARCHBIT/libpgplot.lib \
                ../lib/$$ARCH$$ARCHBIT/vcf2c.lib
    } else {
        qf_static:contains(DEFINES, HAS_PGPLOT) {
            LIBS += -lws2_32
        } else {
            # MY_DIR is defined in QFitsView_common.pro for WIN
            CONFIG(release, debug|release):LIBS += $$MY_DIR/QFitsView.dll
            CONFIG(debug, debug|release):LIBS += $$MY_DIR/QFitsViewD.dll
        }
    }
}

##### ZLIB stuff
win32:CONFIG(debug, debug|release): LIBS += ../lib/$$ARCH$$ARCHBIT/libpng.a ../lib/$$ARCH$$ARCHBIT/libz.a
linux: LIBS += ../lib/$$ARCH$$ARCHBIT/libz.a
mac: LIBS += ../lib/$$ARCH$$ARCHBIT/libpng.a ../lib/$$ARCH$$ARCHBIT/libz.a

#exists( $(QTDIR)/include/QtZlib/zlib.h) {
#  DEFINES += DPQT_ZLIB
#} else {
#  LIBS += ../lib/$$ARCH$$ARCHBIT/libpng.a ../lib/$$ARCH$$ARCHBIT/libz.a
#}

##### settings specific to MACOSX #####
mac { 
    INCLUDEPATH += /System/Library/Frameworks/Carbon.framework/Headers
    LIBS        += -framework Cocoa
    ICON        =  resources/telescope.icns
}

#### settings specific to LINUX #####
unix:!mac { 
    LIBS += -rdynamic \
            -lXt
}

# ### add sources and headers ################
# Linux and MacOS
!win32 { 
    CONFIG += qf_sources \
              dp_sources
    include(QFitsView_common.pro)
    CONFIG -= qf_sources \
              dp_sources
}

# Win32 and (vs_proj or qf_static)
win32:!qf_shared { 
    CONFIG += qf_sources \
              dp_sources
    include(QFitsView_common.pro)
    CONFIG -= qf_sources \
              dp_sources
}

# Win32 and qf_shared
win32:qf_shared:SOURCES = launcher.cpp

!win32:qf_shared {
    LIBS += ../lib/$$ARCH$$ARCHBIT/libpng.a ../lib/$$ARCH$$ARCHBIT/libz.a
}
