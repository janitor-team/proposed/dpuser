#include "osdir.h"
#include <iostream>

void walk(const std::string& aDir, const std::string& aName)
{
	std::string Full = aDir+'\\'+aName;
	if (aName == "")
		Full = aDir;
	std::cout << Full << std::endl;
	if ( (aName != ".") && (aName != "..") )
	{
		oslink::directory cur(Full);
		while (cur)
			walk(Full,cur.next());
	}
}

int main()
{
	walk("c:","");
	return 0;
}
