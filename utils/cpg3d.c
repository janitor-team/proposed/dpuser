#ifdef WIN
#pragma warning (disable: 4244) // disable warning for conversion from double to float, possible loss of data
#endif

#include <string.h>

#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define MASK 123459876

#ifdef HP
float ran(long *idum)
#else
float ran_(long *idum)
#endif
{
	long k;
	float ans;

	*idum ^= MASK;
	k=(*idum)/IQ;
	*idum=IA*(*idum-k*IQ)-IR*k;
	if (*idum < 0) *idum += IM;
	ans=AM*(*idum);
	*idum ^= MASK;
	return ans;
}
#undef IA
#undef IM
#undef AM
#undef IQ
#undef IR
#undef MASK

#ifdef HP
extern void freddy();
#else
extern void freddy_();
#endif

void cfreddy(const float *a, int kx, int ny, float size, float angle) {
#ifdef HP
	freddy((float *)a, &kx, &ny, &size, &angle);
#else
	freddy_((float *)a, &kx, &ny, &size, &angle);
#endif
}

#ifdef HP
extern void sbfint();
#define CSBFINT sbfint
#else
extern void sbfint_();
#define CSBFINT sbfint_
#endif

void csbfint(const float *rgb, int ic, int ibmode, int ibuf, int *maxbuf) {
	CSBFINT((float *)rgb, &ic, &ibmode, &ibuf, maxbuf);
}
#undef CSBFINT

#ifdef HP
extern void sbfbkg();
#define CSBFBKG sbfbkg
#else
extern void sbfbkg_();
#define CSBFBKG sbfbkg_
#endif

void csbfbkg(int ic1, int ic2, int ishade) {
	CSBFBKG(&ic1, &ic2, &ishade);
}
#undef CSBFBKG

#ifdef HP
extern void sbfsav();
#define CSBFSAV sbfsav
#else
extern void sbfsav_();
#define CSBFSAV sbfsav_
#endif

void csbfsav(int ibuf) {
	CSBFSAV(&ibuf);
}
#undef CSBFSAV

#ifdef HP
extern void sbfcls();
#define CSBFCLS sbfcls
#else
extern void sbfcls_();
#define CSBFCLS sbfcls_
#endif

void csbfcls(int ibuf) {
	CSBFCLS(&ibuf);
}
#undef CSBFCLS

#ifdef HP
extern void colint();
#define CCOLINT colint
#else
extern void colint_();
#define CCOLINT colint_
#endif

void ccolint(const float *rgb, int ic1, int ic2, float diffuse, float shine, float polish) {
	CCOLINT((float *)rgb, &ic1, &ic2, &diffuse, &shine, &polish);
}
#undef CCOLINT

#ifdef HP
extern void coltab();
#define CCOLTAB coltab
#else
extern void coltab_();
#define CCOLTAB coltab_
#endif

void ccoltab(const float *rgb, int ncol, float alfa, int ic1, int ic2) {
	CCOLTAB((float *)rgb, &ncol, &alfa, &ic1, &ic2);
}
#undef CCOLTAB

#ifdef HP
extern void colsrf();
#define CCOLSRF colsrf
#else
extern void colsrf_();
#define CCOLSRF colsrf_
#endif

void ccolsrf(const float *rgb, int ncol, float alfa, int ic1, int ic2, int ncband, float difuse, float shine, float polish) {
	CCOLSRF((float *)rgb, &ncol, &alfa, &ic1, &ic2, &ncband, &difuse, &shine, &polish);
}
#undef CCOLSRF

#ifdef HP
extern void sbball();
#define CSBBALL sbball
#else
extern void sbball_();
#define CSBBALL sbball_
#endif

void csbball(const float *eye, const float *centre, float radius, int ic1, int ic2, const float *light, int lshine, float *x0, float *y0, float *r0) {
	CSBBALL((float *)eye, (float *)centre, &radius, &ic1, &ic2, (float *)light, &lshine, x0, y0, r0);
}
#undef CSBBALL

#ifdef HP
extern void sbtbal();
#define CSBTBAL sbtbal
#else
extern void sbtbal_();
#define CSBTBAL sbtbal_
#endif

void csbtbal(const float *eye, const float *centre, float radius, int ic1, int ic2, const float *light, int lshine, float *x0, float *y0, float *r0, int itrans) {
	CSBTBAL((float *)eye, (float *)centre, &radius, &ic1, &ic2, (float *)light, &lshine, x0, y0, r0, &itrans);
}
#undef CSBTBAL

#ifdef HP
extern void sbplan();
#define CSBPLAN sbplan
#else
extern void sbplan_();
#define CSBPLAN sbplan_
#endif

void csbplan(const float *eye, float nv, const float *vert, int ic1, int ic2, const float *light) {
	CSBPLAN((float *)eye, &nv, (float *)vert, &ic1, &ic2, (float *)light);
}
#undef CSBPLAN

#ifdef HP
extern void sbplnt();
#define CSBPLNT sbplnt
#else
extern void sbplnt_();
#define CSBPLNT sbplnt_
#endif

void csbplnt(const float *eye, float nv, const float *vert, int ic1, int ic2, const float *light, int itrans) {
	CSBPLNT((float *)eye, &nv, (float *)vert, &ic1, &ic2, (float *)light, &itrans);
}
#undef CSBPLNT

#ifdef HP
extern void sbrod();
#define CSBROD sbrod
#else
extern void sbrod_();
#define CSBROD sbrod_
#endif

void csbrod(const float *eye, const float *end1, const float *end2, float radius, int ic1, int ic2, const float *light, int nsides, int lend) {
	CSBROD((float *)eye, (float *)end1, (float *)end2, &radius, &ic1, &ic2, (float *)light, &nsides, &lend);
}
#undef CSBROD

#ifdef HP
extern void sbcone();
#define CSBCONE sbcone
#else
extern void sbcone_();
#define CSBCONE sbcone_
#endif

void csbcone(const float *eye, const float *base, const float *apex, float radius, int ic1, int ic2, const float *light, int nsides) {
	CSBCONE((float *)eye, (float *)base, (float *)apex, &radius, &ic1, &ic2, (float *)light, &nsides);
}
#undef CSBCONE

#ifdef HP
extern void sbelip();
#define CSBELIP sbelip
#else
extern void sbelip_();
#define CSBELIP sbelip_
#endif

void csbelip(const float *eye, const float *centre, const float *paxes, int ic1, int ic2, const float *light, int lshine, int icline, float anglin, float *x0, float *y0, float *r0) {
	CSBELIP((float *)eye, (float *)centre, (float *)paxes, &ic1, &ic2, (float *)light, &lshine, &icline, &anglin, x0, y0, r0);
}
#undef CSBELIP

#ifdef HP
extern void sbline();
#define CSBLINE sbline
#else
extern void sbline_();
#define CSBLINE sbline_
#endif

void csbline(const float *eye, const float *end1, const float *end2, int icol, int ldash) {
	CSBLINE((float *)eye, (float *)end1, (float *)end2, &icol, &ldash);
}
#undef CSBLINE

#ifdef HP
extern void sbtext();
#define CSBTEXT sbtext
#else
extern void sbtext_();
#define CSBTEXT sbtext_
#endif

void csbtext(const float *eye, const char *text, int icol, const float *pivot, float fjust, const float *orient, float size) {
	int text_len = strlen(text);
	CSBTEXT((float *)eye, (char *)text, &icol, (float *)pivot, &fjust, (float *)orient, &size, text_len);
}
#undef CSBTEXT

#ifdef HP
extern void sbsurf();
#define CSBSURF sbsurf
#else
extern void sbsurf_();
#define CSBSURF sbsurf_
#endif

void csbsurf(const float *eye, const float *latice, float *dens, int n1, int n2, int n3, float dsurf, int ic1, int ic2, const float *light, int lshine) {
	CSBSURF((float *)eye, (float *)latice, dens, &n1, &n2, &n3, &dsurf, &ic1, &ic2, (float *)light, &lshine);
}
#undef CSBSURF

#ifdef HP
extern void sbtsur();
#define CSBTSUR sbtsur
#else
extern void sbtsur_();
#define CSBTSUR sbtsur_
#endif

void csbtsur(const float *eye, const float *latice, float *dens, int n1, int n2, int n3, float dsurf, int ic1, int ic2, const float *light, int lshine, int itrans) {
	CSBTSUR((float *)eye, (float *)latice, dens, &n1, &n2, &n3, &dsurf, &ic1, &ic2, (float *)light, &lshine, &itrans);
}
#undef CSBTSUR

#ifdef HP
extern void sbslic();
#define CSBSLIC sbslic
#else
extern void sbslic_();
#define CSBSLIC sbslic_
#endif

void csbslic(const float *eye, const float *latice, float *dens, int n1, int n2, int n3, float dlow, float dhigh, int ic1, int ic2, const float *slnorm, const float *aponit, int icedge) {
	CSBSLIC((float *)eye, (float *)latice, dens, &n1, &n2, &n3, &dlow, &dhigh, &ic1, &ic2, (float *)slnorm, (float *)aponit, &icedge);
}
#undef CSBSLIC

#ifdef HP
extern void sbcpln();
#define CSBCPLN sbcpln
#else
extern void sbcpln_();
#define CSBCPLN sbcpln_
#endif

void csbcpln(const float *eye, const float *latice, int ic1, int ic2, const float *light, const float *slnorm, const float *aponit, int icedge, int itrans) {
	CSBCPLN((float *)eye, (float *)latice, &ic1, &ic2, (float *)light, (float *)slnorm, (float *)aponit, &icedge, &itrans);
}
#undef CSBCPLN

#ifdef HP
extern void sb2srf();
#define CSB2SRF sb2srf
#else
extern void sb2srf_();
#define CSB2SRF sb2srf_
#endif

void csb2srf(const float *eye, const float *latice, float *dens, int n1, int n2, float dlow, float dhigh, float dvert, int ic1, int ic2, int ncband, const float *light, int lshine) {
	CSB2SRF((float *)eye, (float *)latice, dens, &n1, &n2, &dlow, &dhigh, &dvert, &ic1, &ic2, &ncband, (float *)light, &lshine);
}
#undef CSB2SRF

#ifdef HP
extern void xtal();
#define XTAL xtal
#else
extern void xtal_();
#define XTAL xtal_
#endif

void cxtal(const float *x, int nx, const float *y, int ny, float *z, int n1, int n2, float w, float size, int iwidth, const char *xlbl, const char *ylbl, const char *titl) {
	int len_xlbl = strlen(xlbl);
	int len_ylbl = strlen(ylbl);
	int len_titl = strlen(titl);
	XTAL((float *)x, &nx, (float *)y, &ny, z, &n1, &n2, &w, &size, &iwidth, (char *)xlbl, (char *)ylbl, (char *)titl, len_xlbl, len_ylbl, len_titl);
}
#undef xtal

